from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from qtconsole.rich_jupyter_widget import RichJupyterWidget
from qtconsole.manager import QtKernelManager

import sys
from argparse import ArgumentParser
import logging

from herkulex import *
from herkulex.servos import *
from herkulex.util import estimate_servo_timings, estimate_protocol_timeouts, write_servo_timings, read_servo_timings

from herkulex_graphic_tools.config import _config
from herkulex_graphic_tools.scope_widget import *
from herkulex_graphic_tools.control_widget import *
from herkulex_graphic_tools.monitor_widget import *

#import rospy
#from herkulex.ros_bus import ROSBusServer

from herkulex.zmq_bus import ZeroMQBusServer

class ConsoleWidget(RichJupyterWidget):

    def __init__(self, customBanner=None, *args, **kwargs):
        super(ConsoleWidget, self).__init__(*args, **kwargs)

        if customBanner is not None:
            self.banner = customBanner
        #create kernel
        kernel_manager = QtKernelManager(kernel_name='python')
        kernel_manager.start_kernel()
        # connect to it
        kernel_client = kernel_manager.client()
        kernel_client.start_channels()
        # assignmanager and client to current widget
        self.kernel_manager = kernel_manager
        self.kernel_client = kernel_client
        # on stop handler
        def stop():
            self.kernel_client.stop_channels()
            self.kernel_manager.shutdown_kernel()
            guisupport.get_app_qt4().exit()
        self.exit_requested.connect(stop)

    def clear(self):
        """
        Clears the terminal
        """
        self._control.clear()

    def print_text(self, text):
        """
        Prints some plain text to the console
        """
        self._append_plain_text(text)

    def execute_command(self, command):
        """
        Execute a command in the frame of the console widget
        """
        self._execute(command, False)

class Scope(QMainWindow):

    def __init__(self, period, bus, servo_id):
        super(Scope, self).__init__()
        # int ROS node
        # rospy.init_node("herkulex_scope")
        # connect to servos
        self.serial_bus = bus
        proto = ProtocolHandle(self.serial_bus)
        self.servo = Servo(proto, servo_id)
        # configure timings
        servo_timings = estimate_servo_timings(self.servo)
        # servo_timings = read_servo_timings(self.servo)
        write_servo_timings([self.servo], servo_timings)
        rt_timeout, normal_timeout = estimate_protocol_timeouts(proto, servo_timings, 1)
        self.cm_server = ZeroMQBusServer(self.serial_bus, spin_period = period + proto.processing_delay, spin_duration = normal_timeout + proto.processing_delay)
        # scope
        self.scope_widget = ScopeWidget()
        self.setCentralWidget(self.scope_widget)
        # update control 
        self.addDockWidget(Qt.TopDockWidgetArea, self._create_control_dock_widget(period))
        self._create_menu()
        # data source
        self._source_tabs = QTabWidget()
        self._source_tabs.addTab(RAMReadWidget(self.servo), 'READ_RAM')
        self._source_tabs.addTab(RTDebugWidget(self.servo), 'RT_DEBUG')
        source_dock_widget = _config(QDockWidget(), setWidget = self._source_tabs, setWindowTitle = 'Monitors')
        self.addDockWidget(Qt.RightDockWidgetArea, source_dock_widget)    
        # generators
        self._generator_tabs = QTabWidget()
        self._generator_tabs.addTab(GeneratorProfileWidget(self.servo), 'PROFILE')
        self._generator_tabs.addTab(GeneratorWaveformWidget(self.servo), 'POSITION/CURRENT')
        self._generator_tabs.addTab(GeneratorEmbededWidget(self.servo), 'EMBEDED CURRENT')
        self._generator_tabs.currentChanged.connect(self.on_generator_change)
        generator_dock_widget = _config(QDockWidget(), setWidget = self._generator_tabs, setWindowTitle = 'Generators')
        self.addDockWidget(Qt.RightDockWidgetArea, generator_dock_widget)    
        # console
        self._ipython_console = ConsoleWidget()
        ipython_dock_widget = _config(QDockWidget(), setWidget = self._ipython_console, setWindowTitle = 'Console')
        self.addDockWidget(Qt.LeftDockWidgetArea, ipython_dock_widget)    
        # timer
        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(int(1000*period))
        # state: cmd queue, data
        self._cmd_queue = []
        self._data = None
        # connect console to servo
        self._ipython_console.execute_command('%run zmq_startup.py')
        self._ipython_console.execute_command(f's = Servo(bus, {servo_id})')

    def _create_menu(self):
        # create submenu
        menu = self.menuBar()
        save_menu = menu.addMenu('&Data')
        #tuning_menu = menu.addMenu('&Tuning')
        # data submenu
        save_data = _config(QAction('&Save...', self), setShortcut = 'Ctrl+S', setStatusTip = 'Save plot data')
        save_data.triggered.connect(self.on_save_data)
        save_menu.addAction(save_data)
        # tuning submenu
        #ident = _config(QAction('Motor &Identification...', self), setShortcut = 'Ctrl+I', setStatusTip = 'Determine resistance nad backEMF')
        #ident.triggered.connect(self.on_ident)
        #tuning_menu.addAction(ident)

    def _create_control_dock_widget(self, period):
        layout = QHBoxLayout()
        # period
        layout.addWidget(QLabel('Update period (s)'))
        self._period_widget = _config(QDoubleSpinBox(), setMinimum = 0.0, setDecimals = 3, setValue = period)
        self._period_widget.valueChanged.connect(self.on_period_change)
        layout.addWidget(self._period_widget)
        # timer status
        self._timer_status_widget = QLabel()
        layout.addWidget(self._timer_status_widget)
        # data update button
        self._source_enable_button = _config(QPushButton("Data Update"), setCheckable = True)
        layout.addWidget(self._source_enable_button)
        # generator button
        self._generator_enable_button = _config(QPushButton("Generator"), setCheckable = True)
        self._generator_enable_button.toggled.connect(self.on_enable_generator_toggle)
        layout.addWidget(self._generator_enable_button)
        # servo control buttons
        control_on = QPushButton('Control ON')
        control_on.pressed.connect(lambda: self._cmd_queue.extend([lambda servo: servo.torque_on(tryouts=1), lambda servo: servo.status(tryouts=1)]))
        control_off = QPushButton('Control OFF')
        control_off.pressed.connect(lambda: self._cmd_queue.extend([lambda servo: servo.torque_off(tryouts=1), lambda servo: servo.status(tryouts=1)]))
        get_status = QPushButton('Get Status')
        get_status.pressed.connect(lambda: self._cmd_queue.append(lambda servo: servo.status(tryouts=1)))
        clear_status = QPushButton('Clear Status')
        clear_status.pressed.connect(lambda: self._cmd_queue.append(lambda servo: servo.clear_status(tryouts=1)))
        for widget in (control_on, control_off, get_status, clear_status):
            layout.addWidget(widget)
        # cmd status
        self._servo_status_widget = _config(QLabel(), setFixedWidth=400)
        layout.addWidget(self._servo_status_widget)
        # add everyhing to widget
        widget = _config(QWidget(), setLayout = layout)
        return _config(QDockWidget(), setWidget = widget, setWindowTitle = 'Controls')

    #
    # menu toolbar
    #
    def on_save_data(self):
        # nothing to save
        if self._data is None:
            return
        #select file
        file_name, _ = QFileDialog.getSaveFileName(self, "Save Plot Data", "", "NumPy Files (*.npy);;All Files (*)")
        if file_name is not None:
            np.save(file_name, self._data)

    #
    # ovwerride
    #
    def closeEvent(self, event):
        self.cm_server.shutdown()
        event.accept()
    
    #
    # control toolbar events
    #
    def on_period_change(self, period):
        self.timer.start(int(1000*period))
        # TODO: reconfigure cm_server

    def on_generator_change(self, index):
        # disable generator
        self._generator_enable_button.setChecked(False)

    def on_enable_generator_toggle(self, checked):
        if checked:
            generator = self._generator_tabs.currentWidget()
            generator.reset()

    #
    # update event
    #
    def set_servo_status(self, status_error, status_detail):
        control_on = bool(status_error & status_error.CONTROL_ON)
        control_on_text = 'ON' if control_on else 'OFF'
        # display status
        status_error &= ~status_error.CONTROL_ON
        status_detail &= ~status_detail.VERSION_BIT
        text = f'Servo {control_on_text}: {status_error.to_string()} {status_detail.to_string()}'
        self._servo_status_widget.setText(text)

    def update(self):
        last_resp = None
        # generators
        if self._generator_enable_button.isChecked():
            # apply generator step
            generator = self._generator_tabs.currentWidget()
            resp = generator.update_control(self.servo)
            last_resp = resp if resp is not None else last_resp
        # sources
        if self._source_enable_button.isChecked():
            # get data from selected source
            source = self._source_tabs.currentWidget()
            data, resp = source.update_monitor(self.servo)
            last_resp = resp if resp is not None else last_resp
            # update plot and save data
            if data is not None:
                self.scope_widget.update_plot(data)
                self._data = data
        # pocess config and debug requests
        if len(self._cmd_queue) == 0:
            self.cm_server.spin()
        else:
            cmd = self._cmd_queue.pop(0)
            try:
                resp = cmd(self.servo)
                last_resp = resp if resp is not None else last_resp
            except ProtocolError:
                pass
        # display servo status
        if last_resp is not None:
            self.set_servo_status(last_resp.status_error, last_resp.status_detail)
        # check timer state
        remaining_time = self.timer.remainingTime()
        if remaining_time > 0:
            self._timer_status_widget.setText(f'OK (remains {remaining_time:4d} ms)')
        else:
            self._timer_status_widget.setText(f'OVERFLOW  by {remaining_time:4d} ms')

#
# argument parsing
#

parser = ArgumentParser(description='GUI tool to monitor and test Herkulex servo.')
def servo_id(servo_id):
    return int(servo_id, 0)
parser.add_argument('servo_id', metavar='SERVO_ID', type=servo_id, help='Servo ID.')
parser.add_argument('-d', '--dev', default='/dev/ttyUSB0' , help='Serial bus device.')
parser.add_argument('-b', '--baudrate', type=int, default=115200, help='Serial bus baudrate: 57600, 115200, 223400, 460800, 921600 and etc.')
parser.add_argument('-p', '--period', type=float, default=0.05, help='Exchange period.')

args = parser.parse_args(sys.argv[1:])

#
# logger
#
logging.basicConfig(format="%(asctime)s %(name)s - %(levelname)s: %(message)s")
# logging.getLogger('protocol.bus').setLevel(logging.DEBUG)

#
# start application
#
app = QApplication([])
serial_bus = SerialBusHandle(args.dev, args.baudrate)
main = Scope(args.period, serial_bus, args.servo_id)
main.show()
app.exec()

