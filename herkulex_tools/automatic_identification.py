import time
import numpy as np
import logging

from herkulex import *
from herkulex.servos import *

#
# Logging facilities
#

log = logging.getLogger('identification')

#
# identification routines
#

def identificate_resistance(servo, voltage_cmd_step = 0.1, voltage_cmd = 0.2, target_speed = 30.0, target_current = 0.5, max_current = 1.0, experiment_timeout = 20.0, stopped_timeout = 0.5, stopped_pos_diff = 0.5, period = 0.1, write_eep = True):
    # reset servo configuration and turn it on
    log.info(f'Prepare servo {servo.name} with ID {servo.servo_id}.')
    servo.reset()
    time.sleep(0.25)
    servo.write_ram('operation_mode', 0)
    servo.write_ram('max_avg_current', max_current)
    servo.write_ram('resistance', 1.0)
    servo.write_ram('torque_policy', servo.StatusError.VOLTAGE | servo.StatusError.OVERLOAD | servo.StatusError.TEMPERATURE)
    servo.torque_on()

    # move to limit
    try:
        state_prev = servo.read_ram(['position', 'speed', 'current'])
        time_now = time.time()
        experiment_deadline = time_now + experiment_timeout
        stopped_deadline = time_now + stopped_timeout
        cycle_deadline = time_now + period
        while True:
            time_now = time.time()
            # exchange data with servo
            servo.rt_write_one(servo.ControlMode.CURRENT, 0.0, 0.0, voltage_cmd)
            state = servo.read_ram(['position', 'speed', 'current'])
            # check movement
            pos_diff = state.register_values[0].value - state_prev.register_values[0].value
            is_moving = pos_diff > stopped_pos_diff
            # check status
            status_error = not (state.status_error & servo.StatusError.CONTROL_ON)
            current_reached = state.register_values[2].value > target_current
            speed_reached = abs(pos_diff) / period > target_speed
            # perform desition
            if status_error:
                # determine reason
                if state.status_error == servo.StatusError.VOLTAGE:
                    raise RuntimeError(f'Identification failed: under/overvoltage. Check DC power supply, use poswer supply with greater output current.')
                elif state.status_error == servo.StatusError.TEMPERATURE:
                    raise RuntimeError(f'Identification failed: overheat. Decrease current settings, wait until servo is colled.')
                elif state.status_error == servo.StatusError.OVERLOAD:
                    raise RuntimeError(f'Identification failed: overload. Decrease voltage step or target speed.')
                else:
                    raise RuntimeError(f'Identification failed: {status.status_error.to_string()}.')
            # increase voltage if current or speed is not reached
            if not current_reached and not speed_reached:
                voltage_cmd += voltage_cmd_step
            # detect if servo is stopped on obstacle
            if (not is_moving) and current_reached:
                # check stop timeout
                if time_now > stopped_deadline:
                    break
            else:
                # reset stop deadline
                stopped_deadline = time_now + stopped_timeout
            # check experiment deadline
            if time_now > experiment_deadline:
                if speed_reached:
                    raise RuntimeError('Identification failed: the rest is not reached.')
                elif is_moving and current_reached:
                    raise RuntimeError('Identification failed: servo is still moveing when is at the rest.')
                else:
                    raise RuntimeError('Identification failed: timeout.')
            # save servo state
            state_prev = state
            # logging
            log.info(f'Moving to the rest: U = {voltage_cmd} V, I = {state.register_values[2].value} A, is moving: {is_moving}, traget current reached: {current_reached}, speed reached {speed_reached}.')
            # sleep until next period
            remaining_time = cycle_deadline - time.time()
            # print(f'period start {time_now}, period end {time.time()}, remaining time {remaining_time}')
            if remaining_time >= 0.0:
                cycle_deadline += period
                time.sleep(remaining_time)
            else:
                raise RuntimeError('Identification failed: timer overflow. Use greater period value.')
        # get experimental data 
        log.info(f'Get data via RT_DEBUG interface: fixed voltage U = {voltage_cmd} V.')
        _, data = servo.rt_debug(10, 1024)
        # estimate resitance
        R = np.average(data['voltage_pwm']) / np.average(data['current'])
        log.info(f'Identifiaction result: R = {R}')
        # write result
        if write_eep:
            log.info("Write 'resistance' EEP register.")
            servo.write_eep('resistance', R)
        #return result
        return R
    except Exception as e:
        log.error(str(e))
        raise e
    finally:
        servo.reset()

def identificate_backemf_coefficient(servo, voltage_amplitude = 1.0, voltage_amplitude_step = 0.5, target_position_amplitude = 30.0, max_position_amplitude = 120.0, signal_period = 1.0, min_speed = 60.0, n_samples = 100, experiment_timeout = 20.0, period = 0.02, write_eep = True):
    center = 180.0
    position_tolerance = 5.0
    # move servo to central position
    log.info(f'Prepare servo {servo.name} with ID {servo.servo_id}. Centering.')
    servo.reset()
    # config registers
    time.sleep(0.25)
    servo.write_ram('operation_mode', 0) # voltage-based control
    # move servo to senter position
    servo.torque_on()
    servo.rt_write_one(servo.ControlMode.PROFILE_TIME, center, 0.5, 1.0)
    time.sleep(2.2)
    # check if motion has succeed
    res = servo.read_ram('position')
    if abs(res.register_values[0].value - center) > position_tolerance:
        raise RuntimeError('Identification failed: unable to center servo.')
    # reconfigure for experiment
    servo.write_ram(['min_position', 'max_position'], [center - max_position_amplitude, center + max_position_amplitude])
    servo.write_ram('torque_policy', servo.StatusError.VOLTAGE | servo.StatusError.OVERLOAD | servo.StatusError.TEMPERATURE | servo.StatusError.POT_LIMIT)
    servo.clear_status()
    try:
        # buffers
        data = np.ndarray(shape=(n_samples,), dtype=[('position', np.float64), ('current', np.float64), ('voltage_pwm', np.float64)])
        data_index = 0
        wfreq = 2*np.pi/signal_period
        signal_period_end = signal_period
        min_position, max_position = np.inf, -np.inf
        amplitude_reached = False
        cycle_deadline = time.time() + period
        t = 0.0
        while True:
            time_now = time.time()
            # print(cycle_deadline - time_now)
            # exchange data with servo
            servo.rt_write_one(servo.ControlMode.CURRENT, 0.0, 0.0, voltage_amplitude*np.sin(wfreq * t))
            state = servo.read_ram(['position', 'speed', 'current', 'voltage', 'voltage_pwm'])
            # check status
            status_error = not (state.status_error & servo.StatusError.CONTROL_ON)
            if status_error:
                # determine reason
                if state.status_error == servo.StatusError.VOLTAGE:
                    raise RuntimeError(f'Identification failed: under/overvoltage. Check DC power supply, use poswer supply with greater output current.')
                elif state.status_error == servo.StatusError.TEMPERATURE:
                    raise RuntimeError(f'Identification failed: overheat. Decrease current settings, wait until servo is colled.')
                elif state.status_error == servo.StatusError.OVERLOAD:
                    raise RuntimeError(f'Identification failed: overload. Check if servo is stuck.')
                elif state.status_error == servo.StatusError.POT_LIMIT:
                    raise RuntimeError(f'Identification failed: move outside motion limits.')
                else:
                    raise RuntimeError(f'Identification failed: {status.status_error.to_string()}.')
            # min/max position detection
            position = state.register_values[0].value
            min_position, max_position = min(min_position, position), max(max_position, position)
            # end of signal period processing
            if signal_period_end < t:
                # logging
                log.info(f'Adjusting amplitude: Uamp = {voltage_amplitude} V, position amplitude  = {max_position - min_position} deg, amplitude is reached: {amplitude_reached}.')
                # check amplitude
                amplitude_reached = max(max_position - min_position, 0) >= 2*target_position_amplitude
                if not amplitude_reached:
                    voltage_amplitude += voltage_amplitude_step
                # reset
                signal_period_end += signal_period
                min_positon, max_position = np.inf, -np.inf
            # gather data
            if amplitude_reached:
                data[data_index] = tuple( state.register_values[k].value for k in (0, 2, 4) )
                data_index += 1
                # check if experiment is complete
                if data_index >= n_samples:
                    break
            else:
                data_index = 0
            # check experiment deadline
            if t > experiment_timeout:
                if amplitude_reached:
                    raise RuntimeError('Identification failed: not enough time to gather data. Lower n_samples or increse experiemnt_time.')
                else:
                    raise RuntimeError('Identification failed: unable to reach target amplitude.')
            # save servo state
            state_prev = state
            # sleep until next period
            remaining_time = cycle_deadline - time.time()
            # print(remaining_time)
            if remaining_time > 0:
                time.sleep(remaining_time)
            else:
                raise RuntimeError('Identification failed: timer overflow. Use greater period value.')
            cycle_deadline += period
            t += period

        # stop servo
        servo.rt_write_one(servo.ControlMode.BRAKE, 0, 0, 0)
        # process identification data
        # get servo resistanse
        res = servo.read_ram('resistance')
        R = res.register_values[0].value
        # estimate speed and back_emf
        back_emf = data['voltage_pwm'] - R*data['current']
        back_emf = back_emf[1:-1]
        speed = (data['position'][2:] - data['position'][:-2]) / (2*period)
        # select by speed treashhold
        index = abs(speed) > min_speed
        back_emf = back_emf[index]
        speed = speed[index]
        # inverse back EMF estimate
        log.info(f'Identifiaction: {len(back_emf)} samples.')
        inv_back_emf_coef = np.dot(back_emf, speed) / np.dot(back_emf, back_emf)
        log.info(f'Identifiaction result: back emf coefficien: {1/inv_back_emf_coef} Vs/deg')
        # write configuration
        if write_eep:
            log.info("Write 'inv_back_emf_coeff' EEP register.")
            servo.write_eep('inv_back_emf_coeff', inv_back_emf_coef)
        # return result
        return 1/inv_back_emf_coef
    except Exception as e:
        log.error(str(e))
        raise e
    finally:
        servo.reset()

        


    




    

    







