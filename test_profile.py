#!/usr/bin/env python3

import sys
import rospy
import itertools

from sensor_msgs.msg import JointState

from herkulex import *
from herkulex.servos import *
from herkulex.ros_bus import ROSBusServer

def usage():
    print('Usage:')
    print('%s ID1 RATE {accel|time} arg2 arg3 [CM_SLOT_DURATION]' % sys.argv[0])
    sys.exit(0)

#
# process argv
#
if len(sys.argv) < 6:
    usage()
servo_id = int(sys.argv[1], 0)
period = 1.0/float(sys.argv[2])
if sys.argv[3] == 'accel':
    mode = ServoCMC01.ControlMode.PROFILE_ACCEL
elif sys.argv[3] == 'time':
    mode = ServoCMC01.ControlMode.PROFILE_TIME
else:
    usage()
arg2 = float(sys.argv[4])
arg3 = float(sys.argv[5])
if len(sys.argv) >= 7:
    cm_slot_duration = float(sys.argv[6])
else:
    cm_slot_duration = 0.0

#
# init ros node
#
rospy.init_node("profile_test")
# joint state publisher
joints_pub = rospy.Publisher('profile_test', JointState, queue_size=1)

#
# initialization
#

# init servos
baudrate = 115200
serial_bus = SerialBusHandle('/dev/ttyUSB0', baudrate)
bus = ProtocolHandle(serial_bus)
servo = Servo(bus, servo_id)
servo.write_ram('inpos_threshold', 4.0)
servo.torque_on()
print(f'Servo: {servo}')

# provide CM service
if cm_slot_duration > 0.0:
    cm_server = ROSBusServer(serial_bus, spin_period = period, spin_duration = cm_slot_duration)
else:
    cm_server = None

# buffer
joints = JointState()
joints.name = ['real', 'reference']
joints.position = [ 0.0, 0.0 ]
joints.velocity = [ 0.0, 0.0 ]
joints.effort = [ 0.0, 0.0 ]

# loop configuration
inpos_pause = rospy.Duration(1.0)
positions = itertools.cycle([ 90.0, 270.0 ])

#
# Start motion
#
target_position = next(positions)
servo.rt_write_one(mode, target_position, arg2, arg3, tryouts = 3)
print(f'Target position: {target_position}')


#
# Main loop
#

rate = rospy.Rate(1.0/period)
inpos_timestamp = rospy.Time(0.0)
while not rospy.is_shutdown():
    try:
        timestamp = rospy.Time.now()
        # get servo status
        resp = servo.read_ram(["position_ref", "speed_ref", "current_ff", "current_ref", "position", "speed", "current"], tryouts = 1)

        # fill servo status
        joints.header.stamp = timestamp
        joints.position[0] = resp.register_values[4].value # position
        joints.velocity[0] = resp.register_values[5].value # speed
        joints.effort[0] = resp.register_values[6].value   # current
        joints.position[1] = resp.register_values[0].value # position_ref
        joints.velocity[1] = resp.register_values[1].value # speed_ref
        joints.effort[1] = resp.register_values[3].value   # current_ref
        # publish it
        joints_pub.publish(joints)

        # check inpos
        inpos = (resp.status_detail & servo.StatusDetail.INPOS) and not(resp.status_detail & servo.StatusDetail.MOVING)
        #inpos = not(resp.status_detail & servo.StatusDetail.MOVING)
        inpos_timeout = timestamp > (inpos_timestamp + inpos_pause)
        # set next target position
        if inpos and inpos_timeout:
            target_position = next(positions)
            servo.rt_write_one(mode, target_position, arg2, arg3, tryouts = 3)
            print(f'Target position: {target_position}')
        # save last timestamp
        if not inpos:
            inpos_timestamp = timestamp

        # CM slot
        if cm_server is not None:
            cm_server.spin()
    except ProtocolError as e:
        print(f'COMMUNICATION FAILED: {e}')


    # check timer overflow
    if rate.remaining().to_nsec() < 0:
        print(f'TIMER OVERFLOW: remaining time {rate.remaining().to_sec()} s')
    # sleep
    rate.sleep()


# stop servo
servo.torque_off()





