#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys, os, time
from argparse import ArgumentParser, RawDescriptionHelpFormatter

from herkulex import *
from herkulex.servos import *
from herkulex.servos.cmc01 import ServoCMCTestSensorsBase

N_SAMPLES = 1024
DEFAULT_VOLTAGES = [0, 0.8056, 1.6112]
DEFAULT_SAW_AMPLITUDES = [3.2224, 6.4448]
DEFAULT_SAW_PERIODS = [0.0026, 0.0026]
DEFAULT_STEP_AMPLITUDES = [0.2]

def save_rt_debug_data(servo, channel, exp_name, dest_dir = None):
    # get data from servo
    for tryout in range(0, 3):
        print(exp_name, '...', end='', flush=True)
        raw_data, conv_data = servo.rt_debug(channel, N_SAMPLES)
        if raw_data is None:
            print('FAILED')
            continue
        print('OK')
        break
    if raw_data is None:
        raise ProtocolError(f'RT_DEBUG request failed: servo {servo} channel {channel}')
    # save results 
    if dest_dir is not None:
        np.save(os.path.join(dest_dir, f'{exp_name}_raw.npy'), raw_data)
        np.save(os.path.join(dest_dir, f'{exp_name}.npy'), conv_data)
    # return results
    return raw_data, conv_data

def save_all_rt_debug_data(servo, exp_details, dest_dir):
    ''' Save standart dataset to dest_dir '''
    if exp_details != '':
        exp_details = '_' + exp_details
    save_rt_debug_data(servo, 2, f'position{exp_details}', dest_dir)
    save_rt_debug_data(servo, 3, f'current{exp_details}', dest_dir)
    save_rt_debug_data(servo, 4, f'voltage{exp_details}', dest_dir)
    save_rt_debug_data(servo, 5, f'temperature{exp_details}', dest_dir)
    save_rt_debug_data(servo, 6, f'all-measurements{exp_details}', dest_dir)
    save_rt_debug_data(servo, 7, f'all-filtered{exp_details}', dest_dir)
    save_rt_debug_data(servo, 8, f'ident-hf{exp_details}', dest_dir)

def experiments(servo, dest_dir, voltages = DEFAULT_VOLTAGES, saw_amplitudes = DEFAULT_SAW_AMPLITUDES, saw_periods = DEFAULT_SAW_PERIODS, step_amplitudes = DEFAULT_STEP_AMPLITUDES):
    # prepear directory
    if not os.path.isdir(dest_dir):
        os.mkdir(dest_dir)

    try:
        #
        # configuration
        #
        servo.write_ram('operation_mode', 0) # current controller is off
        servo.write_ram('resistance', 1.0) # current = voltage

        # 
        #
        # mottor off experiemnts
        #

        print()
        print(' Motor off '.center(50, '='))
        print()
        servo.torque_off()
        save_all_rt_debug_data(servo, 'off', dest_dir)

        #
        # mottor on, constant voltage experiemnts
        #
        servo.torque_on()
        for voltage in voltages:
            print()
            print(f' Motor on, voltage = {voltage} V '.center(50, '='))
            print()
            servo.rt_write_one(servo.ControlMode.CURRENT, 0, 0, voltage) 
            time.sleep(2.0)
            
            save_all_rt_debug_data(servo, f'voltage{voltage}'.replace('.', 'd'), dest_dir)


        #
        # mottor on, saw voltage experiemnts
        #
        for saw_amplitude, saw_period in zip(saw_amplitudes, saw_periods):
            print()
            print(f' Motor on, saw {saw_amplitude} V, period {saw_period} s '.center(50, '='))
            print()
            servo.start_waveform_generator(servo.ControlMode.CURRENT_SAW_GENERATOR, saw_amplitude/2, saw_period, saw_amplitude/2)
            time.sleep(2.0)
            save_all_rt_debug_data(servo, f'saw{saw_amplitude}'.replace('.','d'), dest_dir)

        #
        # mottor on, step voltage experiemnts
        #
        for step_amplitude, step_period in zip(step_amplitudes, saw_periods):
            print()
            print(f' Motor on, step {voltage} V, period {saw_period} s '.center(50, '='))
            print()
            servo.start_waveform_generator(servo.ControlMode.CURRENT_STEP_GENERATOR, step_amplitude/2, step_period, step_amplitude/2)
            time.sleep(2.0)
            save_all_rt_debug_data(servo, f'step{step_amplitude}'.replace('.','d'), dest_dir)

    finally:
        #
        # turn servo off
        #
        servo.torque_off()

# detect start as stand-alone script
if __name__ == '__main__':
    # argumnent parsing
    parser = ArgumentParser(description='Low levwl experimants with servos with SensorTest firmware. Results are written to DEST_DIR.', 
                        formatter_class=RawDescriptionHelpFormatter,
                        allow_abbrev=True)

    parser.add_argument('servo_id', metavar='ID', help='Servo ID.')
    parser.add_argument('dest_dir', metavar='DEST_DIR', help='Directory with experiemnt results (numpy structured arrays).')
    parser.add_argument('-d', '--dev', default='/dev/ttyUSB0' , help='Serial bus device.')
    parser.add_argument('-b', '--baudrate', type=int, default=115200, help='Serial bus baudrate: 57600, 115200, 223400, 460800, 921600 and etc.')
    parser.add_argument('--voltages', type=float, nargs='*', default=DEFAULT_VOLTAGES, help='Voltages for constant voltage experiments.')
    parser.add_argument('--saw-amplitudes', type=float, nargs='*', default=DEFAULT_SAW_AMPLITUDES, help='Saw amplitude for saw experiments.')
    parser.add_argument('--step-amplitudes', type=float, nargs='*', default=DEFAULT_STEP_AMPLITUDES, help='Step amplitude for step experiments.')
    parser.add_argument('--periods', type=float, nargs='*', default=DEFAULT_SAW_AMPLITUDES, help='Period for saw and step experiments.')
    # parameters
    args = parser.parse_args(sys.argv[1:])
    print(args)
    # create servo
    bus = ProtocolHandle(SerialBusHandle(args.dev, args.baudrate))
    servo = Servo(bus, args.servo_id)
    if not isinstance(servo, ServoCMCTestSensorBase):
        raise RuntimeError(f'Servo {servo}: SensorTest firmware is not detected.')
    # perform experiment sequence
    experiments(servo, args.dest_dir, voltages=args.voltages, saw_amplitudes=args.saw_amplitudes, saw_periods=args.periods, step_amplitudes=args.step_amplitudes)


