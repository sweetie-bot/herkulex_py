from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import numpy as np
import time
import itertools

from herkulex import *
from herkulex.servos.cmc01 import ServoCMCBase, ServoCMCTestSensorsBase

from .config import _config
from .servo_widget import ServoWidget


class RTDebugWidget(ServoWidget):
    supported_servos = [ ServoCMCBase, ServoCMCTestSensorsBase ]

    def __init__(self, servo):
        super(RTDebugWidget, self).__init__(servo)
        # crate layout
        layout = QFormLayout()
        # add channel selector
        self._channel_widget = QComboBox()
        for channel in range(1,17):
            self._channel_widget.addItem(str(channel), userData = channel)
        layout.addRow(QLabel('Debug channel'), self._channel_widget)
        # add status indicator
        self._status_widget = QLabel()
        layout.addRow(QLabel('Status'), self._status_widget)
        # servo status
        self._status_error_widget = QLabel()
        self._status_detail_widget = QLabel()
        layout.addRow(QLabel('Status Error'), self._status_error_widget)
        layout.addRow(QLabel('Status Detail'), self._status_detail_widget)
        # set layout
        self.setLayout(layout)

    def update_monitor_impl(self, servo):
        channel = self._channel_widget.currentData()
        # get data from servo
        raw_data, conv_data = servo.rt_debug(channel, sample_num = 1024)
        if raw_data is not None:
            if conv_data is not None:
                data = conv_data
                self._status_widget.setText('CONVERTED DATA')
            else:
                data = raw_data
                self._status_widget.setText('RAW_DATA')
        else:
            data = None
            self._status_widget.setText('FAILED')
        # servo status
        resp = None
        try:
            resp = servo.status()
            self._status_error_widget.setText(resp.status_error.to_string())
            self._status_detail_widget.setText(resp.status_detail.to_string())
        except ProtocolError:
            self._status_error_widget.setText('NOT RESPONDING')
            self._status_detail_widget.setText('NOT RESPONDING')
        # return reult 
        return data, resp

class RAMReadWidget(ServoWidget):

    def __init__(self, servo):
        super(RAMReadWidget, self).__init__(servo)
        # crate layout
        layout = QFormLayout()
        # number of samples selector 
        self._nsamples_widget = QSpinBox()
        self._nsamples_widget.setRange(100, 8000)
        self._nsamples_widget.setValue(1000)
        self._nsamples_widget.valueChanged.connect(self.on_nsamples_change)
        layout.addRow(QLabel('Buffer size'), self._nsamples_widget)
        # buffer clear
        clear_buffer_button = QPushButton('Clear buffer')
        clear_buffer_button.pressed.connect(self.clear_buffer)
        layout.addRow(QLabel(), clear_buffer_button)
        # rolling time update
        self._is_rolling_widget = QCheckBox()
        self._is_rolling_widget.setChecked(True)
        layout.addRow(QLabel('Rolling display'), self._is_rolling_widget)
        # select registers to read
        self._registers_widget = QListWidget()
        self._registers_widget.addItems([reg_info.name for reg_info in servo.registers_ram])
        self._registers_widget.setSelectionMode(QListWidget.MultiSelection)
        self._registers_widget.itemSelectionChanged.connect(self.on_registers_change)
        layout.addRow(QLabel('Registers'), self._registers_widget)
        # status
        self._status_widget = QLabel()
        layout.addRow(QLabel('Status'), self._status_widget)
        # servo status
        self._status_error_widget = QLabel()
        self._status_detail_widget = QLabel()
        layout.addRow(QLabel('Status Error'), self._status_error_widget)
        layout.addRow(QLabel('Status Detail'), self._status_detail_widget)
        # progress bar
        self._data_bar_widget = QProgressBar()
        self._data_bar_widget.setRange(0, self._nsamples_widget.value())
        self._data_bar_widget.setValue(0)
        layout.addRow(QLabel('Data buffer'), self._data_bar_widget)
        # set layout
        self.setLayout(layout)
        # data buffer,
        self._registers = []
        self._registers_sel_induces = []
        self._data = None
        self._data_index = None
        self._start_time = time.time()
        # reserve buffer
        self.on_registers_change()

    def reinit(self,servo):
        # clear and spawn again register list
        self._registers_widget.clear()
        self._registers_widget.addItems([reg_info.name in servo.registers_ram])
        self.on_registers_change()

    def on_registers_change(self):
        # get selected items
        sel_induces = [ idx.row() for idx in self._registers_widget.selectedIndexes() ]
        sel_induces.sort()
        if len(sel_induces) > 0:
            # get overall selection diapazone
            row_start = sel_induces[0]
            row_end = sel_induces[-1] + 1
            # form full registers list and selection index
            self._registers = [ self._registers_widget.item(row).text() for row in range(row_start, row_end) ]
            self._registers_sel_induces = [ row - row_start for row in sel_induces ]
        else:
            self._registers = []
            self._registers_sel_induces = []
        # construct dtype 
        # if registers list is empty buffer is still constructed to not invalidate buffer-related operations)
        dtype = np.dtype([('time', float)] + [ (self._registers[idx], float) for idx in self._registers_sel_induces ])
        # recreate data buffer
        n_samples = self._nsamples_widget.value()
        self._data = np.ndarray(shape = (n_samples,), dtype = dtype)
        self.clear_buffer()

    def on_nsamples_change(self, new_size):
        old_size = len(self._data)
        # resize array
        if new_size < old_size:
            data = np.ndarray((new_size,), dtype = self._data.dtype)
            data[0:min(self._data_index, new_size)] = self._data[max(self._data_index - new_size,0):self._data_index]
            self._data = data
            self._data_index = min(self._data_index, new_size)
        else:
            self._data = np.pad(self._data, new_size - old_size, 'constant', constant_values=(np.nan,))
        # progress bar
        self._data_bar_widget.setMaximum(len(self._data))
        self._data_bar_widget.setValue(self._data_index)

    def clear_buffer(self):
        self._data[:] = np.nan
        self._data_index = 0
        self._start_time = time.time()
        self._data_bar_widget.setValue(0)

    def update_monitor_impl(self, servo):
        is_rolling = self._is_rolling_widget.isChecked()
        # get data from servo
        try:
            if len(self._registers) > 0:
                # request registers values
                timestamp = time.time()
                # get servo status
                resp = servo.read_ram(self._registers, tryouts = 1)
                # insert index
                if self._data_index < len(self._data):
                    index = self._data_index
                    self._data_index += 1
                else:
                    if is_rolling:
                        self._data[:-1] = self._data[1:]
                        index = self._data_index - 1
                    else:
                        index = 0
                        self._data_index = 0
                        self._start_time = timestamp
                # add data
                self._data[index] = (timestamp - self._start_time,) + tuple(resp.register_values[idx].value for idx in self._registers_sel_induces)
                # progress bar
                self._data_bar_widget.setValue(self._data_index)
                # set status
                self._status_widget.setText('SUCCESS')
                self._status_error_widget.setText(resp.status_error.to_string())
                self._status_detail_widget.setText(resp.status_detail.to_string())
                # return result if necessary
                if is_rolling:
                    return self._data, resp
                elif self._data_index == len(self._data):
                    return self._data, resp
                else:
                    return None, resp
            else:
                # nothing to request, so get servo status
                resp = servo.status(tryouts = 1)
                # progress bar
                self._data_bar_widget.setValue(0)
                # set status
                self._status_widget.setText('SUCCESS')
                self._status_error_widget.setText(resp.status_error.to_string())
                self._status_detail_widget.setText(resp.status_detail.to_string())
                # retur result
                return None, resp
        except ProtocolError as e:
            self._status_widget.setText(f'FAILED: {e}')
            self._status_error_widget.setText('')
            self._status_detail_widget.setText('')
            return None, None

