def _config(obj, **kwargs):
    for attr, value in kwargs.items():
        method = getattr(obj, attr)
        if isinstance(value, (list, tuple)):
            method(*value)
        elif isinstance(value, dict):
            method(**value)
        else:
            method(value)
    return obj
