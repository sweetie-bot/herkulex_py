from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import numpy as np
import time
import itertools

from herkulex import *
from herkulex.servos.cmc01 import ServoCMCBase, ServoCMCTestSensorsBase

from .servo_widget import ServoWidget

from .config import _config

class GeneratorProfileWidget(ServoWidget):
    supported_servos = [ ServoCMCBase ]

    def __init__(self, servo):
        super(GeneratorProfileWidget, self).__init__(servo)
        # create layout
        layout = QFormLayout()
        # mode
        self._mode_widget = _config(QComboBox(), addItems=(['PROFILE_ACCEL',  'PROFILE_TIME',],))
        layout.addRow(QLabel('Mode'), self._mode_widget)
        # period 
        self._period_widget = _config(QDoubleSpinBox(), setRange=(0.0, 1000.0), setValue = 3.0)
        layout.addRow(QLabel('Motion period (s)'), self._period_widget)
        # start and stop postion
        self._start_widget = _config(QDoubleSpinBox(), setRange=(-1000.0, 1000.0), setValue=90.0)
        self._end_widget = _config(QDoubleSpinBox(), setRange=(-1000.0, 1000.0), setValue=270.0)
        layout.addRow(QLabel('First waypoint (deg)'), self._start_widget)
        layout.addRow(QLabel('Second waypoint (deg)'), self._end_widget)
        # motion prtiod
        self._speed_widget = _config(QDoubleSpinBox(), setRange=(0.0, 5000.0), setValue=180.0)
        self._accel_widget = _config(QDoubleSpinBox(), setRange=(0.0, 5000.0), setValue=360.0)
        self._time1_widget = _config(QDoubleSpinBox(), setRange=(0.0, 100.0), setValue=0.5)
        self._time2_widget = _config(QDoubleSpinBox(), setRange=(0.0, 100.0), setValue=1.0)
        layout.addRow(QLabel('Profile speed (deg/s)'), self._speed_widget)
        layout.addRow(QLabel('Profile accel (deg/s^2)'), self._accel_widget)
        layout.addRow(QLabel('Accel/deccel time (s)'), self._time1_widget)
        layout.addRow(QLabel('Constant speed time (s)'), self._time2_widget)
        # status
        self._status_widget = QLabel()
        layout.addRow(QLabel('Status'), self._status_widget)
        # set layout
        self.setLayout(layout)
        # state
        self._postions_iter = iter(itertools.cycle([self._start_widget, self._end_widget]))
        self._start_time = time.time()

    def reset(self):
        self._trigger_time = time.time() 

    def update_control_impl(self, servo):
        resp = None
        # check if trigger is reached
        current_time = time.time() 
        if self._trigger_time < current_time:
            # calculate new trigger time
            period = self._period_widget.value()
            if period > 0.0:
                self._trigger_time += (((current_time - self._trigger_time) // period) + 1) * period
            else:
                self._trigger_time = current_time
            # control mode
            mode = getattr(servo.ControlMode, self._mode_widget.currentText())
            target_position = next(self._postions_iter)
            try:
                if mode.name.startswith('PROFILE_TIME'):
                    resp = servo.rt_write_one(mode, target_position.value(), self._time1_widget.value(), self._time2_widget.value())
                else:
                    resp = servo.rt_write_one(mode, target_position.value(), self._speed_widget.value(), self._accel_widget.value())
                self._status_widget.setText('SUCCEED')
            except ProtocolError as e:
                self._status_widget.setText(f'FAILED: {e}')
        return resp


class GeneratorWaveformWidget(ServoWidget):
    supported_servos = [ ServoCMCBase, ServoCMCTestSensorsBase ]

    def __init__(self, servo):
        super(GeneratorWaveformWidget, self).__init__(servo)
        # create layout
        layout = QFormLayout()
        # mode
        self._mode_widget = _config(QComboBox(), addItems=(['POSITION', 'CURRENT'],))
        layout.addRow(QLabel('Mode'), self._mode_widget)
        # signal
        self._waveform_widget = _config(QComboBox(), addItems=(['STEP', 'SINE'],))
        layout.addRow(QLabel('Waveform'), self._waveform_widget)
        # period 
        self._period_widget = _config(QDoubleSpinBox(), setRange=(0.0, 1000.0), setDecimals = 3, setValue = 0.5)
        layout.addRow(QLabel('Period (s)'), self._period_widget)
        # signal properties
        self._position_amplitude_widget = _config(QDoubleSpinBox(), setRange=(0.0, 360.0), setValue=10.0)
        layout.addRow(QLabel('Position amplitude (deg)'), self._position_amplitude_widget)
        self._current_ff_widget = _config(QDoubleSpinBox(), setRange=(-1.0, 1.0), setDecimals=4, setValue=0.0)
        layout.addRow(QLabel('Current feedforward (As^2/deg)'), self._current_ff_widget)
        self._current_amplitude_widget = _config(QDoubleSpinBox(), setRange=(0.0, 5.0), setValue=0.2)
        layout.addRow(QLabel('Current amplitude (A)'), self._current_amplitude_widget)
        self._current_offset_widget = _config(QDoubleSpinBox(), setRange=(-5.0, 5.0), setValue=0.0)
        layout.addRow(QLabel('Current offset (A)'), self._current_offset_widget)
        # status
        self._status_widget = QLabel()
        layout.addRow(QLabel('Status'), self._status_widget)
        # set layout
        self.setLayout(layout)
        # state
        self._start_time = time.time()
        self._is_startup = True

    def reset(self):
        self._start_time = time.time()
        self._is_startup = True

    def update_control_impl(self, servo):
        resp = None
        # check if startup
        if self._is_startup:
            # reset time
            self._start_time = time.time()
            # get position
            try:
                resp = servo.read_ram('position', tryouts = 1)
                self._postions_offset = resp.register_values[0].value
                # success startup
                self._is_startup = False
                self._status_widget.setText(f'SUCCESS')
            except ProtocolError as e:
                # startup failed: try agin
                self._status_widget.setText(f'FAILED: {e}')
            return resp
        else:
            # signal generation
            mode = getattr(servo.ControlMode, self._mode_widget.currentText())
            waveform = self._waveform_widget.currentText()
            # signal properties
            period = self._period_widget.value()
            if period > 0.0:
                wfreq = 2.0*np.pi / period
                phase = wfreq * (time.time() - self._start_time)
            else:
                wfreq = 0.0
                phase = 0.0
            # signal types
            if mode.name.startswith('POSITION'):
                # position mode
                position_amplitude = self._position_amplitude_widget.value()
                current_offset = self._current_offset_widget.value()
                if waveform == 'SINE':
                    position = self._postions_offset +  position_amplitude * np.sin(phase)
                    speed = wfreq * position_amplitude * np.cos(phase)
                    accel = - wfreq**2 * position_amplitude * np.sin(phase)
                    current = self._current_ff_widget.value() * accel + current_offset
                elif waveform == 'STEP':
                    position = self._postions_offset +  position_amplitude * np.sign(np.sin(phase))
                    speed = 0.0
                    current = current_offset
                else:
                    position, speed, current = (0.0, 0.0, 0.0)
            else:
                # current mode
                current_amplitude = self._current_amplitude_widget.value()
                current_offset = self._current_offset_widget.value()
                if waveform == 'SINE':
                    current = current_offset +  current_amplitude * np.sin(phase)
                elif waveform == 'STEP':
                    current = current_offset +  current_amplitude * np.sign(np.sin(phase))
                else:
                    current = 0.0
                position, speed = (0.0, 0.0)
            # apply control
            try:
                resp = servo.rt_write_one(mode, position, speed, current)
                self._status_widget.setText('SUCCEED')
            except ProtocolError as e:
                self._status_widget.setText(f'FAILED: {e}')
            return resp

class GeneratorEmbededWidget(ServoWidget):
    supported_servos = [ ServoCMCBase, ServoCMCTestSensorsBase ]

    def __init__(self, servo):
        super(GeneratorEmbededWidget, self).__init__(servo)
        # create layout
        layout = QFormLayout()
        # mode
        self._mode_widget = _config(QComboBox(), addItems=(['CURRENT_STEP_GENERATOR', 'CURRENT_SAW_GENERATOR'],))
        layout.addRow(QLabel('Mode'), self._mode_widget)
        # geneartor parameters 
        self._current_amplitude_widget = _config(QDoubleSpinBox(), setRange=(0.0, 5.0), setValue=0.5)
        layout.addRow(QLabel('Current amplitude (A)'), self._current_amplitude_widget)
        self._current_offset_widget = _config(QDoubleSpinBox(), setRange=(-5.0, 5.0), setValue=0.0)
        layout.addRow(QLabel('Current offset (A)'), self._current_offset_widget)
        self._period_widget = _config(QDoubleSpinBox(), setRange=(0.0, 10.0), setDecimals = 4, setValue = 0.005)
        layout.addRow(QLabel('Period (s)'), self._period_widget)
        # start and stop postion
        self._mode_widget.currentIndexChanged.connect(lambda index: self.reset())
        self._current_amplitude_widget.valueChanged.connect(self.reset)
        self._current_offset_widget.valueChanged.connect(self.reset)
        self._period_widget.valueChanged.connect(self.reset)
        # status
        self._status_widget = QLabel()
        layout.addRow(QLabel('Status'), self._status_widget)
        # set layout
        self.setLayout(layout)
        # state
        self._restart = True

    def reset(self):
        self._restart = True

    def update_control_impl(self, servo):
        resp = None
        # check if restart is required
        if self._restart:
            mode = getattr(servo.ControlMode, self._mode_widget.currentText())
            amplitude = self._current_amplitude_widget.value()
            offset = self._current_offset_widget.value()
            period = self._period_widget.value()
            try:
                resp = servo.start_waveform_generator(mode, amplitude, period, offset, tryouts = 1)
                self._status_widget.setText('SUCCEED')
                self._restart = False
            except (ProtocolError, ZeroDivisionError) as e:
                self._status_widget.setText(f'FAILED: {e}')
        # return result
        return resp
