from PyQt5.QtWidgets import *

from herkulex import Servo

class ServoWidget(QWidget):
    supported_servos = [ Servo ]

    def _is_servo_supported(self, servo):
        return any(isinstance(servo, servo_type) for servo_type in self.supported_servos)

    def __init__(self, servo, *args, **kwargs):
        super(ServoWidget, self).__init__(*args, **kwargs)
        # disable widget if servo is not supported
        self.setEnabled( self._is_servo_supported(servo) )
           
    def reinit(self, servo):
        # disable widget if servo is not supported
        self.setEnabled( self._is_servo_supported(servo) )

    def update_monitor(self, servo):
        if self.isEnabled():
            return self.update_monitor_impl(servo)
        else:
            return None, None

    def update_control(self, servo):
        if self.isEnabled():
            return self.update_control_impl(servo)
        else:
            return None

    def update_monitor_impl(self, servo):
        raise NotImplementedError

    def update_control_impl(self, servo):
        raise NotImplementedError
