from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import pyqtgraph as pg

import numpy as np

class ScopeWidget(QTabWidget):
    def __init__(self):
        super(ScopeWidget, self).__init__()
        # create plot layout
        self._plot_layout = pg.GraphicsLayoutWidget()
        # table with statistic
        self._stat_table = QTableWidget()
        # setup layout
        self.addTab(self._plot_layout, 'Graph')
        self.addTab(self._stat_table, 'Stat')
        # plot-related references
        self._plot_dtype = None  # dtype compatible with current layout
        self._plots = {}         # subplots
        self._plot_lines = {}    # actual plots
        # stat-related
        self._stat_agregators = [ len, np.average, np.std, np.min, np.max ]

    def update_plot_layout(self, data):
        # initialization 
        names = list(data.dtype.names)
        # get time
        if 'time' in names:
            names.remove('time')
            time = data['time']
            time_label = 't, s'
        else:
            time = np.arange(0, len(data))
            time_label = 't, cycle'
        # plot layout style
        self._plot_layout.setBackground("w")
        # clear axes and plots
        self._plots.clear()
        self._plot_lines.clear()
        self._plot_layout.clear()
        # construct plots
        row = 0
        colors = ['b', 'r', 'g', 'm']
        first_plot = None
        for name in names:
            plot_name = name.split('_')[0]
            # subplot: it can be shared
            plot = self._plots.get(plot_name)
            if plot is None: 
                # create subplot
                plot = self._plot_layout.addPlot(row = row, col = 0)
                plot.setTitle(plot_name, style = 'k')
                plot.setLabel("bottom", time_label, style = 'k')
                plot.addLegend(labelTextColor = 'k')
                plot.showGrid(x=True, y=True)
                # add it to map 
                self._plots[plot_name] = plot
                # increase row number
                row += 1
                # link X-axes
                if first_plot is None:
                    first_plot = plot
                else:
                    plot.setXLink(first_plot)
            # draw line: is always unique
            line = plot.plot(time, data[name], name = name, pen = colors[len(plot.listDataItems())])
            self._plot_lines[name] = line
        # setup stat table
        self._stat_table.clear()
        self._stat_table.setColumnCount(1 + len(self._stat_agregators))
        self._stat_table.setHorizontalHeaderLabels(['signal name'] + [a.__name__ for a in self._stat_agregators])
        self._stat_table.setRowCount(len(names))
        self._stat_table.setSelectionMode(QTableWidget.NoSelection)
        for row, name in enumerate(names):
            self._stat_table.setItem(row, 0, QTableWidgetItem(name))
            for col in range(len(self._stat_agregators)):
                self._stat_table.setItem(row, col+1, QTableWidgetItem())

        # save data layout to detect it change
        self._plot_dtype = data.dtype

    def update_plot(self, data):
        # check if data layout is compatible
        if data.dtype != self._plot_dtype:
            # reconfigure plot layout
            self.update_plot_layout(data)
            return
        # graph names
        names = list(data.dtype.names)
        # get time
        if 'time' in names:
            names.remove('time')
            time = data['time']
        else:
            time = np.arange(0, len(data))
        # update plots
        for name in names:
            self._plot_lines[name].setData(time, data[name])
        # update stat
        for row, name in enumerate(names):
            for col, agregator in enumerate(self._stat_agregators):
                value = agregator(data[name])
                self._stat_table.item(row, col+1).setText(f'{value:.3f}')

