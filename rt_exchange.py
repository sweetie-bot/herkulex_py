#!/usr/bin/env python3

import sys, time, threading, signal
import numpy as np
from argparse import ArgumentParser, RawDescriptionHelpFormatter

from herkulex import *
from herkulex.servos import *
from herkulex.util import estimate_servo_timings, estimate_protocol_timeouts, write_servo_timings, read_servo_timings

#
# command line argument parsing
#

parser = ArgumentParser(description='Perform exchange with servo with RT_WRITE/RT_READ command.', 
                        formatter_class=RawDescriptionHelpFormatter,
                        allow_abbrev=True,
                        epilog = 
'''Reference trajectory is generated according to reference MODE and passed to servos with provided IDs with given RATE.

If IDs list includes servos which do not present physically corresponding subpackets are still added to RT_WRITE/RT_READ commands.
This allows to test how exchange happens with 'wide' command RT_WRITE/RT_READ command and estimate data loss on communication line.

Before starting exchange RT timings are calculated and configured according to selected BAUDRATE for each servo on array. This may 
be supressed by corresponding options. 

After exchange is finished bus statistic is printed. On idealline whithout packet loss following equality must hold:

    recv_pkts = number_of_actual_servos * (send_pkts / 2)

''')

def id_list(ids):
    return [int(s, 0) for s in ids.split(',')]

parser.add_argument('servo_ids', metavar='ID1[,ID2..]', type=id_list, 
                    help='Comma separated list of servos ID. If servo doe not present physically  corresponding subpackets are still added to RT_WRITE/RT_READ command.')
parser.add_argument('rate', metavar='RATE', type = float, help='Exchange rate in Hz.')
mode_parsers = parser.add_subparsers(title='Reference signal', dest='mode', metavar='MODE', required=True, help='Reference signal generator mode')
parser.add_argument('-r', '--ros', dest='use_ros', action='store_true', help='Init ROS node \'herkulex_rt_exchange\' and publish real and reference poses as sensor_msgs/JointState message on \'joint_state\' and \joint_state_ref\' topics.')
parser.add_argument('-c', '--cm_server', action='store_true', help='Add config/monitoring slot to exchnge round. Provide ROS service for forwanding protocol packets to servos.')
parser.add_argument('-p', '--preserve_timings',  action='store_true', help='Do not config servo timings accroding to baudrate.')
parser.add_argument('-d', '--dev', default='/dev/ttyUSB0' , help='Serial bus device.')
parser.add_argument('-b', '--baudrate', type=int, default=115200, help='Serial bus baudrate: 57600, 115200, 223400, 460800, 921600 and etc')
parser.add_argument('-t', '--timeout', type=int, help='Exit after specified timeout in seconds.')
none_parser = mode_parsers.add_parser('none', help='Monitor servo state.')
sine_parser = mode_parsers.add_parser('sine', help='Generate sine siganl.')
sine_parser.add_argument('ref_position_ampl', metavar='POSITION_AMPL', type=float, help='Position signal amplitude isn deg.')
sine_parser.add_argument('ref_freq', metavar='FREQ', type=float, help='Frequecy in Hq.')
sine_parser.add_argument('ref_current_ampl', metavar='CURRENT_AMPL', type=float, help='Current amplitude. Applied in antiphase to postion so can be used as feedforward term.')

#
# process argv
#
args = parser.parse_args(sys.argv[1:])
servo_ids = args.servo_ids
n_servos = len(servo_ids)
period = 1.0/args.rate

#
# initialization
#

# init servos
bus = ProtocolHandle(SerialBusHandle('/dev/ttyUSB0', args.baudrate))
# determine actual servos and turn them on
servos = []
actual_servos = []
n_actual_servos = 0
for servo_id in servo_ids:
    try:
        servo = Servo(bus, servo_id, name = f'servo_0x{servo_id:02x}') 
        # check if servo presents
        servo.status()
        # configure servo
        # servo.write_ram('torque_policy', servo.StatusError.VOLTAGE|servo.StatusError.OVERLOAD|servo.StatusError.TEMPERATURE)
        if args.mode != 'none':
            servo.torque_on()
        # add actula servo
        servos.append(servo)
        actual_servos.append(servo)
        n_actual_servos += 1
    except NoACKError:
        # append mock to servo
        servos.append( ServoCMC01(bus, servo_id, name = f'dummy_0x{servo_id:02x}') )
n_servos = len(servos)
print(f'Servos: {n_actual_servos} actual servo, {n_servos - n_actual_servos} dummy servos.')

# configura servos timings
if not args.preserve_timings:
    # estimate timings and write them to servos
    servo_timings = estimate_servo_timings(actual_servos[0], baudrate=args.baudrate)
    print(f'Write timing configuration\n{servo_timings}')
    write_servo_timings(actual_servos, servo_timings)
else:
    # get timings from first servo
    servo_timings = read_servo_timings(actual_servos[0])
    print(f'Use {actual_servos[0].name} timing configuration\n{servo_timings}')
# estimate timeouts based on timings
rt_timeout, normal_timeout = estimate_protocol_timeouts(bus, servo_timings, len(servos))
bus.processing_delay = 0.005
# estimate excahnge duration
round_duration = bus.processing_delay + rt_timeout # real time slot
if args.cm_server:
    round_duration += bus.processing_delay + normal_timeout # config/monitoring slot
# print protocol timings configuration 
print(f'Protocol rt_timeout (RT echange slot duration): min {rt_timeout} s')
print(f'Protocol normal_timeout (config/monitoring slot duration): min {normal_timeout} s')
print(f'Protocol processing_delay: {bus.processing_delay} s')
print(f'Total exchange round_duration: min {round_duration} s')
print(f'Control period: {period} s')
if round_duration > period:
    raise ValueError('Period is configured smaller then minimal round duration.')
print('Timing configuration is OK.')

#
# init ROS node
#

if args.use_ros:
    # imports
    import rospy
    from sensor_msgs.msg import JointState
    from std_msgs.msg import String
    # init node
    rospy.init_node("herkulex_rt_exhange")
    # joint state publisher
    joints_pub = rospy.Publisher('joint_state', JointState, queue_size=1)
    joints_ref_pub = rospy.Publisher('joint_state_ref', JointState, queue_size=1)

#
# protocol config/monitoring server
#
cm_server = None
if args.cm_server:
    if args.use_ros:
        from herkulex.ros_bus import ROSBusServer
        cm_server = ROSBusServer(bus.bus, spin_period = period, spin_duration = normal_timeout)
    else:
        from herkulex.zmq_bus import ZeroMQBusServer
        cm_server = ZeroMQBusServer(bus.bus, spin_period = period, spin_duration = normal_timeout)

#
# reserve buffers
#

# reference state
state_ref = ServoArrayCommand(servo=servos, mode=[s.ControlMode.POSITION for s in servos], position=np.zeros(n_servos), speed=np.zeros(n_servos), current=np.zeros(n_servos))
# state
state = ServoArrayState()
state.reserve(state_ref.servo)

if args.use_ros:
    # ros message buffer
    joint_state = JointState()
    joint_state.name = [ servo.name for servo in servos ]
    # copy references to actula buffers
    joint_state.position = state.position
    joint_state.velocity = state.speed
    joint_state.effort = state.current
    # ros message buffer
    joint_state_ref = JointState()
    joint_state_ref.name = [ servo.name for servo in servos ]
    # copy references to actula buffers
    joint_state_ref.position = state_ref.position
    joint_state_ref.velocity = state_ref.speed
    joint_state_ref.effort = state_ref.current

#
# Start preparations
#

shutdown = threading.Event()

# handle ROS shutdown 
if args.use_ros:
    rospy.on_shutdown(lambda: shutdown.set())

# handle SIGINT
def on_sigint(sig_num, frame):
    # shutdown ROS
    if args.use_ros:
        rospy.signal_shutdown('SIGINT is received.')
    # signal main loop
    shutdown.set()
signal.signal(signal.SIGINT, on_sigint)

# reset statistics
bus.bus.reset_statistics()

# read current postions
Servo.rt_read(state)
print(f'Servo initial state:\n', state)
# extract current postions
position_center = np.nan_to_num( state.position, nan = 0.0 )

#
# main loop
#

print(f'Start main loop: period {period} s')
t = 0.0
start_time = time.time()
while not shutdown.is_set():
    # assign referense position
    if args.mode == 'sine':
        state_ref.position[:] = position_center + args.ref_position_ampl * np.sin(args.ref_freq * t)
        state_ref.speed[:] = args.ref_freq * args.ref_position_ampl * np.cos(args.ref_freq * t)
        state_ref.current[:] = args.ref_current_ampl * np.sin(args.ref_freq * t)
    # rt exchange 
    if args.mode == 'none':
        state = Servo.rt_read(state, prepend_rt_write=False)
    else:
        Servo.rt_write(state_ref) 
        state = Servo.rt_read(state, prepend_rt_write=True)
    # ROS support 
    if args.use_ros:
        # publish joint state
        stamp = rospy.Time.now()
        joint_state.header.stamp = stamp
        joints_pub.publish( joint_state )
        # publish reference joint state
        joint_state_ref.header.stamp = stamp
        joints_ref_pub.publish( joint_state_ref )
    # config/monitoring slot
    if cm_server is not None:
        cm_server.spin()
    # time shift
    t += period
    # check timeout exit
    if args.timeout is not None and t > args.timeout:
        if args.use_ros:
            rospy.signal_shutdown('timeout')
        break
    # check overflow
    remaining_time = start_time + t - time.time()
    if remaining_time < 0:
        print(f'TIMER OVERFLOW: remaining time {remaining_time} s')
        # recalculate time
        remaining_time = start_time + t - time.time()
        skip_time = period*((abs(remaining_time) // period) + 1)
        remaining_time += skip_time
        t += skip_time
    # sleep
    time.sleep(remaining_time)

# print statistics
print()
print(bus.bus.statistics)

# desroy server
if cm_server is not None:
    cm_server.shutdown()

