import struct
import sys

# basic import 
from herkulex import *
from herkulex.servos import *
from herkulex.util import discover, set_servo_baudrate


# default values
baudrate = 115200
device = '/dev/ttyUSB0'
# get arguaments
argc = len(sys.argv)
if argc == 2:
	baudrate = int(sys.argv[1])
elif argc == 3:
	device = sys.argv[1]
	baudrate = int(sys.argv[2])

# create protocol handle object
bus = ProtocolHandle(SerialBusHandle(device, baudrate))
