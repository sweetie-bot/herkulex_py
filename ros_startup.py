import sys
import rospy

# basic import 
from herkulex import *
from herkulex.servos import *
from herkulex.ros_bus import ROSBusHandle

# default values
namespace = ''
# get arguaments
argc = len(sys.argv)
if argc >= 2:
    namespace = sys.argv[1]


rospy.init_node('ros_bus_client')
# create protocol handle object
bus = ProtocolHandle(ROSBusHandle(namespace))
