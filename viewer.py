from PyQt5.QtWidgets import * 
from PyQt5.QtCore import *

import sys, os
import numpy as np

from herkulex_graphic_tools.config import _config
from herkulex_graphic_tools.scope_widget import ScopeWidget

class Viewer(QMainWindow):

    def __init__(self, path):
        super(Viewer, self).__init__()
        # scope
        self._scope_widget = ScopeWidget()
        self.setCentralWidget(self._scope_widget)
        # data source
        # layut
        layout = QVBoxLayout()
        # current directory
        self._directory_label = QLabel()
        self._directory_label.setText(os.path.split(path)[-1])
        layout.addWidget(self._directory_label)
        # file widget data model
        self._file_model = QFileSystemModel()
        self._file_model.setFilter(QDir.NoDot | QDir.AllDirs | QDir.Files)
        self._file_model.setNameFilters( ('*.npy',) )
        self._file_model.setRootPath(path)
        # file widget 
        self._file_list = _config(QListView(), setModel = self._file_model)
        self._file_list.setRootIndex(self._file_model.index(path))
        self._file_list.clicked.connect(self.on_file_clicked)
        layout.addWidget(self._file_list)
        # number of colums 
        self._columns_widget = _config(QSpinBox(), setRange = (1, 10), setValue = 1000, setEnabled=False)
        self._columns_widget.valueChanged.connect(self.on_columns_change)
        layout.addWidget(self._columns_widget)
        # status
        self._status_label = _config(QLabel(), setWordWrap=True)
        layout.addWidget(self._status_label)
        # dock vidget
        file_dock_widget = _config(QDockWidget(), setWidget = _config(QWidget(), setLayout = layout))
        self.addDockWidget(Qt.RightDockWidgetArea, file_dock_widget)
        # data buffer
        self._data = None

    def plot_unstructured_data(self, data, n_cols):
        # cut imcomplete row
        n_rows = data.size // n_cols
        data = data[0: n_rows*n_cols]
        # reshape data in structured array
        dtype =  np.dtype([ (f'{k}', data.dtype) for k in range(n_cols) ]) 
        data = data.view(dtype)
        # plot
        self._scope_widget.update_plot(data)

    def on_columns_change(self, new_n_cols):
        # check if data present
        if self._data is None:
            return
        # check that data is 1D unstructured
        if self._data.dtype.fields is None and len(self._data.shape) == 1:
            self.plot_unstructured_data(self._data, new_n_cols)

    def on_file_clicked(self, index):
        # clear status label 
        # get selected item
        info = self._file_model.fileInfo(index)
        path = info.absoluteFilePath()
        if info.isDir():
            # change direcory
            self._file_model.setRootPath(path)
            self._file_list.setRootIndex(self._file_model.index(path))
            self._directory_label.setText(os.path.split(path)[-1])
            self._status_label.setText('Directory have been changed.')
        elif info.isFile():
            # load file and attenpt to process it
            try:
                data = np.load(path)
            except ValueError:
                self._status_label.setText('File does not contains numpy data.')
                return
            # save data
            self._data = data
            # check if file contains structed array
            if data.dtype.fields == None:
                if len(data.shape) == 1:
                    # attempt convert to structed array
                    self.plot_unstructured_data(data, self._columns_widget.value())
                    # display info
                    self._columns_widget.setEnabled(True)
                    self._status_label.setText(f'Unstructured 1D data: {len(data)} samples.')
                if len(data.shape) == 2:
                    # reshape data
                    n_cols = min(data.shape)
                    if n_cols > 10:
                        self._status_label.setText(f'Unstructured 2D data: to many signals to display: {data.shape} samples.')
                        return
                    dtype = np.dtype([ (f'{k}', data.dtype) for k in range(n_cols) ])
                    # plotting
                    self._scope_widget.update_plot(np.reshape(data.view(dtype), (data.size // n_cols,)))
                    # display info
                    self._columns_widget.setEnabled(False)
                    self._status_label.setText(f'Unstructured 2D data: {data.shape} samples.')
            else:
                # plot structured data
                self._scope_widget.update_plot(data)
                # disable columns widget
                self._columns_widget.setEnabled(False)
                self._status_label.setText(f'Structured data: {len(data)} records.')

#
# start application
#
app = QApplication([])
main = Viewer(QDir.currentPath())
main.show()
app.exec()
