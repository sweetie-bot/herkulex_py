import logging 
from time import sleep

from herkulex import *
from herkulex.servos import *
from herkulex.code_generator import generate_register_mapper_desc

def print_header(header):
    print()
    print(f' {header}  '.center(80, '='))
    print()

# coefficients
print_header('CONSTANTS')

print(f'POSITION_SCALE_RAW2RAD = {ServoCMC01._position_scale};')
print(f'VELOCITY_SCALE_RAW2RADS = {ServoCMC01._speed_scale};')
print(f'ACCEL_SCALE_RAW2RADS = {ServoCMC01._accel_scale};')
print(f'CURRENT_SCALE_RAW2A = {ServoCMC01._current_scale};')
print(f'TIME_SCALE_RAW2S = {ServoCMC01.T2};')
print()

# util
print_header('REGISTER MAPPER')
print(generate_register_mapper_desc(ServoCMC01, with_converters = False))

# util
print_header('REGISTER MAPPER CONVERTER')
print(generate_register_mapper_desc(ServoCMC01, with_converters = True))
