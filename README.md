Herkulex protocol library 
=========================

Main features:

* Low-level packet-centered operations.
   * `SerialBusHandle` class (implements `BusHandleInterface`).
   * Provides `send_packet()` and `receive_packet()` methods.
* Middle level command-centered operations.
   * `ProtocolHandle` class.
   * Implements protocol commands: 
       * Normal commands: `status()`, `write_ram()`, `read_ram()`, `write_eep()`, `read_eep()`, `reset()`, `rollback()`, 
	   * Control commands: `s_jog()`, `i_jog()`, `rt_read()`, `rt_write()`
	   * Debug commands: `rt_debug()`
	* For normal commands operation is completed successfully or exception is happens.
   * Timeouts and tryouts handling: both user-supplied timeouts and tryouts number or default values.
   * Timeouts for control and debug commands calculated based on bus speed and servo configuration (ServoTimings).
* High-level servo-centered operations.
   * `Servo` base class and different servo types as its subclasses (`servos` subpackage).
   * Automatic servo type detection (based on model and version pair), ack mode support.
   * Register table, unit conversions.
* `util` subpackage: servo discovery, protocol timings calculation.
* Library is oriented to use with IPython console.
   * Exceptions: `ProtocolError` and its subclasse `NoACKError` and etc
   * Global enums: AckCmd, ReqCmd. 
   * Servo type specific enums defined in servo subclasses: `StatusDetail`, `StatusError`, TorqueMode` and etc
   * Dataclasses: `HerkulexPacket`, `ServoResponse`, `RegisterValue`, `ServoTimings`, `ServoArrayState`
   * Protocol implentation: `SerialBusHandle`, `ProtocolHandle`, `Servo`
* python logging library

## Installation

`sudo apt install python3-serial python3-pip python3-numpy python3-tabulate`

## Examples

Run ipython console

    $ ipython

Initialization:

	# perform imports, create ProtocolHandle
	# script takes BAUDRATE or DEVICE and BAUDRATE pair optional arguments
	# see this script for import examples
	%run startup.py 

Add servos by hand or discover them by scanning bus:

	s = ServoDRS201(bus, 0x01)
	servos = discover(bus)

Typical servo operations:
	
	s.ack_policy(s.AckPolicy.ALL)
	s.read_all_ram()
	s.write_ram('position_ref', 180)
	s.ijog([s], [s.JogMode.POSITION], [180.0], [2.0]) # servos, jog mode, position in degrees playtime in seconds
	state = Servo.rt_read(servos.values())

Further help and examples:

    help(ProtocolHandle)
    help(Servo)


