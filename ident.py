#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt

# load raw data
filename = sys.argv[1]
data = np.load(filename)

# split raw data in time series
n_columns = 3
n_rows = data.size // n_columns
data = np.reshape(data[0:n_rows*n_columns], (n_rows, n_columns))
I = np.reshape(data[:, 0], (n_rows,1))
U = np.reshape(data[:, 1], (n_rows,1))
pwm = np.reshape(data[:, 2], (n_rows,1))

# constants
n_samples_per_period = 4
adc_duration = 20
adc_sequence_size = 4
freq_mcu = 48e6
current_lsb = 0.00148
voltage_lsb = 11 * 3.3 / 2**12 

# dependent constants
pwm_period = n_samples_per_period * adc_sequence_size * adc_duration * 4
T = (pwm_period / n_samples_per_period) / freq_mcu
pwm_lsb = 1.0 / pwm_period
N = data.shape[0]

# discret time model
#
# I[k+1] = a I[k] + b V[k] * sigma[k]
#            / 1.0, pwm[k] > (k % n_samples_per_period + 1)*(pwm_period/n_samples_per_period)
# sigma[k] = | 0.0, pwm[k] < (k % n_samples_per_period)*(pwm_period/n_samples_per_period)
#            \ (pwm[k] - (k % n_samples_per_period)*pwm_period) / (pwm_period/n_samples_per_period)
# 
# regresser vector
#
# phi(k) = [ I[k], V[k]*sigma[k] ]
# theta = [ a, b ]
#
# requipments
#
# 1. pwm(k) have the same signs
# 2. I[k] are positive
# 3. PWM period starts with first measurements

# get sigma
pwm_shift = (np.reshape(range(1,N+1), (N,1)) % n_samples_per_period) * (pwm_period/n_samples_per_period)
sigma = (np.abs(pwm) - pwm_shift) / (pwm_period/n_samples_per_period)
sigma = np.maximum(0.0, np.minimum(1.0, sigma))

# get regressors
phi = np.hstack(( I[0:N-1], U[0:N-1]*sigma[0:N-1] ))
y = I[1:N]

# solve LSQ problem
print('Indefication')
print('N = %d, freq_pwm = %f' % (N, freq_mcu/pwm_period))
theta, residual, rank, singval = np.linalg.lstsq(phi, y, rcond=None)
print('residual = %.4f, rank = %d, theta = ' % (residual, rank), theta.T)

# extract parameters
a = theta[0]
b = theta[1]
L = T/b * (voltage_lsb/current_lsb)
R = (1 - a)/b * (voltage_lsb/current_lsb)
print('Indefication results')
print('a = %.4f, b = %.4f' % (a, b))
print('L = %.2f mH, R = %.4f Ohm' % (L*1e6, R))

print('Firmware configuration')
current_frac_bits = 12
current_R = int((1-a)/b*2**current_frac_bits)
current_L_inv = int(b*2**current_frac_bits)
print('current_R = %d, current_L_inv = %d' % (current_R, current_L_inv))
a_fw = 2**current_frac_bits - (current_R*current_L_inv >> current_frac_bits)
b_fw = current_L_inv
print('a_fw = %.4f, b_fw = %0.4f' % (a_fw / 2**current_frac_bits, b_fw / 2**current_frac_bits))

# plot results
Ip = phi @ theta

plt.subplot(2, 1, 1)
plt.plot(I[1:N] * current_lsb, 'r.-')
plt.plot(Ip * current_lsb, 'b.-')

plt.subplot(2, 1, 2)
plt.plot(U[1:N] * voltage_lsb, 'r.-')

plt.show()





