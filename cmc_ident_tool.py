import sys
from argparse import ArgumentParser
import logging

from herkulex_tools.automatic_identification import *

#
# argument parsing
#

parser = ArgumentParser(description='Interactive servo motor resistance and backEMF identificatio utility.')
def servo_id(servo_id):
    return int(servo_id, 0)
parser.add_argument('servo_id', metavar='SERVO_ID', type=servo_id, help='Servo ID.')
parser.add_argument('-d', '--dev', default='/dev/ttyUSB0' , help='Serial bus device.')
parser.add_argument('-b', '--baudrate', type=int, default=115200, help='Serial bus baudrate: 57600, 115200, 223400, 460800, 921600 and etc.')
parser.add_argument('-q', '--quiet', dest='quiet', action='store_true',  help='Do not print experiemnt log.')
args = parser.parse_args(sys.argv[1:])

#
# configuation
#

# logging
logging.basicConfig(format="%(asctime)s %(name)s - %(levelname)s: %(message)s")
if not args.quiet:
    logging.getLogger('identification').setLevel(logging.DEBUG)
    logging.getLogger('protocol').setLevel(logging.DEBUG)

# servo
bus = ProtocolHandle(SerialBusHandle(args.dev, args.baudrate))
servo = Servo(bus, args.servo_id)

print(f'Servo: {servo}')
print()

#
# resistance 
#
print('Resistance identification.')
print(' 1. Lock the servo with the clamp.\n 2. Install horn on the axle.\n 3. The axle must lock when rotating forward.')
prompt = input('Continue? (Yes)/(N)o/(S)kip: ').lower()
if not prompt.startswith('y') and not prompt.startswith('s'):
    sys.exit(1)
# identification
if not prompt.startswith('s'):
    while True:
        R = identificate_resistance(servo, write_eep = False)
        print(f'Resistance: {R} Ohm')
        prompt = input('Write to EEP? (Y)es/(N)o/(R)peat: ').lower()
        if prompt.startswith('y'):
            servo.write_eep('resistance', R)
            print(f'EEP register is written.')
        if prompt.startswith('r'):
            continue
        break

#
# back EMF
#
print()
print('BackEMF identification.')
print('Servo will be moved in PROFILE_TIME_VOLTAGE mode. Unsure that position controller settings must be valid.')
print(' 1. Lock the servo with the clamp.\n 2. Remove horn from the axle.\n 3. The axle must be rotating freely.')
prompt = input('Continue? (Yes)/(N)o/(S)kip: ').lower()
if not prompt.startswith('y') and not prompt.startswith('s'):
    sys.exit(1)
# identification
if not prompt.startswith('s'):
    while True:
        Kemf = identificate_backemf_coefficient(servo, write_eep = False)
        print(f'BackEMF coefficient: {Kemf} Vs/deg')
        prompt = input('Write to EEP? (Y)es/(N)o/(R)peat: ').lower()
        if prompt.startswith('y'):
            servo.write_eep('inv_back_emf_coeff', 1/Kemf)
            print(f'EEP register is written.')
        if prompt.startswith('r'):
            continue
        break

