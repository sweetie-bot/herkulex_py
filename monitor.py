from herkulex.serial_bus import SerialBusHandle
from herkulex.protocol import ProtocolHandle, HerkulexPacket, ReqCmd, AckCmd
from herkulex.servo import ServoResponse
from herkulex.servos.cmc01 import ServoCMC01

# bus with servos
bus = ProtocolHandle(SerialBusHandle('/dev/ttyUSB0', 115200))
servo = ServoCMC01(bus, 0xfd)

# motior
while True:
    p = servo.read_ram('position')
    s = servo.read_ram('speed')
    res = ServoResponse(s.status_error, s.status_detail, p.register_values + s.register_values)
    print(res)
