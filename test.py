import logging 
from time import sleep

from herkulex import *
from herkulex.servos import *
from herkulex.code_generator import generate_register_mapper_desc, generate_register_table

def print_header(header):
    print()
    print(f' {header}  '.center(80, '='))
    print()

# set verbode logging
logging.basicConfig(format="%(asctime)s %(name)s - %(levelname)s: %(message)s")
logging.getLogger('protocol').setLevel(logging.DEBUG)

# util
print_header('REGISTER MAPPER')
print(generate_register_mapper_desc(ServoCMC01))
print_header('REGISTER TABLE EEP')
try:
    print(generate_register_table(ServoCMC01.registers_eep))
except ValueError as e:
    print(e)
    pass
print_header('REGISTER TABLE RAM')
try:
    print(generate_register_table(ServoCMC01.registers_ram))
except ValueError as e:
    print(e)
    pass

# bus with servos
#bus = ProtocolHandle(SerialBusHandle('/dev/ttyUSB0', 115200))
#bus = ProtocolHandle(SerialBusHandle('/dev/ttyUSB0', 921600))
bus = ProtocolHandle(SerialBusHandle('/dev/ttyUSB0', 1000000))

# simple request
print_header('PROTOCOL HANDLE')
ack = bus.status(0xfd)
print('\nstatus: \n', ack)
ack = bus.read_eep(0xfd, 0, 4)
print('\nread_eep: \n', ack)
ack = bus.rt_read([0xfd])
print(f'\nrt_read: {len(ack)} packets')
for pkt in ack:
    print(pkt)
bus.rt_write(bytearray([0xfd, 0, 0, 0, 0, 0, 0, 0]))
print(f'\nrt_write')
ack = bus.rt_write_one(0xfd, bytearray([0xfd, 0, 0, 0, 0, 0, 0, 0]))
print(f'\nrt_write_one: ', ack)
ack = bus.read_ram(0xfd, 51, 6)
print('\nread_ram: ', ack)
#ack = bus.rt_debug(0xfd, 2, 500)
#if ack is not None:
    #print(f'\nrt_debug: read {len(ack)} samples.')
#else:
    #print(f'\nrt_debug: None')
    #sleep(5.0)
ack = bus.reset(0xfd)
print('reset :', ack)
sleep(1.0)

# servo class
print_header('SERVO OBJECT')
servo = ServoCMC01(bus, 0xfd)
res = servo.status()
print('\nstatus: \n', res)
res = servo.read_ram('temperature')
print('\nread_ram: \n', res)
res = servo.read_ram(['position', 'speed'])
print('\nread_ram: \n', res)
res = servo.write_ram('torque_mode', servo.TorqueMode.TORQUE_ON)
print('\nwrite_ram: \n', res)
res = servo.rt_write_one(servo.ControlMode.FREE, 180.0, 0.0, 0.0)
print('\nrt_write: \n', res)
try:
    res = servo.write_ram('position', 0)
    print('write_ram: \n', res)
except Exception as e:
    print('write_ram exception: \n', e)
res = servo.read_ram(['position_ref', 'speed_ref', 'current_ff'])
print('\nread_ram: \n', res)
cmd = ServoArrayCommand(servo=[servo], mode=[servo.ControlMode.FREE], position=[90.0], speed=[-10.0], current=[0.1])
Servo.rt_write(cmd)
print(f'\nrt_write')
servo.rt_write_one(servo.ControlMode.FREE, 0, 0, 0)
print(f'\nrt_write_one: ', ack)
state = Servo.rt_read([servo])
print('\nrt_write/read_ram: \n', state)
res = servo.read_all_ram()
print('\nread_all_ram: \n', res)
res = servo.read_all_eep()
print('\nread_all_eep: \n', res)

#
# Statistics
#
stat = bus.bus.statistics
print('\nstatistics: %s\n' % 'OK' if stat.send_pkts - stat.recv_pkts == 2 else 'FAILED')
print(stat)
