import sys
from herkulex import *
from herkulex.servos import *
from herkulex.util import *
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from tabulate import tabulate

#
# command argumants parsing
#
parser = ArgumentParser(description='Estimate and modify servo baudrate and timing configuration.', 
                        formatter_class=RawDescriptionHelpFormatter,
                        allow_abbrev=True,
                        epilog = 
'''Servo timing configuration includes following register values:
 * rt_start_delay is delay between end of RT_READ REQ packet and first RT_READ ACK packet, 
 * rt_delay is time slot duration for each RT_READ ACK packet,
 * ack_timeout is timeout for normal commands.
These timings depend on baudrate configuration and firmware properties (T1 period and etc).

Servo array timings information can be used to configure communication software.
 * rt_timeout is minimal duaration for sending RT_WRITE request and performing following RT_READ 
     exchange for given number of servos. 
 * normal_timeout is overall duartion for REQ/ACK exchange with servo for typical non RT commands.
These timings depend on baudrate configuration, servo timings and OS properties (UART communication delay).
''')

parser.add_argument('servo_id', metavar='SERVO_ID', help='Servos ID. ')
parser.add_argument('-d', '--dev', default='/dev/ttyUSB0' , help='Serial bus device.')
parser.add_argument('-b', '--baudrate', type=int, default=115200, help='Serial bus baudrate: 57600, 115200, 223400, 460800, 921600 and etc.')
parser.add_argument('-n', '--new-baudrate', type=int, default=None, help='New baudrate configuration to be written to EEP and override current configuration during timing analisys')
parser.add_argument('-w', '--write', choices=['ram', 'eep', 'eep_baud'], help='Write new baudrate and servo timings to RAM or EEP.')
parser.add_argument('-a', '--print-array-timings', type=int, metavar='N', help='Print timing configuration for servo array for N servos: recomended durations of realtime slot and  config/monitoring slot ')
parser.add_argument('-o', '--os-delay', type=float, default=0.001, help='Additional OS-related delay is used for array communication timing estimation.')
parser.add_argument('-v', '--verbose', dest='be_verbose', action='store_true',  help='Verbose output.')

args = parser.parse_args(sys.argv[1:])

#
# connect to servo
#
baudrate = args.baudrate
bus = ProtocolHandle(SerialBusHandle(args.dev, baudrate))
s = Servo(bus, int(args.servo_id, 0))
if not isinstance(s, (ServoCMC01, ServoCMC01Enc)):
	raise TypeError(f'Unknown or unsupported servo type: {s}')

#
# new baudrate and servo timings
#
new_baudrate = args.new_baudrate
if new_baudrate is not None:
    baudrate = new_baudrate
    new_baudrate = s.Baudrate.from_numeric(new_baudrate)

servo_timings = estimate_servo_timings(s, baudrate, printout = args.be_verbose)
if not args.be_verbose:
    print(f'Servo timings ({baudrate} baudrate)')
    print(servo_timings)
	
#
# write baudrate and servo timings
#

if args.write == 'eep':
    if new_baudrate is not None:
	    s.write_eep('baudrate', new_baudrate)
    write_servo_timings([s,], servo_timings, target_mem = Memory.EEP)
elif args.write == 'eep_baud' and new_baudrate is not None:
	s.write_eep('baudrate', new_baudrate)
elif args.write == 'ram':
    if new_baudrate is not None:
        print('WARNING! New baudrate configuration is not written!')
    write_servo_timings([s,], servo_timings, target_mem = Memory.RAM)

#
# estimate and servo array timings
#
if args.print_array_timings is not None:
    n_servos = args.print_array_timings
    rt_timeout, normal_timeout = estimate_protocol_timeouts(bus, servo_timings, n_servos, printout = args.be_verbose)
    # estimate excahnge duration
    rt_timeout += args.os_delay  # real time slot
    normal_timeout += args.os_delay # config/monitoring slot
    round_duration = rt_timeout + normal_timeout
    # print array timings configuration 
    print()
    print(f'Servo array timings ({n_servos} servos on {baudrate} baudrate, OS delay {args.os_delay} s)')
    table = [
        [ 'rt_timeout', rt_timeout, 'RT exchange slot minimal duration (s)' ],
        [ 'rt_max_freq', 1.0/rt_timeout, 'Maximal update frequency if only RT slot is present (s)' ],
        [ 'normal_timeout', normal_timeout, 'Config/monitoring slot minimal duration (s)' ],
        [ 'round_duration', round_duration, 'Minimal control period  duration with RT and config/monitoring slot (s)' ],
        [ 'max_freq', 1.0/round_duration, 'Maximal update frequency (s)' ],
    ]
    print(tabulate(table))

