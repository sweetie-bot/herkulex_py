#!/usr/bin/env python3
import sys, operator
from functools import reduce

if len(sys.argv) < 3:
    print('%s SERVO_ID CMD [DATABYTE1 DATABYTE2 ...]' % sys.argv[0])
    print()
    print('Print herkulex protocol command in hex representation.')
    sys.exit(1)

# get user input
servo_id = int(sys.argv[1], 0)
cmd = int(sys.argv[2], 0)
data = [ int(s, 0) for s in sys.argv[3:] ]

# construct packet
packet_size = 7 + len(data)
checksum1 = (packet_size ^ servo_id ^ cmd ^ reduce(operator.xor, data, 0)) & 0xFE
checksum2 = ~checksum1 & 0xFE
pkt = [ 0xff, 0xff, packet_size, servo_id, cmd, checksum1, checksum2 ] + data

# print packet
pkt_str = str.join(' ', [ '0x%02x' % b for b in pkt ])
print(pkt_str)
