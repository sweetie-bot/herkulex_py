import sys
import atexit

# basic import 
from herkulex import *
from herkulex.servos import *
from herkulex.zmq_bus import ZeroMQBusHandle


# create protocol handle object
bus = ProtocolHandle(ZeroMQBusHandle())

# register exit hook
atexit.register(bus.bus.shutdown)

