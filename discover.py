from herkulex import *
from herkulex.servos import *
from herkulex.util import discover

print("Servo types: ", Servo.registered_servo_types())

# bus with servos
bus = ProtocolHandle(SerialBusHandle('/dev/ttyUSB0', 115200))

# discover servos
servos = discover(bus)
for servo in servos.values():
    print(servo)
