import numpy as np
from .servo import ConvBase

# temperature
# adc = 4096*R/(Rt+R) => Rt + R = (4096 / adc) * R => Rt = (4096 - adc) / adc * R
# B = ln(Rt/Rt0) / (1/T - 1/T0)  => 1/T - 1/T0 = ln( (4096 - adc) / adc ) / B
# => 1/T = (B + T0*ln( ... )) / (B T0)
# => T = (B*T0) / (B + T0*ln( ... ))
# Rt/Rt0 = exp(B*(1/T - 1/T0))
# adc = 4096/(1 + Rt/Rt0 * Rt0/R)

class ConvTermistor(ConvBase):
    ZERO_K = 273.15

    def __init__(self, dtype, T0, Rt0, B, Rd = None, adc_bits = 12, unit = '\'C'):
        self.T0 = T0 + self.ZERO_K
        self.B = B
        if Rd is not None:
            self.R_div_Rt0 = Rd / Rt0
        else:
            self.R_div_Rt0 = 1.0
        self.adc_max = 2**adc_bits
        # common fields
        super(ConvTermistor, self).__init__(dtype, unit)

    def _equal_parameters(self, other):
        return (self._dtype, self._unit) == (other._dtype, other._unit) \
                and (self.T0, self.B, self.R_div_Rt0, self.adc_max) == (self.T0, self.B, self.R_div_Rt0, self.adc_max)

class ConvTermistorUp(ConvTermistor):
    def from_target(self, raw):
        T = (self.B * self.T0) / (self.B + self.T0*np.log( (self.adc_max - raw) / raw * self.R_div_Rt0))
        return T - self.ZERO_K

    def to_target(self, value):
        Rt_div_Rt0 = np.exp(self.B*(1.0/(value + self.ZERO_K) - 1.0/self.T0))
        raw = self.max_adc * self.R_div_Rt0 / (self.R_div_Rt0 + Rt_div_Rt0)
        if raw < np.iinfo(self._dtype).min or raw > np.iinfo(self._dtype).max:
            raise ValueError(f'unable convert {value} to {self._dtype} with target unit {self._target_unit}')
        return self._dtype(raw)

    def __eq__(self, other):
        return isinstance(other, ConvTermistorUp) and self._equal_parameters(other)

# adc = 4096*Rt/(Rt+R) => Rt + R = (4096 / adc) * Rt => R = ((4096 - adc)/ adc) * Rt => Rt = adc/(4096 - adc) * R
# B = ln(Rt/Rt0) / (1/T - 1/T0)  => 1/T - 1/T0 = ln( adc/(4096 - adc) * R/Rt0 ) / B
# => 1/T = (B + T0*ln( ... )) / (B T0)
# => T = (B*T0) / (B + T0*ln( ... ))
# Rt/Rt0 = exp(B*(1/T - 1/T0))
# adc = 4096/(1 + Rt/Rt0 * Rt0/R)

class ConvTermistorDown(ConvTermistor):
    ZERO_K = 273.15

    def from_target(self, raw):
        T = (self.B * self.T0) / (self.B + self.T0*np.log( raw / (self.adc_max - raw) * self.R_div_Rt0))
        return T - self.ZERO_K

    def to_target(self, value):
        Rt_div_Rt0 = np.exp(self.B*(1.0/(value + self.ZERO_K) - 1.0/self.T0))
        raw = self.max_adc * Rt_div_Rt0 / (self.R_div_Rt0 + Rt_div_Rt0)
        if raw < np.iinfo(self._dtype).min or raw > np.iinfo(self._dtype).max:
            raise ValueError(f'unable convert {value} to {self._dtype} with target unit {self._target_unit}')
        return self._dtype(raw)

    def __eq__(self, other):
        return isinstance(other, ConvTermistorDown) and self._equal_parameters(other)

class ConvTable(ConvBase):
    ''' Convert byte-size trget type to SI value using table values. '''

    def __init__(self, dtype, table, unit = ''):
        super(ConvTable, self).__init__(dtype, unit)
        self._table = table
        if len(table) !=  256:
            raise ValueError(f'Provided table (len = {len(table)}) does not cover uint8 type range.')
        if dtype().nbytes != 1:
            raise TypeError(f'Only one-byte types are allowed but {dtype} is received.')

    def from_target(self, raw):
        index = self._dtype(raw).view(np.uint8)
        return float(self._table[index])

    def to_target(self, value):
        raw = bisect_left(self._table, value)
        if raw == len(self._table):
            raw = len(self._table) - 1
        return raw

