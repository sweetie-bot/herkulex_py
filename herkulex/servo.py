import enum
from tabulate import tabulate
from collections import namedtuple
from dataclasses import dataclass, field

import itertools 
import struct
import logging

import numpy as np

from .protocol import StatusDetail as StatusDetailV2, AckCmd

#
# Additional import
#
import sys
if sys.hexversion >= 0x30a0000: # 3.10
    from bisect import bisect_left
else:
    def bisect_left(a, x, key):
        ''' Binary search with key function '''
        lo = 0
        hi = len(a)
        while lo < hi:
            mid = (lo + hi) // 2
            if key(a[mid]) < x:
                lo = mid + 1
            else:
                hi = mid
        return lo

#
# Logging facilities
#

log = logging.getLogger('protocol')


#
# Register description
#

class Access(enum.Flag):
    """ Register access mode. """
    RO = 0x1
    WO = 0x2
    RW = RO | WO

class Memory(enum.Flag):
    """ Register memory type. """
    RAM = 0x1
    EEP = 0x2
    BOTH = RAM | EEP

class RegisterDesc(namedtuple('RegisterDesc', 'name dtype memory access converter description')):
    """ Register description record: name, data type, memory location, read/write access, value interpretation (converter), register description."""
    pass

#
#  Register value interpretation
#

class ConvBase:
    ''' Base class for  value converters. '''
    def __init__(self, dtype, unit = ''):
        ''' Base for value converter: convert values between target unit and SI unit.

        Parameters:
        ----------
        dtype: numpy.type
            Traget data type: int8, uint8, int16 and etc
        unit: str
            SI unit string.
        '''
        self._dtype = dtype
        self._unit = unit

    @property
    def unit(self):
        ''' SI unit string. '''
        return self._unit

    @property
    def dtype(self):
        ''' Traget data type. '''
        return self._dtype

    def from_target(self, raw):
        ''' Convert value from traget units to SI. '''
        raise NotImplementedError

    def to_target(self, value):
        ''' Convert value from SI to traget units. '''
        raise NotImplementedError

    def __eq__(self, other):
        ''' Check if converters are same.'''
        return self is other


class ConvScale(ConvBase):
    """ Convert target value to SI by scaling on specified coeffient. 
    """
    def __init__(self, dtype, target_unit, unit = '', fracbits = 0):
        """ Create scale converter.

        Parameters:
        -----------
        dtype: numpy.dtype
            Target data type.
        target_unit: float
            Scale coefficient: (value in SI) = target_unit * (target value)
        unit: str, defult ''
            SI unit string.
        fracbits: int, defult 0
            Number of fracbits in fixed-point target value representataion: (target value) = (traget valuwe as int) / 2**fracbits
        """

        super(ConvScale, self).__init__(dtype, unit)
        self._target_unit = target_unit / 2**fracbits

    def to_target(self, value):
        raw = value / self._target_unit
        if raw < np.iinfo(self._dtype).min or raw > np.iinfo(self._dtype).max:
            raise ValueError(f'unable convert {value} to {self._dtype} with target unit {self._target_unit}')
        return self._dtype(raw)

    def from_target(self, raw):
        return raw * self._target_unit

    @property
    def scale(self):
        ''' Scale coefficient. '''
        return self._target_unit

    def __eq__(self, other):
        ''' Check if converters are same.'''
        return isinstance(other, ConvScale) \
               and self._dtype == other._dtype and self._unit == other._unit \
               and self._target_unit == other._target_unit

    def __repr__(self):
        return f'ConvScale({self._dtype.__name__} <-> float, 1 LSB = {self._target_unit} {self._unit})'

class ConvDerived(ConvScale):
    """ Scale register vaule. Scale coefficient is calculated from other converters. """
    def __init__(self, dtype, mul = None, div = None, fracbits = 0, unit = None):
        """ Derive scale converter from other converters.

        Assume we have current and voltage converters, so  converter for resistanse can be derived:

            resistance_conv = ConvDerived(np.uint16, mul=voltage_conv, div=current_conv, unit='Ohm', fracbits=8)

        this means that inside targget code

            voltage = (resistance * current) >> 8

        Parameters:
        -----------
        dtype: numpy.dtype
            Target data type.
        mul: ConvScale or list(ConvScale)
            Multiplers converters.
        div: ConvScale or list(ConvScale)
            Dividers converters.
        unit: str, defult None
            SI unit string. If not present unit string is derived.
        fracbits: int, defult 0
            Number of fracbits in target value fixed-point representataion: (target value) = (traget valuwe as int) / 2**fracbits
        """
        target_unit = 1.0
        unit_derived = ''
        # multiplication 
        if mul is not None:
            if not isinstance(mul, (list, tuple)):
                mul = [ mul ]
            # process list
            for m in mul:
                if isinstance(m, ConvScale):
                    target_unit *= m._target_unit
                    unit_derived += m._unit
                elif isinstance(m, (int, float, np.number)):
                    target_unit *= m
        # division
        if div is not None:
            if not isinstance(div, (list, tuple)):
                div = [ div ]
            # add division to unit
            unit_derived += '/' if unit_derived != '' else '1/'
            # process list
            for d in div:
                if isinstance(d, ConvScale):
                    target_unit /= d._target_unit
                    unit_derived += d._unit
                elif isinstance(m, (int, float, np.number)):
                    target_unit /= d
        # fractions
        target_unit /= 2**fracbits
        # units
        if unit is None:
            unit = unit_derived
        # supercalss
        super(ConvDerived, self).__init__(dtype = dtype, target_unit = target_unit, unit = unit)

class ConvEnum(ConvBase):
    """ Interpret target value as an enum or a flag set.
    """
    def __init__(self, dtype, enum_type):
        """ Create converter.

        Parameters:
        -----------
        dtype: numpy.dtype
            Target data type.
        dtype: IntFlagWithDescription or IntEnumWithDescription
            Target value interpretation as flags or enum.
        """ 
        super(ConvEnum, self).__init__(dtype)
        self._enum_type = enum_type

    @property
    def enum_type(self):
        """ Underlying enum type. """
        return self._enum_type

    def to_target(self, value):
        """ Convert enum type to target value. """
        if not isinstance(value, self._enum_type):
            value = self._enum_type(value)
        return self._dtype(value.value)

    def from_target(self, raw):
        """ Convert target value to enum type. """
        return self._enum_type.__new__(self._enum_type, int(raw))

    def __eq__(self, other):
        ''' Check if converters are same.'''
        return isinstance(other, ConvEnum) \
                and self._enum_type is other._enum_type

    def __repr__(self):
        return f'ConvEnum({self._dtype.__name__} <-> {self._enum_type.__name__})'

#
# Dataclasses
#

# Register value

class RegisterValue(namedtuple('RegisterValue', ['raw', 'value', 'reg_info'])):
    """ Register content representation. 
        raw: int
            Target value (raw register value).
        value: float or None
            Converted register value.
        reg_info: RegisterDecoder.RegisterInfo
            Related register description.
    """

    @property
    def unsigned_raw(self):
        """ Register content as unsigned. """
        return np.dtype('uint' + str(self.reg_info.width*8)).type(self.raw)

    @property
    def str_value(self):
        """ Human redable representation of value """
        conv = self.reg_info.converter 
        unit = conv.unit if conv is not None else ''
        value = self.value
        if isinstance(value, (float, np.floating)):
            return f'{value:5f} {unit}'
        elif isinstance(value, enum.Enum):
            return str(value)
        elif value is None:
            return ''
        else:
            return f'{value} {unit}'

    def __repr__(self):
        return f'RegisterValue(name={self.reg_info.name}, raw={self.raw}, value={self.value})'

    def __str__(self):
        return f'Register "{self.reg_info.name}": dec={self.raw}, hex=0x{self.unsigned_raw:02x}, value={self.str_value}'

# Servo responce and state

@dataclass(frozen = True)
class ServoResponse:
    """ Servo response representation.
        cmd: IntEnumWithDescription
            Protocol command.
        status_error, status_detail: IntFlagWithDescription
            Servo status 
        registers: list(RegisterValue) or None
            Read register values.
    """
    cmd: int
    status_error: int
    status_detail: int
    register_values: list = None

    def __getitem__(self, name):
        for reg_val in register_values:
            if reg_val.name == name:
                return reg_val
        raise KeyError(f'register {name} value is not present in servo response.')

    def __repr__(self):
        if self.cmd == AckCmd.STAT:
            # print detailed status description
            table = []
            for error_flag, detail_flag in zip(self.status_error.members.values(), self.status_detail.members.values()):
                table.append(["%d" % bool(self.status_error & error_flag), error_flag.name, "%d" % bool(self.status_detail & detail_flag), detail_flag.name])
            return tabulate(table, headers=['0x%02x' % self.status_error, 'Status Error', '0x%02x' % self.status_detail, 'Status Detail'])
        elif self.register_values is None:
            # check for error status
            if (self.status_detail & StatusDetailV2.VERSION_BIT) and (self.status_detail & StatusDetailV2.PROTOCOL_ERROR_MASK):
                # error packet (normally does not happen)
                status_protocol = StatusDetailV2(self.status_detail & StatusDetailV2.PROTOCOL_ERROR_MASK)
                return f'ERROR {self.cmd.name}. Operation has FAILED: {status_protocol}.\nStatus Error: {self.status_error.to_string()}\nStatus Detail: {self.status_detail.to_string()}'
            else:
                # normal ack packet
                return f'ACK {self.cmd.name}. Operation has SUCCEED.\nStatus Error: {self.status_error.to_string()}\nStatus Detail: {self.status_detail.to_string()}'
        else:
            # print register table 
            output = f'ACK {self.cmd.name}. Operation has SUCCEED.\n'
            table = []
            for reg_value in self.register_values:
                reg_info = reg_value.reg_info
                table.append([reg_info.address, reg_info.name, reg_value.raw, f'0x{reg_value.unsigned_raw:02x}', reg_value.str_value])
            output += tabulate(table, headers=['Address', 'Register Name', 'Raw Dec', 'Raw Hex', 'Value'])
            output += f'\nStatus Error: {self.status_error.to_string()}\nStatus Detail: {self.status_detail.to_string()}'
            return output

def _make_alias(name):
    return property(lambda self: getattr(self, name), lambda self, value: setattr(self, name, value))

# Servo array control command
@dataclass
class ServoArrayCommand:
    """ Control command for RT_WRITE. Dataclass object with fields: 'servo', 'mode', 'arg1', 'arg2', 'arg3'. 

        Each of fields is a list, all lists must have the same size.

        Attributes:
            servo (list): Servo objects, 
            mode (list, array_like): Command mode of corresponding servos.
            arg1, arg2, arg3 (list, array_like): command argumants which semantic depending on mode.

        For convinence several aliases are defined: 
            'position' -> 'arg1'
            'speed' -> 'arg2'
            'current' -> 'arg3'
            'accel' -> 'arg3'
            'time1' -> 'arg2'
            'time2' -> 'arg3'
    """
    def __init__(self, **kwargs):
        ''' ServoArrayCommand constructor. 

        Constructor do not check integrity: list can have different sizes. 

        Parameters:
        -----------
        servo: list, default []
            List of servo objects.
        mode: list or array_like, default []
            Serov control modes
        arg1, arg2, arg3, position, speed, acceleration, time1, time2: list or array_like, default []
            Control mode parameters.
        '''
        # add attributes
        for attr in ['servo', 'mode', 'arg1', 'arg2', 'arg3']:
            setattr(self, attr, [])
        # assign attributes
        for k, v in kwargs.items():
            if hasattr(self, k):
                setattr(self, k, v)
            else:
                raise KeyError(f"ServoArrayCommand does not have '{k}' attribute.")

    position = _make_alias('arg1')
    speed = _make_alias('arg2')
    current = _make_alias('arg3')
    accel = _make_alias('arg3')
    time1 = _make_alias('arg2')
    time2 = _make_alias('arg3')

    def reserve(self, servos):
        ''' Assign servo field, fill numeraical fields with zeros.

        Parameters
        ----------
        servos: list(Servo)
            List of Servo objects to assign to 'servo' field.
        '''
        self.servo = servos
        self.mode = [ 0 ] * len(servos)
        self.arg1 = [ 0.0 ] * len(servos)
        self.arg2 = [ 0.0 ] * len(servos)
        self.arg3 = [ 0.0 ] * len(servos)

    def __repr__(self):
        # form table
        table = []
        for servo, mode, arg1, arg2, arg3 in zip(self.servo, self.mode, self.arg1, self.arg2, self.arg3):
            table.append([ servo.servo_id, servo.ControlMode(mode), self.arg1, self.arg2, self.arg3 ])
        # return table
        return tabulate(table, headers = ['servo_id', 'cmd_mode', 'arg1\n(position)', 'arg2\n(speed, time1)', 'arg3\n(current, accel, time2)'])

# Servo array state
@dataclass
class ServoArrayState:
    """ Status of servo group as it received by RT_READ commands. 

        Data fields are lists of the same size. Data in same positions corespond to same servo.

        Attributes:
            servos (list): list of servos objects.
            position (list, array_like): servo positions.
            speed (list, array_like): servo velocities.
            current (list, array_like): corresponding currents.
            temperature (list, array_like): servo tempearatures.
            status_error (list, array_like): servo status flags, meaning may depends on servo.
            response_is_received (list, array_like): boolean, True is response from servo is received.
    """
    servo: list = field(default_factory = list)
    position: list = field(default_factory = list)
    speed: list = field(default_factory = list)
    current: list = field(default_factory = list)
    temperature: list = field(default_factory = list)
    status_error: list = field(default_factory = list)
    response_is_received: list = field(default_factory = list)

    def reserve(self, servos):
        ''' Assign servo field, fill numeraical fields with nan and assign status_error with zero.

        Parameters
        ----------
        servos: list(Servo)
            List of Servo objects to assign to 'servo' field.
        '''
        self.servo = servos
        self.position = [ np.nan ] * len(servos)
        self.speed = [ np.nan ] * len(servos)
        self.current = [ np.nan ] * len(servos)
        self.temperature = [ np.nan ] * len(servos)
        self.status_error = [ servo.StatusError(0) for servo in servos ]
        self.response_is_received = [ False ] * len(servos)

    def __repr__(self):
        field_names = ['position', 'speed', 'current', 'temperature', 'status_error', 'response_is_received']
        # form table list of nonempty lists and table header
        headers = ['servo_id']
        fields = []
        for field_name in field_names:
            field = getattr(self, field_name)
            if len(field) > 0:
                headers.append(field_name)
                fields.append(field)
        # form table
        table = []
        for k, servo in enumerate(self.servo):
            table.append([ servo.servo_id ] + [ str(field[k]) for field in fields ])
        # return table
        return tabulate(table, headers = headers)

#
# Register Decoders
#

class RegisterDecoder:
    """ Decode and encode register (name,value) pairs to memory region bytes. """

    RegisterInfo = namedtuple('RegisterInfo', ['num', 'name', 'dtype', 'width', 'address', 'access', 'converter', 'description'])

    def __init__(self, registers_desc, memory = Memory.RAM):
        """ Construct RegisterDecoder from memory description.

        Parameters:
        -----------
        registers_desc: list(RegisterDesc)
            Servo registers description. Registers must be ordered according to addreses. 
        memory: Memory
            Produce decoder for RAM or EEP registers.  """
        # create empty index
        self._registers_map = {}
        self._registers_list = []

        # decode register file description 
        State = enum.Enum('State', ['BEFORE', 'BLOCK', 'AFTER'])
        address = 0
        num = 0
        state = State.BEFORE
        for reg_desc in registers_desc:
            # convert from tuple to register description
            reg_desc = RegisterDesc(*reg_desc)
            # parse: BEFORE block
            if state == State.BEFORE:
                if reg_desc.memory & memory:
                    state = State.BLOCK # found first register
                else:
                    continue
            # parse: registers BLOCK
            if state == State.BLOCK:
                if not (reg_desc.memory & memory):
                    state = State.AFTER
                    continue
                else:
                    name = reg_desc.name
                    width = reg_desc.dtype().nbytes 
                    # create register info
                    reg_info = RegisterDecoder.RegisterInfo(num, name, reg_desc.dtype, width, address, reg_desc.access, reg_desc.converter, reg_desc.description)
                    num += 1
                    address += width
                    # add register
                    if self._registers_map.get(reg_desc.name) is not None:
                        raise ValueError('incorrect register description: dublicate register name %s' % name)
                    self._registers_map[name] = reg_info
                    self._registers_list.append(reg_info)
                    continue
            # parse: AFTER
            if state == State.BLOCK:
                if reg_desc.memory & memory:
                    raise ValueError('incorrect register description: register block must be continous')
            # save file size
            self._size = address

    def __getitem__(self, index):
        """ Get register by its name or number. """
        if isinstance(index, str):
            return self._registers_map[index]
        elif isinstance(index, int):
            return self._registers_list[index]

    def __iter__(self):
        return iter(self._registers_list)

    def __len__(self):
        """ Return number of registers. """
        return len(self._registers_list)

    def size_in_bytes(self):
        """ Return size of register table in bytes. """
        reg_info = self.register_list[-1]
        return reg_info.address + reg_info.width

    @property
    def register_info(self):
        """ Return register index: dict which maps name to RegisterInfo. """
        return self._registers_map

    @property
    def register_list(self):
        """ Return list of RegisterInfo tuples ordered by address memeory. """
        return self._registers_list

    def address_range(self, reg_names):
        """ Return address range corresponding to specified registers. 

        Parameters:
        -----------
        reg_names: list(str) or str:
            Register name or list of registers.

        Returns:
        --------
        address, length: int
            Register table region.

        Raises: 
        -------
        ValueError, KeyError
        """

        if isinstance(reg_names, (list, tuple)):
            # process registers list
            # first register: get its 
            reg_info = self._registers_map.get(reg_names[0])
            if reg_info is None:
                raise KeyError(f'Unknown register "{reg_names[0]}".')
            address = reg_info.address
            length = reg_info.width
            next_reg_num = reg_info.num + 1
            # rest registers: check
            for reg_name in reg_names[1:]:
                reg_info = self._registers_list[next_reg_num]
                if reg_info.name != reg_name:
                    if reg_name not in self._registers_map:
                        raise KeyError(f'Unknown register "{reg_name}".')
                    else: 
                        raise ValueError(f'Registers ({", ".join(reg_names)}) are not continously located in servo memory.')
                length += reg_info.width
                next_reg_num += 1
            # return result: register address, length
            return address, length
        else:
            # assume provided single register name: return reg_addr and width
            reg_info = self._registers_map.get(reg_names)
            if reg_info is None:
                raise KeyError(f'Unknown register "{reg_names}".')
            return reg_info.address, reg_info.width

    def encode(self, reg_names, values, convert = False):
        """ Converet register (name, value) pairs to register table region bytes.

        Parameters:
        ----------
        reg_names: list(str) or str
            Register anme or list of register names.
        values: list
            Register values in target units or SI units.
        convert: bool, defult False
            Weither values should be converted to target units.

        Returns:
        --------
        address, length: int 
            Register table region.
        data: bytes
            Region value.
        """
        if isinstance(reg_names, (list, tuple)):
            # process registers list
            # first register: get its propertuies
            reg_info = self._registers_map[reg_names[0]]
            if reg_info is None:
                raise KeyError(f'Unknown register "{reg_names[0]}".')
            address = reg_info.address
            reg_num = reg_info.num
            # rest registers: itrate over registers
            length = 0
            data = bytearray()
            for reg_name, value in zip(reg_names, values):
                reg_info = self._registers_list[reg_num]
                # check name
                if reg_info.name != reg_name:
                    if reg_name not in self._registers_map:
                        raise KeyError(f'Unknown register "{reg_name}".')
                    else: 
                        raise ValueError(f'Registers ({", ".join(reg_names)}) are not continously located in servo memory.')
                # increase length and number
                length += reg_info.width
                reg_num += 1
                # covert data
                if convert and reg_info.converter is not None:
                    value = reg_info.converter.to_target(value)
                else:
                    value = reg_info.dtype(value)
                data.extend(value.tobytes())
            # return result: register address, length, data
            return address, length, data
        else:
            # assume provided single register name
            reg_info = self._registers_map.get(reg_names)
            if reg_info is None:
                raise KeyError(f'Unknown register "{reg_names}".')
            if convert and reg_info.converter:
                value = reg_info.converter.to_target(values)
            else:
                value = reg_info.dtype(values)
            return reg_info.address, reg_info.width, value.tobytes()

    def decode(self, data):
        """ Convert READ_* packet payload to list of register (name,value) pairs.
            
            Payload structure:

                | addr | len | data1... dataN | status_error | status_detail |

        Parameters:
        ----------
        data: bytes
            Packet payload

        Returns:
        --------
        list(RegisterValue)
            Decoded registers.

        Raises:
        -------
        ValueError
            READ_* packet payload havr incorrect structure.
        """
        # check data size
        if len(data) == 0:
            return {} # error packet, do nothing
        # extract data fragment size
        address_begin = data[0]
        length = data[1]
        if len(data) != length + 4:
            raise ValueError('Incorrect data size')
        # find register
        reg_num = bisect_left(self._registers_list, address_begin, key = lambda reg_info: reg_info.address)
        # decode data fragment
        data = np.frombuffer(data[2:], dtype=np.uint8)
        regs = []
        while reg_num < len(self._registers_list):
            reg_info = self._registers_list[reg_num]
            # calculate fragment position
            shift_begin = reg_info.address - address_begin
            shift_end = shift_begin + reg_info.width
            if shift_end > length:
                break
            # extract data
            raw = data[shift_begin:shift_end].view(reg_info.dtype)[0]
            converted = reg_info.converter.from_target(raw) if reg_info.converter else None
            regs.append( RegisterValue(raw, converted, reg_info) )
            # next register
            reg_num += 1
        # return result
        return regs

    def __repr__(self):
        return tabulate([ [reg.address, reg.width, np.dtype(reg.dtype).name, reg.name, reg.description ] for reg in self._registers_list ], headers=['Address', 'Width', 'Type', 'Name', 'Description'])


class ServoDummy(namedtuple('ServoDummy', 'protocol_handle servo_id model version')):
    """ Servo of unknown type discovered on bus. """
    def __str__(self):
            return f'UnknownServo(id {self.servo_id:d} (0x{self.servo_id:02x}), model {self.model}, version {self.version} on bus {self.protocol_handle.bus})'

class Servo:
    """ Servo on specific bus with known register layout.

    Specific servo types are defined as subclasses:

        class ServoDRS101(Servo, model = , version, register = register_desc):
            pass

    Servo object can be created either by calling subclass constructor or by calling Servo factory

        bus = ProtocolHandle(SerialBusHandle('/dev/ttyUSB0', 115200))

        servo1 = ServoDRS101(bus, 0x01) # direct call subclass constructor
        servo2 = Servo(bus, 0x2) # use factory to detect servo type by (model, version) pair
        if isinstance(servo2, ServoDummy):
            print 'Unknown servo type.'

    Then different methods can be used to interact with servo: status(), read_ram(), read_eep(), write_ram(). write_eep(), reset(), rollback(), rt_debug().
    They envelop corresponding ProtocolHandle methods but accepts register names and values in SI and decode received results to SI.
            
        # get servo staus as ServoResponse
        resp = servo1.status() # raises ProtocolError if failed
        print('Servo 0x01 status:', resp.status_error)

        # read max_position from EEP as ServoResponse
        resp = servo1.read_eep('max_position')
        print('Servo 0x01 max_position: ', resp.register_values[0].value)

        # set enum value (enable control)
        servo1.write_ram('torque_mode', servo1.TorqueMode.TORQUE_ON)

        # move to target position in speed profile time-based mode: to
        servo1.rt_write_one(servo1.ControlMode.PROFILE_TIME_VOLTAGE, 180.0, 0.25, 0.5) # raises ProtocolError on error

        # RT exchange in POSITION control mode
        servos = [servo1, servo2]
        ref_state = ServoArrayCommand(servo = servos, mode = [s.ControlMode.POSITION_VOLTAGE for s in servos], position = [180.0, 90.0], speed = [0.0 10.0], current = [0.3, 0.0]) # deg, deg/s, A
        Servo.rt_write(ref_state)
        actual_state = Servo.rt_read([servo1, servo2], prepend_rt_write = True) 
        print(actual_state)
        Servo.rt_read(actual_state) # refresh actual_state in-place
        print(actual_state)
    """

    _servo_type_map = {}
   
    #
    # subclass agumentation
    #

    def __init_subclass__(cls, model, version, registers):
        """ Agument subclass with additional fields and register servo type. """
        # add model stamp
        cls.model = model
        cls.version = version
        # create registers decoders
        cls.registers_desc = tuple( RegisterDesc(*reg_desc_tuple) for reg_desc_tuple in registers )
        cls.registers_ram = RegisterDecoder(registers, Memory.RAM)
        cls.registers_eep = RegisterDecoder(registers, Memory.EEP)
        # add Enum and Flag datatypes for fast access
        added_enum_types = set()
        for reg_info in itertools.chain(cls.registers_ram, cls.registers_eep):
            if isinstance(reg_info.converter, ConvEnum):
                enum_type = reg_info.converter.enum_type
                if enum_type not in added_enum_types:
                    setattr(cls, enum_type.__name__, enum_type)
                    added_enum_types.add(enum_type)
        # register subclass
        cls._servo_type_map[(model, version)] = cls

    #
    # Servo Factory
    #

    def __new__(cls, protocol_handle, servo_id, name = None, write_ack = True):
        """ Servo factory: request servo model and version select corresponding servo type. """
        if cls is Servo:
            # read servo model and version
            pkt = protocol_handle.read_eep(servo_id, 0, 4)
            # decode
            model1, model2, version1, version2 = struct.unpack('BBBB', pkt.data[2:6])
            log.debug(f'Query servo {servo_id} ({servo_id:02x} on bus {protocol_handle.bus}: version=({version1}, {version2}), model=({model1}, {model2}).')
            # find corresponding subclass
            servo_type = cls._servo_type_map.get( (model1, version1) )
            if servo_type is None:
                # return dummy
                log.warning(f'Unknown servo {servo_id} ({servo_id:02x}) on bus {protocol_handle.bus}: version=({version1}, {version2}), model=({model1}, {model2}).')
                return ServoDummy(protocol_handle, servo_id, model = (model1, model2), version = (version1, version2))
            else:
                log.debug(f'Servo  {servo_id} ({servo_id:02x}) type is {servo_type}.')
                # return servo
                return super(Servo, cls).__new__(servo_type)
        else:
            # subclass object is being creatian: normal behavior
            return super(Servo, cls).__new__(cls)

    #
    # Constructor
    #

    def __init__(self, protocol_handle, servo_id, name = None, write_ack = True):
        """ Servo object constructor. 

        Parameters:
        ----------  
        protocol_handle: ProtocolHandle
            Protocol handle assotiated with servo bus.
        servo_id: int
            Servo ID.
        name: str or None
            Servo name.  
        write_ack: bool or None
            ACK packet is expected in response to WRITE commands if parameter is set True.
            Request current ack mode is requested from servo.
        """
        self._servo_id = servo_id
        self._bus = protocol_handle
        self._name = name
        # get ack policy
        if write_ack is None:
            resp = self.read_ram('ack_policy') 
            write_ack = bool(resp.register_values[0].value & self.AckPolicy.WRITE)
        self._write_ack = write_ack

    #
    # Basic nmethods and properties
    #

    @property
    def servo_id(self):
        """ Return servo ID. """
        return self._servo_id

    @property 
    def name(self):
        """ Get servo name. """
        return self._name

    @property
    def bus(self):
        """ Return corresponding ProtocolHandle object."""
        return self._bus

    @property
    def write_ack(self):
        """ Expect ACK packet in response to WRITE commands. 
        To change this property use ack_policy() method.
        """
        return self._write_ack

    @staticmethod
    def registered_servo_types():
        """ Dict { (model, version): ServoSubclass } with registered servo types. """
        return Servo._servo_type_map

    def __str__(self):
        return f'Servo {self.__class__.__name__}(id {self._servo_id} (0x{self._servo_id:02x}) "{self.name}") on bus {self._bus.bus}'

    #
    # Protocol commands
    #

    def read_ram(self, register, **kwargs):
        """ Read RAM registers.

        Parameters:
        -----------
        register: str or list(str)
            Register name or list of names.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse
            Decoded servo response.

        Raises:
        -------
        ProtocolError, KeyError, ValueError
            Communication failure, unknown register, bad register range.
        """
        address, length = self.registers_ram.address_range(register)
        # communicate with servo
        pkt = self._bus.read_ram(self._servo_id, address, length, **kwargs)
        # decode result
        error, detail = self._decode_status(pkt.data[-2:])
        registers = self.registers_ram.decode(pkt.data)
        return ServoResponse(pkt.cmd, error, detail, registers)

    def write_ram(self, register, value, convert = True, **kwargs):
        """ Write RAM registers.

        Parameters:
        -----------
        register: str or list(str)
            Register name or list of names.
        value: number or list(number)
            Register value or values in SI or target unit. For enum/flag registers int or corresponding enum values can be used.
        convert: bool, defult True
            Convert values from SI to target units.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse or None
            Decoded servo response with status if corresponding ACK policy is set.

        Raises:
        -------
        ProtocolError, KeyError, ValueError
            Communication failure, unknown register, bad register range.
        """
        reg_addr, length, data = self.registers_ram.encode(register, value, convert)
        # communicate with servo
        pkt = self._bus.write_ram(self._servo_id, reg_addr, data, receive_ack = self._write_ack, **kwargs)
        # decode status fields
        if self._write_ack:
            error, detail = self._decode_status(pkt.data)
            return ServoResponse(pkt.cmd, error, detail)
        else:
            return None

    def read_eep(self, register, **kwargs):
        """ Read EEP registers.

        Parameters:
        -----------
        register: str or list(str)
            Registers to request.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse
            Decoded servo response.

        Raises:
        -------
        ProtocolError, KeyError, ValueError
            Communication failure, unknown register, bad register range.
        """
        reg_info = self.registers_eep[register] 
        # communicate with servo
        pkt = self._bus.read_ram(self._servo_id, reg_info.address, reg_info.width, **kwargs)
        # decode result
        error, detail = self._decode_status(pkt.data[-2:])
        registers = self.registers_eep.decode(pkt.data)
        return ServoResponse(pkt.cmd, error, detail, registers)

    def write_eep(self, register, value, convert = True, **kwargs):
        """ Write EEP registers.

        Parameters:
        -----------
        register: str or list(str)
            Register name or list of names.
        value: number or list(number)
            Register value or values in SI or target unit. For enum/flag registers int or corresponding enum values can be used.
        convert: bool, defult True
            Convert values from SI to target units.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse or None
            Decoded servo response with status if corresponding ACK policy is set.

        Raises:
        -------
        ProtocolError, KeyError, ValueError
            Communication failure, unknown register, bad register range.
        """
        reg_addr, length, data = self.registers_eep.encode(register, value, convert)
        # communicate with servo
        pkt = self._bus.write_eep(self._servo_id, reg_addr, data, receive_ack = self._write_ack, **kwargs)
        # decode status fields
        if self._write_ack:
            error, detail = self._decode_status(pkt.data)
            return ServoResponse(pkt.cmd, error, detail)
        else:
            return None
   
    def status(self, **kwargs):
        """ Get servo status.

        Parameters:
        -----------
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse
            Decoded servo response with status.

        Raises:
        -------
        ProtocolError
            Communication failure.
        """
        # request status
        pkt = self._bus.status(self._servo_id, **kwargs)
        # decode status fields
        error, detail = self._decode_status(pkt.data)
        return ServoResponse(pkt.cmd, error, detail)

    def reset(self, **kwargs):
        """ Reset servo.

        Parameters:
        -----------
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse
            Decoded servo response with status if corresponding ACK policy is set.

        Raises:
        -------
        ProtocolError
            Communication failure.
        """
        # request status
        pkt = self._bus.reset(self._servo_id, receive_ack = self._write_ack, **kwargs)
        # decode status fields
        if self._write_ack:
            error, detail = self._decode_status(pkt.data)
            return ServoResponse(pkt.cmd, error, detail)
        else:
            return None

    def rollback(self, **kwargs):
        """ Reset servo EEP regster table to default values.

        Parameters:
        -----------
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse
            Decoded servo response with status if corresponding ACK policy is set.

        Raises:
        -------
        ProtocolError
            Communication failure.
        """
        # request status
        pkt = self._bus.rollback(self._servo_id, receive_ack = self._write_ack, **kwargs)
        # decode status fields
        if self._write_ack:
            error, detail = self._decode_status(pkt.data)
            return ServoResponse(pkt.cmd, error, detail)
        else:
            return None

    def rt_debug(self, channel, sample_num, **kwargs):
        ''' Request real-time debug data.

        Parameters:
        -----------
        channel, sample_num: int
            Debug channel numnber and desired number of samples.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.

        Returns:
        --------
        raw_data: numpy.array or None
            Received data (note that servo an reduce number of samples) or None if not all data is received. 
        decoded_data: numpy.array or None
            Decoded received data if decoder supplied in subclass.
        '''
        raw_data = self._bus.rt_debug(self._servo_id, channel, sample_num, **kwargs)
        if raw_data is not None:
            return self.decode_rt_debug(channel, raw_data)
        else:
            return raw_data, None

    #
    # Additional commands
    #

    def clear_status(self, **kwargs):
        """ Clear 'status_error' and 'status_detail' registers 

        Parameters:
        -----------
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse or None
            Decoded servo response with status if corresponding ACK policy is set.
        """
        return self.write_ram(['status_error', 'status_detail'], [0, 0], **kwargs)

    def torque_off(self, brake = False, **kwargs):
        """ Switch 'torque_mode' to FREE.

        Parameters:
        -----------
        brake: bool, default False
            Use BRAKE mode instead of FREE.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse or None
            Decoded servo response with status if corresponding ACK policy is set.
        """
        mode = self.TorqueMode.BRAKE if brake else self.TorqueMode.FREE
        return self.write_ram('torque_mode', mode, **kwargs)

    def torque_on(self, **kwargs):
        """ Switch 'torque_mode' to TORQUE_ON.

        Parameters:
        -----------
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse or None
            Decoded servo response with status if corresponding ACK policy is set.
        """
        return self.write_ram('torque_mode', self.TorqueMode.TORQUE_ON, **kwargs)

    def ack_policy(self, value, **kwargs):
        """ Set Servo 'ack_policy': and change write_ack property.

        Parameters:
        -----------
        value: int or ServoSubclass.AckPolicy
            New ack policy.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse or None
            Decoded servo response with status if corresponding ACK policy is set.
        """
        old_write_ack = self._write_ack
        try:
            self._write_ack = bool(value & self.AckPolicy.WRITE)
            return self.write_ram('ack_policy', value)
        except Exception:
            self._write_ack = old_write_ack

    def read_all_ram(self, bytes_per_packet = 10, **kwargs):
        """ Read all RAM registers.

        Parameters:
        -----------
        bytes_per_packet: int, default 10
            Read memory by chunks of given size.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------    
        ServoResponse
            Full register table,

        Raises:
        -------
        ProtocolError
            Communication error.
        """
        # calculate memory length    
        length = self.registers_ram.size_in_bytes()
        # get data from servo
        data = bytearray([0, length])
        for chunk_start in range(0, length, bytes_per_packet):
            chunk_end = min(chunk_start + bytes_per_packet, length)
            # read data
            pkt = self._bus.read_ram(self._servo_id, chunk_start, chunk_end - chunk_start, **kwargs)
            data.extend(pkt.data[2:-2])
        data.extend(b'\0\0')
        # decode data
        error, detail = self._decode_status(pkt.data[-2:])
        registers = self.registers_ram.decode(data)
        return ServoResponse(pkt.cmd, error, detail, registers)

    def read_all_eep(self, bytes_per_packet = 10, **kwargs):
        """ Read all EEP registers.

        Parameters:
        -----------
        bytes_per_packet: int, default 10
            Read memory by chunks of given size.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------    
        ServoResponse
            Full register table,

        Raises:
        -------
        ProtocolError
            Communication error.
        """
        # calculate memory length    
        length = self.registers_eep.size_in_bytes()
        # get data from servo
        data = bytearray([0, length])
        for chunk_start in range(0, length, bytes_per_packet):
            chunk_end = min(chunk_start + bytes_per_packet, length)
            # read data
            pkt = self._bus.read_eep(self._servo_id, chunk_start, chunk_end - chunk_start, **kwargs)
            data.extend(pkt.data[2:-2])
        data.extend(b'\0\0')
        # decode data
        error, detail = self._decode_status(pkt.data[-2:])
        registers = self.registers_eep.decode(data)
        return ServoResponse(pkt.cmd, error, detail, registers)

    # 
    # Group commands
    #

    @staticmethod
    def rt_read(servos, convert = True, **kwargs):
        ''' Send RT_READ command and receivew servo responses.

        Parameters:
        -----------
        servos: list(Servos) or ServoArrayState
            List of Servo to poll. If ServoArrayState is provided it is updated it in place.
        convert: bool
            Convert received data to SI unit.
        prepend_rt_write, timeout
            Additional arguments are passed to ProtocolHandle.rt_read().

        Returns:
        --------
        ServoArrayState
            Servo states structure. If 'servos' is list of servo objects it contains only received responses.
            If 'servos' is ServoArrayState reference to it is returned. Only records corresponding to 
            received packets are updated.

        '''
        if isinstance(servos, ServoArrayState):
            # get bus 
            bus = servos.servo[0].bus
            # send rt_read request and get responses
            pkts = bus.rt_read([servo._servo_id for servo in servos.servo], **kwargs)
            # decode result
            pkt_n = 0
            for k, servo in enumerate(servos.servo):
                # check servo id
                if pkt_n < len(pkts) and pkts[pkt_n].servo_id == servo._servo_id:
                    # decode packet
                    position, speed, current, temperature, status_error = servo.decode_rt_read(pkts[pkt_n].data, convert)
                    # update state
                    servos.servo[k] = servo
                    servos.position[k] = position
                    servos.speed[k] = speed
                    servos.current[k] = current
                    servos.temperature[k] = temperature
                    servos.status_error[k] = status_error
                    servos.response_is_received[k] = True
                    # increase packet number
                    pkt_n += 1
                else:
                    servos.response_is_received[k] = False
            # return result
            return servos
        else:
            # get bus 
            bus = servos[0].bus
            # send rt_read request and get responses
            pkts = bus.rt_read([servo._servo_id for servo in servos], **kwargs)
            # decode response    
            state = ServoArrayState()
            servo_it = iter(servos)
            for pkt in pkts:
                # find corresponding servo
                while True:
                    servo = next(servo_it)
                    if servo._servo_id == pkt.servo_id:
                        break
                # decode packet
                position, speed, current, temperature, status_error = servo.decode_rt_read(pkt.data, convert)
                # add servo state to array
                state.servo.append(servo)
                state.position.append(position)
                state.speed.append(speed)
                state.current.append(current)
                state.temperature.append(temperature)
                state.status_error.append(status_error)
            # return result
            return state

    @staticmethod
    def rt_write(group_cmd, convert = True):
        ''' Send RT_WRITE command.

        Parameters:
        ----------
        group_cmd: ServoArrayCommand
            Group command representation.
        convert: bool, default True
            Convert reference values from SI to target units.
        '''
        bus = group_cmd.servo[0].bus
        # caonstruct packet payload
        data = bytearray()
        for servo, mode, arg1, arg2, arg3 in zip(group_cmd.servo, group_cmd.mode, group_cmd.arg1, group_cmd.arg2, group_cmd.arg3):
            data.extend( servo.encode_rt_write_subpacket(mode, arg1, arg2, arg3, convert) )
        # send RT_WRITE command
        bus.rt_write(data)

    def rt_write_one(self, mode, arg1, arg2, arg3, convert = True, **kwargs):
        ''' Send RT_WRITE to one servo and receive confiramtion from servo.

        Parameters:
        -----------
        mode: Servo.ControlMode
            Control mode.
        arg1, arg2, arg3: float
            Control command arguments.
        convert: bool, defult True
            Convert values from SI to target units.
        timeout, tryouts
            Additional args for ProtocolHandle method.

        Returns:
        --------
        ServoResponse or None
            Decoded servo response with status if corresponding ACK policy is set.

        Raises:
        -------
        ProtocolError, KeyError, ValueError
            Communication failure, unknown register, bad register range.
        '''
        data = self.encode_rt_write_subpacket(mode, arg1, arg2, arg3, convert)
        # communicate with servo
        pkt = self._bus.rt_write_one(self._servo_id, data, receive_ack = self._write_ack, **kwargs)
        # decode status fields
        if self._write_ack:
            error, detail = self._decode_status(pkt.data)
            return ServoResponse(pkt.cmd, error, detail)
        else:
            return None

    @staticmethod
    def i_jog(servos, jog_modes, jog_cmds, playtimes, convert = True):
        ''' Send I_JOG command.

        Parameters:
        ----------
        servos: list(Servo)
            Traget servo list.
        jog_mode: list(Servo.JogMode)
            Control mode of each servo. May depends on servo type.
        jog_cmds: list(float) or list(int)
            JOG command: desired position, speed and etc in SI units or target units. May be empty if irrelevant to JOG mode.
        playtimes: list(float) or list(int)
            Execution time in SI or target units. May be empty if irrelevant in given  JOG mode.
        convert: bool, default True
            Convert jog_cmd and playtime values from SI to target units.
        '''
        bus = servos[0].bus
        # caonstruct packet payload
        data = bytearray()
        for servo, jog_cmd, jog_mode, playtime in zip(servos, jog_cmds, jog_modes, playtimes):
                data.extend( servo.encode_i_jog_subpacket(jog_mode, jog_cmd, playtime, convert) )
        # send RT_WRITE command
        bus.i_jog(data)

    @staticmethod
    def s_jog(servos, jog_modes, jog_cmds, playtime, convert = True):
        ''' Send I_JOG command.

        Parameters:
        ----------
        servos: list(Servo)
            Traget servo list. All servos in list must use the same target units for playtime. 
        jog_mode: list(Servo.JogMode)
            Control mode of each servo. May depends on servo type.
        jog_cmds: list(float) or list(int)
            JOG command: desired position, speed and etc in SI units or target units. May be empty if irrelevant to JOG mode.
        playtime: float or int
            Execution time in SI or target units.
        convert: bool, default True
            Convert jog_cmd and playtime values from SI to target units.
        '''
        bus = servos[0].bus
        # construct packet payload
        data = bytearray( servos[0].encode_i_jog_subpacket(jog_modes[0], jog_cmds[0] if len(jog_cmds) != 0 else None, playtime) )
        playtime = data.pop() 
        for servo, jog_cmd, jog_mode in zip(servos[1:], jog_cmds[1:], jog_modes[1:]):
            data.extend( servo.encode_s_jog_subpacket(jog_mode, jog_cmd, convert) )
        # send RT_WRITE command
        bus.s_jog(playtime, data)

    #
    # Debug data decoder
    #

    def decode_rt_debug(self, channel, data):
        ''' Convert raw debug data from rt_debug() command to numpy structured array in Si units

        Should be implemented in servo subclass. Use _decode_rt_debug_data() to decode 
        data with fixed layout.

        Parameters:
        -----------
        channel: int
            Number of debug channel
        data: numpy.array(dtype=np.uint16)
            Debug data.

        Returns:
        --------
        shaped_data: np.ndarray
            Structured numpy array with raw data.
        converted_data: np.ndarray
            Structured numpy array with converted data.
        '''
        return data, None


    #
    # Group commands encoders/decoders
    #

    def encode_i_jog_subpacket(self, jog_mode, jog_cmd, playtime, convert = True):
        ''' Return I_JOG supacket as bytes. Implement this method in Servo subclass to add I_JOG support.

        Parameters:
        -----------
        servo_id: int
            Servo id.
        jog_mode: ServoSubclass.JogMode or int
            Control mode.
        jog_cmd: float int
            Target position, speed, current, voltage, depending on jog_mode in SI units or target units.
        playtime: int
            Duration of motion in SI units ortarget units.
        convert: bool, default True
            Convert jog_cmd and playtime values from SI to target units.

        Return:
        -------
        bytes
            Subpacket bytes representation.
        '''
        raise NotImplementedError

    def encode_s_jog_subpacket(self, jog_mode, jog_cmd, convert = True):
        ''' Return S_JOG supacket as bytes. Implement this method in Servo subclass to add S_JOG support.

        Parameters:
        -----------
        servo_id: int
            Servo id.
        jog_mode: ServoSubclass.JogMode or int
            Control mode.
        jog_cmd: float int
            Target position, speed, current, voltage, depending on jog_mode in SI units or target units.
        convert: bool, default True
            Convert jog_cmd and playtime values from SI to target units.

        Return:
        -------
        bytes
            Subpacket bytes representation.
        '''
        raise NotImplementedError


    def encode_rt_write_subpacket(self, mode, arg1, arg2, arg3, convert = True):
        """ Encode arg1, arg2, and arg3 according to mode to RT_WRITE subpacket.

        Parameters:
        -----------
        mode: int
            Servo control mode.
        arg1, arg2, arg3: float
            Servo RT_WRITE command arguments. They represent position, speed, current, acceleratio and 
            time depending on control mode.
        convert: bool, defult True
            Convert SI values to target units.

        Raises:
        -------
        ValueError
            Values are out of range.
        """
        raise NotImplementedError

    def decode_rt_read(self, data, convert = True):
        """ Decode RT_READ packet. 

        Parameters:
        -----------
        data: bytes
            RT_READ ACK packet payload.
        convert: bool, defult True
            Convert target units to SI.

        Returns:
        --------
        position, speed, current, temperature: float or int
            Servo ststus in SI or target units.
        status_error: self.StatusError

        Raises:
        -------
        ValueError
            Incorrect data size.
        """
        raise NotImplementedError

    #
    # Helper functions
    #

    def _decode_status(self, data):
        ''' Decode servo status bytes. 

        Status semntic may be different depending servo 

        Parameters:
        -----------
        data: bytes(2)
            Pair of status bytes.

        Returns:
        --------
        status_error: ServoSubclass.StatusError
            Servo status
        status_detail: ServoSubclass.StatusDetail
            Servo status in SI or target units.
        '''
        status_error = self.StatusError(data[0])
        status_detail = self.StatusDetail(data[1])
        return status_error, status_detail

    @staticmethod
    def _decode_rt_debug_data(data, fields, T):
        """ Decode rt_debug data using provided data layout.

        Data is viewed as sequence of data records. Data is truncated to integer number of records.
        The original array is reshaped as numpy structured array. 
        Then conversation of units is performed and 'time' field is added to converted array.

        Example of usage:

            fileds = [ ('position', position_converter), ('speed', speed_conveter) ]
            shaped_data, converted_data = Servo._decode_rt_debug_data( raw_data, fields, record_sampling_period )

        Parameters:
        ----------
        data: np.ndarray(np.int16)
            rt_debug raw data.
        fields: list(tuple(str, ConvBase))
            Record layout. List of tuples ('<field_name>', converter) where converter is ConvBase subclass.
            Order of tuples specifies data record structure.
        T: float
            Records sampling period.

        Returns:
        --------
        shaped_data: np.ndarray
            Structured numpy array with raw data.
        converted_data: np.ndarray
            Structured numpy array with converted data.
        """
        def extend_dtype(dtype):
            if dtype().nbytes < 2:
                # extend dtypo to corresponding two-byte type
                return np.dtype(np.dtype(np.int8).str[:2] + '2').type
            else:
                return dtype
        # truncate data
        n_rows = len(data) // len(fields)
        data = data[0:n_rows*len(fields)]
        # shape in structured array
        shaped_data = data.view( np.dtype([ (field[0], extend_dtype(field[1].dtype)) for field in fields ]) )
        # converted data
        conv_dtype = np.dtype( [ ('time', np.float32) ] + [ (field[0], np.float32) for field in fields ] )
        conv_data = np.ndarray(shape = (n_rows,), dtype = conv_dtype)
        conv_data['time'] = np.arange(0, n_rows, dtype=np.float32) * T
        for field_name, converter in fields:
            conv_data[field_name] = converter.from_target( shaped_data[field_name] )
        # return result
        return shaped_data, conv_data


