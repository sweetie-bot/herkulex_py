# -*- coding: utf-8 -*-

import logging
from dataclasses import dataclass
from enum import IntEnum, IntFlag

import time
import struct
import numpy as np
from tabulate import tabulate

from .enum_description import IntFlagWithDescription

#
# Constants
#

BROADCAST_ID = 0xfe
# header layout constants
HEADER_LENGTH = 7
HEADER_START = b'\xFF\xFF'
HEADER_LEN_FIELD = 2
HEADER_ID_FIELD = 3
HEADER_CMD_FIELD = 4
HEADER_CHECKSUM1_FIELD = 5
HEADER_CHECKSUM2_FIELD = 6
# packets layout
RT_WRITE_SUBPACKET_LENGTH = 8
RT_READ_ACK_DATA_LENGTH = 8
RT_DEBUG_ACK_HEADER_LENGTH = 5
RT_DEBUG_SAMPLES_PER_PACKET = (50 - (HEADER_LENGTH + RT_DEBUG_ACK_HEADER_LENGTH)) // 2
S_JOG_SUBPACKET_LENGTH = 4
I_JOG_SUBPACKET_LENGTH = 5

#
# Protocol enum types 
#

class ReqCmd(IntEnum):
    ''' Request codes '''
    EEP_WRITE = 0x01,
    EEP_READ = 0x02,
    RAM_WRITE = 0x03,
    RAM_READ = 0x04,
    I_JOG = 0x05,
    S_JOG = 0x06,
    STAT = 0x07,
    ROLLBACK = 0x08,
    REBOOT = 0x09,
    RT_WRITE = 0x0A,
    RT_READ = 0x0B,
    RT_DEBUG = 0x0E,
    FLOAD = 0x0F,

class AckCmd(IntEnum):
    ''' Ack codes '''
    MASK = 0x40,
    EEP_WRITE = 0x41,
    EEP_READ = 0x42,
    RAM_WRITE = 0x43,
    RAM_READ = 0x44,
    I_JOG = 0x45,
    S_JOG = 0x46,
    STAT = 0x47,
    ROLLBACK = 0x48,
    REBOOT = 0x49,
    RT_WRITE = 0x4A,
    RT_READ = 0x4B,
    RT_DEBUG = 0x4E,
    FLOAD = 0x4F,

class StatusDetail(IntFlagWithDescription):
    ''' Protocol-related status flags. '''
    RECV_OVERFLOW = 0x04, 'Servo internal receive buffer is oveflowed'
    INVALID_REQ = 0x08, 'Malformed request packet'
    BAD_REG_RANGE = 0x10, 'Register address is out of range for given operation'
    OP_ERROR = 0x20, 'Operaion failed'
    VERSION_BIT = 0x80, 'Status version bit.'
    PROTOCOL_ERROR_MASK = 0x3c, 'Protocol error flag mask.'

#
# Logging facilities
#

log = logging.getLogger('protocol')

#
# Protocol-related Exceptions
#

class ProtocolError(Exception):
    pass

class NoACKError(ProtocolError):
    ''' Communication target is not responding. '''

    def __init__(self, req_pkt, **kwargs):
        super(NoACKError, self).__init__(**kwargs)
        self.req_pkt = req_pkt

    def __str__(self):
        return f'ACK timeout: servo 0x{self.req_pkt.servo_id:02x}, request cmd 0x{self.req_pkt.cmd:02x}.'

class OpFaliedError(ProtocolError):
    ''' Communication target returned ERROR packet (operation has failed on servo side). '''

    def __init__(self, error_pkt, **kwargs):
        super(OpFaliedError, self).__init__(**kwargs)
        self.error_pkt = error_pkt

    def __str__(self):
        status_detail = StatusDetail(self.error_pkt.data[1] & StatusDetail.PROTOCOL_ERROR_MASK)
        return f'ERROR packet received: servo 0x{self.error_pkt.servo_id:02x}, request cmd 0x{self.error_pkt.cmd:02x}, protocol error status {status_detail}.'

class InvalidPacketError(ProtocolError):
    ''' Malformed packet is received (packet internal structurte error). '''
    pass

class UnexpectedPacketError(ProtocolError):
    ''' Correct but unexpected packet is received (different cmd or address range). '''
    pass

#
# Herkulex packet representation
#

@dataclass
class HerkulexPacket:
    ''' Packet representation. '''
    servo_id: int
    cmd: int
    data: bytearray

    def is_ack_pkt(self):
        ''' Return true if packet is ACK (or ERROR) packet. '''
        return self.cmd & AckCmd.MASK

    def is_error_pkt(self):
        ''' Return true if packet is ERROR packet '''
        return (self.cmd & AckCmd.MASK) and  (len(self.data) == 2) and  (self.data[1] & StatusDetail.VERSION_BIT) and (self.data[1] & StatusDetail.PROTOCOL_ERROR_MASK)
        #return (self.cmd & AckCmd.MASK) and   # is ACK packet
                #(len(self.data) == 2) and     # payload contains only status 
                #(self.data[1] & StatusDetail.VERSION_BIT) and  # status detail version 2 is used
                #(self.data[1] & StatusDetail.PROTOCOL_ERROR_MASK) # protocol-related error flag is set

    def size(self):
        ''' Packet size in bytes (with header). '''
        return HEADER_LENGTH + len(self.data)

    def __str__(self):
        packet_size = self.size()
        s = f'HerkulexPacket(size = {packet_size}, cmd = {self.cmd.__repr__()}, servo_id = {self.servo_id}): '
        s += f'ff ff {packet_size:02x} {self.cmd:02x} {self.servo_id:02x} __ __ '
        s += str.join(' ', [ f'{byte:02x}' for byte in self.data ])
        return s

#
# Bus abstraction
#

@dataclass 
class ServoTimings:
    """ Servo timings configuration.

    * rt_start_delay --- delay between end of RT_READ REQ packet and first RT_READ ACK packet.
    * rt_delay --- time between RT_READ ACK packets of different servos.
    * ack_timeout --- normal command ACK packet timeout.
    """
    rt_start_delay: float
    rt_delay: float
    ack_timeout: float

    def __str__(self):
        return tabulate([
            ('rt_start_delay', self.rt_start_delay, 'Assigned delay between end of RT_READ request and first ACK packet.'),
            ('rt_delay', self.rt_delay, 'Assigned delay between RT_READ ACK packets.'),
            ('ack_timeout', self.ack_timeout, 'Assigned timeout for normal commands.'),
        ])

    def __repr__(self):
        return 'ServoTimings\n' + self.__str__()

class BusHandleInterface:
    ''' HerkulexPacket transfer media interface '''

    def send_packet(self, pkt):
        ''' Send HerkulexPacket over transfer media.

        Parameters
        ----------
        pkt: HerkulexPacket
            Packet to send.
        '''
        raise NotImplementedError

    def receive_packet(self, deadline):
        ''' Wait until correct packet is received or deadline is reached.

        Parameters
        ----------
        deadline: float or None
            Time of receive deadline: deadline = time.time() + timeout. 

        Return
        ------
        HerkulexPacket or None
            Received packet or None if deadline is reached,
        '''
        raise NotImplementedError

    def estimate_transfer_delay(self, req_pkt_size, ack_pkt_size):
        ''' Estimate exchange duration based on transport properties. 

            Parameters
            ----------
            recv_bytes, send_bytes: int
                Number of bytes in packet include header.

            Return
            ------
            float
                Transfer dealy in seconds.

        '''
        raise NotImplementedError

#
# Protocol 
#

class ProtocolHandle:
    ''' Herkulex protocol implementation.

        * Protocol command implementation includes sending REQ packet, waiting for ACK response and checking response correctness.
          * Normal commands (configuration and debugging): status(), read_ram(), read_eep(), write_ram(), write_eep(), reset(), rollback()
          * Control commands: rt_write(), rt_read()
          * Extended debuging: rt_debug()
          In most cases ACK HerkulexPacket or raw data is returned.

        * Timeout handling. Command methods receive timeout as  optional argument. If nothing is provided method calculate timeout 
          based on processing_delay, servo_timings attributes and transfer media properties (baudrate and etc).
          * Normal commands: timeout = request_transfer_time(baudrate) + servo_timings.ack_timeout + processing_delay
          * rt_read commands: timeout = request_transfer_time(baudrate) + servo_timings.rt_start_delay + servo_timings.rt_delay*n_servos + processing_delay
          * rt_debug commands: timeout = request_transfer_time(baudrate) + response_transfer_time(baudrate) + processing_delay

        * Tryout handling. Normal command methods recives number of tryouts as optional parameter. If nothing is provided efault value is used.

        Usage example:

            bus = ProtocolHandle(SerialBusHandle('/dev/ttyUSB0', 115200))
            bus.tryouts = 1 # set deafault tryuouts number

            # get servo staus
            pkt = bus.status(0x01) # raises ProtocolError if failed
            print('Servo 0x01 ACK STAT packet:', pkt)
   
            # write 4 bytes to address 60
            bus.write_ram(0x02, 60, struct.pack('hh', 1000, 2000)) # raises ProtocolError if failed (no ACK or error on servo side)
  
            # RT exchange
            data = bytearry()
            data.extend( bus.pack_rt_write_subpacket(0x1, 1000, 0 , 0) ) # servo_id, postion_ref, speed_ref, current_ref
            data.extend( bus.pack_rt_write_subpacket(0x2, 2000, 0 , 0) ) 
            bus.rt_write([0x01 0x2], data)
            pkts = bus.rt_read([0x01 0x2], prepend_rt_write = True) # no exception, consider previous rt_write command when timeout is calculated
            for pkt in pkts:
                print(f"Servo bus {pkt.servo_id} state:', *bus.unpack_rt_read_data(pkt.data))


    '''

    def __init__(self, bus):
        ''' Create ProtocolHandle instance accotiated with given transfer media. 

        Parameters:
        -----------
        bus: BusHandleInterface
            Transfer media.
        '''
        # packet transfer media
        self._bus = bus
        # timeout calculation and tryouts
        self._tryouts = 3
        self._processing_delay = 0.1
        self._servo_timings = ServoTimings(0.0002, 0.002, 0.004)

    #
    # Basic methods
    # 

    @property
    def bus(self):
        ''' Get assotiated tansfer media. '''
        return self._bus

    @property
    def tryouts(self):
        ''' Number of tryouts for normal commands (status(), read_*(), write_*(), reset(), rollback()) '''
        return self._tryouts

    @tryouts.setter
    def tryouts(self, value):
        if not isinstance(value, int) or value <= 0:
            raise ValueError('Number of tryouts must be positive integer')
        self._tryouts = value

    @property
    def processing_delay(self):
        ''' Processing delay represent time neceserry to process abd react single packet on host and servo side except for transfer time.'''
        return self._processing_delay

    @processing_delay.setter
    def processing_delay(self, value):
        if not isinstance(value, (int, float)) or value <= 0:
            raise ValueError('Processing delay must be positive number.')
        self._processing_delay = value

    @property
    def servo_timings(self):
        ''' Processing delay represent time neceserry to process abd react single packet on host and servo side except for transfer time.'''
        return self._servo_timings

    @servo_timings.setter
    def servo_timings(self, timings):
        if not isinstance(value, ServoTimings):
            raise TypeError('ServoTiming structure is expected')
        if any(v < 0 for v in timings):
            raise ValueError('ServoTimings structure is incorrect')
        self._servo_timings = timings

    def __repr__(self):
        return f'ProtocolHandle({self._bus}, tryouts={self._tryouts}, processing_delay={self._processing_delay})'

    #
    # Protocol commands
    #

    def read_ram(self, servo_id, reg_addr, reg_len, timeout = None, tryouts = None):
        ''' Read RAM register table.

        Parameters:
        ----------
        servo_id, reg_addr, reg_len: int
            Servo id, adrress and memory fragment length to read.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.
        tryouts: int or None, default None
            Number of tryouts. If None is proivided default number of tryouts is used.

        Returns:
        -------
        HerkulexPacket
            Correct ACK packet received.

        Raises:
        -------
        ProtocolError
            On timeout, error on servo side or if incorrect or unexpected packeds received.
        '''
        req_pkt = HerkulexPacket(servo_id, ReqCmd.RAM_READ, bytearray([reg_addr, reg_len]))
        ack_pkt_size = HEADER_LENGTH + 2 + reg_len
        ack_pkt = self._req_ack_exchange(req_pkt, ack_pkt_size , timeout, tryouts)
        return ack_pkt

    def write_ram(self, servo_id, reg_addr, reg_data, receive_ack = True, timeout = None, tryouts = None):
        ''' Write RAM register table.

        Parameters:
        ----------
        servo_id, reg_addr: int
            Servo id, memory adrress to write
        reg_data: bytes or bytearray
            Data to write.
        receive_ack: bool, default True
            Wait and check ACK packet.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.
        tryouts: int or None, default None
            Number of tryouts. If None is proivided default number of tryouts is used.

        Returns:
        -------
        HerkulexPacket or None
            Correct ACK packet received if receive_ack is True. None otherwise.

        Raises:
        -------
        ProtocolError
            On timeout, error on servo side or if incorrect or unexpected packeds received.
        '''
        data = bytearray([reg_addr, len(reg_data)])
        data.extend(reg_data)
        req_pkt = HerkulexPacket(servo_id, ReqCmd.RAM_WRITE, data)
        if receive_ack:
            ack_pkt_size = HEADER_LENGTH + 2
            ack_pkt = self._req_ack_exchange(req_pkt, ack_pkt_size , timeout, tryouts)
            return ack_pkt
        else:
            self._bus.send_packet(req_pkt)
            return None

    def read_eep(self, servo_id, reg_addr, reg_len, timeout = None, tryouts = None):
        ''' Read EEP register table.

        Parameters:
        ----------
        servo_id, reg_addr, reg_len: int
            Servo id, adrress and memory fragment length to read.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.
        tryouts: int or None, default None
            Number of tryouts.

        Returns:
        -------
        HerkulexPacket
            Correct packet received.

        Raises:
        -------
        ProtocolError
            On timeout, error on servo side or if incorrect or unexpected packeds received.
        '''
        req_pkt = HerkulexPacket(servo_id, ReqCmd.EEP_READ, bytearray([reg_addr, reg_len]))
        ack_pkt_size = HEADER_LENGTH + 2 + reg_len
        ack_pkt = self._req_ack_exchange(req_pkt, ack_pkt_size , timeout, tryouts)
        return ack_pkt

    def write_eep(self, servo_id, reg_addr, reg_data, receive_ack = True, timeout = None, tryouts = None):
        ''' Write EEP register table.

        Parameters:
        ----------
        servo_id, reg_addr: int
            Servo id, memory adrress to write
        reg_data: bytes or bytearray
            Data to write.
        receive_ack: bool, default True
            Wait and check ACK packet.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.
        tryouts: int or None, default None
            Number of tryouts. If None is proivided default number of tryouts is used.

        Returns:
        -------
        HerkulexPacket or None
            Correct ACK packet received if receive_ack is True. None otherwise.

        Raises:
        -------
        ProtocolError
            On timeout, error on servo side or if incorrect or unexpected packeds received.
        '''
        data = bytearray([reg_addr, len(reg_data)])
        data.extend(reg_data)
        req_pkt = HerkulexPacket(servo_id, ReqCmd.EEP_WRITE, data)
        if receive_ack:
            ack_pkt_size = HEADER_LENGTH + 2
            ack_pkt = self._req_ack_exchange(req_pkt, ack_pkt_size , timeout, tryouts)
            return ack_pkt
        else:
            self._bus.send_packet(req_pkt)
            return None

    def status(self, servo_id, timeout = None, tryouts = None):
        ''' Request servo status.

        Parameters:
        ----------
        servo_id: int
            Servo id.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.
        tryouts: int or None, default None
            Number of tryouts. If None is proivided default number of tryouts is used.

        Returns:
        -------
        HerkulexPacket
            Correct ACK packet received.

        Raises:
        -------
        ProtocolError
            On timeout, error on servo side or if incorrect or unexpected packeds received.
        '''
        req_pkt = HerkulexPacket(servo_id, ReqCmd.STAT, bytearray())
        ack_pkt = self._req_ack_exchange(req_pkt, ack_pkt_size = HEADER_LENGTH + 2, timeout = timeout, tryouts = tryouts)
        return ack_pkt

    def reset(self, servo_id, receive_ack = True, timeout = None, tryouts = None):
        ''' Reset servo.

        Parameters:
        ----------
        servo_id: int
            Servo id.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.
        receive_ack: bool, default True
            Wait and check ACK packet.
        tryouts: int or None, default None
            Number of tryouts. If None is proivided default number of tryouts is used.

        Returns:
        -------
        HerkulexPacket
            Correct ACK packet received.

        Raises:
        -------
        ProtocolError
            On timeout, error on servo side or if incorrect or unexpected packeds received.
        '''
        req_pkt = HerkulexPacket(servo_id, ReqCmd.REBOOT, bytearray())
        if receive_ack:
            ack_pkt = self._req_ack_exchange(req_pkt, ack_pkt_size = HEADER_LENGTH + 2, timeout = timeout, tryouts = tryouts)
            return ack_pkt
        else:
            self._bus.send_packet(req_pkt)

    def rollback(self, servo_id, receive_ack = True, timeout = None, tryouts = None):
        ''' Restore servo EEP registers to default values. 

        Parameters:
        ----------
        servo_id: int
            Servo id.
        receive_ack: bool, default True
            Wait and check ACK packet.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.
        tryouts: int or None, default None
            Number of tryouts. If None is proivided default number of tryouts is used.

        Returns:
        -------
        HerkulexPacket
            Correct ACK packet received.

        Raises:
        -------
        ProtocolError
            On timeout, error on servo side or if incorrect or unexpected packeds received.
        '''
        req_pkt = HerkulexPacket(servo_id, ReqCmd.ROLLBACK, bytearray())
        if receive_ack:
            ack_pkt = self._req_ack_exchange(req_pkt, ack_pkt_size = HEADER_LENGTH + 2, timeout = timeout, tryouts = tryouts)
            return ack_pkt
        else:
            self._bus.send_packet(req_pkt)
            return None

    def rt_write_one(self, servo_id, payload, receive_ack = True, timeout = None, tryouts = None):
        ''' Send RT_WRITE command to one servo. Use non-broadcast packet to communicate the servo.

        Parameters:
        ----------
        servo_id: int
            Servo id.
        payload: bytes or bytearray
            RT_WRITE subpackets.
        receive_ack: bool, default True
            Wait and check ACK packet.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.
        tryouts: int or None, default None
            Number of tryouts. If None is proivided default number of tryouts is used.

        '''
        if len(payload) != RT_WRITE_SUBPACKET_LENGTH:
            raise ValueError('RT_WRITE payload has incorrect size {len(payload)}')
        req_pkt = HerkulexPacket(servo_id, ReqCmd.RT_WRITE, payload)
        if receive_ack:
            ack_pkt = self._req_ack_exchange(req_pkt, ack_pkt_size = HEADER_LENGTH + 2, timeout = timeout, tryouts = tryouts)
            return ack_pkt
        else:
            self._bus.send_packet(req_pkt)

    def rt_write(self, payload):
        ''' Send RT_WRITE command. Use broadcast packet to communicate multipy servos.

        Parameters:
        ----------
        payload: bytes or bytearray
            Concateneated RT_WRITE subpackets.
        '''
        if len(payload) % RT_WRITE_SUBPACKET_LENGTH != 0:
            raise ValueError('RT_WRITE payload has incorrect size {len(payload)}')
        req_pkt = HerkulexPacket(BROADCAST_ID, ReqCmd.RT_WRITE, payload)
        self._bus.send_packet(req_pkt)

    def s_jog(self, playtime, payload):
        ''' Send S_JOG command.

        Parameters:
        ----------
        playtime: int
            Duration of motion in trget units.
        payload: bytes or bytearray
            Concateneated S_JOG subpackets.
        '''
        if len(payload) % S_JOG_SUBPACKET_LENGTH != 0:
            raise ValueError('S_JOG payload has incorrect size {len(payload)}')
        # prepend payload to JOG subpackets
        data = bytearray()
        data.append(playtime)
        data.extend(payload)
        # send request
        req_pkt = HerkulexPacket(BROADCAST_ID, ReqCmd.S_JOG, data)
        self._bus.send_packet(req_pkt)

    def i_jog(self, payload):
        ''' Send I_JOG command.

        Parameters:
        ----------
        payload: bytes or bytearray
            Concateneated I_JOG subpackets.
        '''
        if len(payload) % I_JOG_SUBPACKET_LENGTH != 0:
            raise ValueError('I_JOG payload has incorrect size {len(payload)}')
        req_pkt = HerkulexPacket(BROADCAST_ID, ReqCmd.I_JOG, payload)
        self._bus.send_packet(req_pkt)

    def rt_read(self, servo_ids, prepend_rt_write = False, timeout = None):
        ''' Send RT_RAED command and receivew servo responses.

        Parameters:
        -----------
        servo_ids: list ort tuple
            List of servos ids.
        prepend_rt_write: bool, default None
            Add RT_WRITE command duration to timeout.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.

        Returns:
        --------
        list of HerkulexPacket
            Received servo responses. Number of packets may be less then number of ids.
        '''
        n_servos = len(servo_ids)
        req_pkt = HerkulexPacket(BROADCAST_ID, ReqCmd.RT_READ, bytearray(servo_ids))
        # timeout clculation
        if timeout is None:
            req_bytes = req_pkt.size()
            if prepend_rt_write:
                req_bytes += HEADER_LENGTH + RT_WRITE_SUBPACKET_LENGTH * n_servos
            servo_timings = self._servo_timings
            timeout = self._bus.estimate_transfer_delay(req_bytes, 0) + servo_timings.rt_start_delay + servo_timings.rt_delay * n_servos + self._processing_delay
        deadline = time.time() + timeout
        # send request 
        self._bus.send_packet(req_pkt)
        # receive response
        packets = []
        while len(packets) < n_servos:
            # check if new packet is availble
            pkt = self._bus.receive_packet(deadline)
            # check payload
            if pkt is None:
                # timeout expired
                return packets
            # check packet content: discard incorrect packets and continue
            if pkt.cmd != AckCmd.RT_READ:
                continue
            if pkt.is_error_pkt():
                log.warning(f'ACK_RT_READ: ERROR packet: {StatusDetail(pkt.data[1])}.')
                continue
            if len(pkt.data) != RT_READ_ACK_DATA_LENGTH:
                log.warning(f'ACK_RT_READ: incorrect packet length: {len(pkt.data)}.')
                continue
            if pkt.servo_id not in servo_ids[len(packets):]:
                log.warning(f'ACK_RT_READ: unexpected servo id: {pkt.servo_id}.')
                continue
            # save packet to list
            packets.append(pkt)
        # return received packets
        return packets

    def rt_debug(self, servo_id, channel, sample_num, timeout = None):
        ''' Request real-time debug data.

        Parameters:
        -----------
        servo_id, channel, sample_num: int 
            Sero id, debug channel and number of samples to read.
        channel, sample_num: int
            Debug channel numnber and desired number of samples.
        timeout: float or None, default None
            Timeout in seconds. If None is provided default timeout is used.

        Returns:
        --------
        numpy.array or None
            Received data (note that servo an reduce number of samples) or None if not all data is received. 
        '''
        req_pkt = HerkulexPacket(servo_id, ReqCmd.RT_DEBUG, bytearray([channel, sample_num & 0xff, sample_num >> 8]))
        # timeout calculation
        if timeout is None:
            n_ack_packets = sample_num // RT_DEBUG_SAMPLES_PER_PACKET
            ack_pkt_size = HEADER_LENGTH + RT_DEBUG_ACK_HEADER_LENGTH + 2*RT_DEBUG_SAMPLES_PER_PACKET
            timeout = self._bus.estimate_transfer_delay(req_pkt.size(), n_ack_packets * ack_pkt_size) 
            timeout += self._processing_delay
        deadline = time.time() + timeout
        # send request
        self._bus.send_packet(req_pkt)
        # receive response
        debug_data = bytearray()
        while True:
            # get packet
            pkt = self._bus.receive_packet(deadline)
            # check payload
            if pkt is None:
                # timeout expired
                break
            # check packet correctness
            if self._check_ack_packet_header(pkt, req_pkt) is not None:
                continue
            # parse payload
            if pkt.data[0] != channel:
                log.warning('ACK_RT_DEBUG channel does not matches')
                continue
            sample_num = pkt.data[3] + (pkt.data[4] << 8)
            if 2*sample_num != len(debug_data):
                log.warning('ACK_RT_DEBUG sample number %d does not matches expected value %d/2' % (sample_num, len(debug_data)))
                continue
            pkt_debug_data = pkt.data[RT_DEBUG_ACK_HEADER_LENGTH:]
            if len(pkt_debug_data) % 2 != 0:
                log.warning('ACK_RT_DEBUG incorrect packet length')
                continue
            # copy data
            debug_data.extend( pkt.data[RT_DEBUG_ACK_HEADER_LENGTH:] )
            # check if all data is received
            expected_data_len = 2*(pkt.data[1] + (pkt.data[2] << 8))
            # log.debug('data_len = %d, debug_data_len = %d, expected_data_len = %d' % (len(pkt.data), len(debug_data), 2*(pkt.data[1] + (pkt.data[2] << 8))))
            if len(debug_data) >= expected_data_len:
                # convert to sequence of uint
                return np.frombuffer(debug_data, dtype=np.int16)
        # not all data is received
        return None

    #
    # Helper functions
    #

    @staticmethod
    def pack_s_jog_subpacket(servo_id, jog_mode, jog_cmd):
        ''' Return S_JOG supacket as bytes

        Parameters:
        -----------
        servo_id: int
            Servo id.
        jog_mode: ServoSubclass.JogMode or int
            Control mode.
        jog_cmd: int
            Target position, speed, current, voltage, depending on jog_mode in target units.

        Return:
        -------
        bytes
            Subpacket bytes representation.
        '''
        return struct.pack('hBB', jog_cmd, jog_mode, servo_id)

    @staticmethod
    def pack_i_jog_subpacket(servo_id, jog_mode, jog_cmd, playtime):
        ''' Return I_JOG supacket as bytes

        Parameters:
        -----------
        servo_id: int
            Servo id.
        jog_mode: ServoSubclass.JogMode or int
            Control mode.
        jog_cmd: int
            Target position, speed, current, voltage, depending on jog_mode in traget units.
        playtime: int
            Duration of motion in trget units.

        Return:
        -------
        bytes
            Subpacket bytes representation.
        '''
        return struct.pack('hBBB', jog_cmd, jog_mode, servo_id, playtime)

    @staticmethod
    def pack_rt_write_subpacket(servo_id, mode, arg1, arg2, arg3):
        ''' Return RT_WRITE supacket as bytes.

        Parameters:
        -----------
        servo_id, mode: int
           Target servo and control mode.
        arg1, arg2, arg3: int
            Interpretation of parameters depends on mode setting.
            In most cases arg1 is position, arg2 is speed and arg3 is current.

        Return:
        -------
        bytes
            Subpacket bytes representation.
        '''
        return struct.pack('BBhhh', servo_id, mode, position_ref, speed_ref, current_ff)

    @staticmethod
    def unpack_rt_read_data(data):
        ''' Decode RT_READ ACK packet data in raw units.
        Parameters:
        -----------
        data: bytes
            ACK RT_READ packet payload.

        Return:
        -------
        postion, speed, current, temperatue, status_error: int
            Servo state in raw units.
        '''
        return struct.unpack('hhhBB')


    def _req_ack_exchange(self, req_pkt, ack_pkt_size = HEADER_LENGTH+2, timeout = None, tryouts = None):
        ''' Wait until correct packet is received or timeout is expired. '''
        if tryouts is None:
            tryouts = self._tryouts
        if timeout is None:
            timeout = self._bus.estimate_transfer_delay(req_pkt.size(), 0) + self._servo_timings.ack_timeout + self._processing_delay
        # attempt excahnge
        for tryout in range(tryouts):
            deadline = time.time() + timeout
            # send request
            self._bus.send_packet(req_pkt)
            # wait for response
            error = None
            while True:
                ack_pkt = self._bus.receive_packet(deadline)
                # if no packet is receved, deadline is reached, next tryout
                if ack_pkt is None:
                    break
                # if header is incorrect, wait until deadline
                error = self._check_ack_packet_header(ack_pkt, req_pkt)
                if error is not None:
                    log.warning(str(error))
                    continue
                # if we have error packet retry if error is not fatal
                if ack_pkt.is_error_pkt():
                    error = OpFaliedError(ack_pkt)
                    log.warning(str(error))
                    # check if error indicate that request is invalid 
                    if ack_pkt.data[1] & (StatusDetail.INVALID_REQ | StatusDetail.BAD_REG_RANGE):
                        raise error
                    break
                # if payload is incorrect, wait until deadline
                error = self._check_ack_packet_data(ack_pkt, req_pkt)
                if error is not None:
                    log.warning(str(error))
                    continue
                # packet is correct: excahnde succeed
                return ack_pkt
        # exchange failed
        if error is not None:
            raise error
        else:
            error = NoACKError(req_pkt)
            log.warning(str(error))
            raise error
            
    @staticmethod
    def _check_ack_packet_header(ack_pkt, req_pkt):
        ''' Check if packet is correct ACK packet with specified properties. Return Exception if check failed. ''' 
        # check servo ID and cmd
        if ack_pkt.servo_id != req_pkt.servo_id:
            return UnexpectedPacketError('Wrong servo_id %d, expected %d.' % (ack_pkt.servo_id, req_pkt.servo_id))
        # check if cmd expected
        if ack_pkt.cmd != (req_pkt.cmd | AckCmd.MASK):
            return UnexpectedPacketError('Wrong ACK packet cmd: expected 0x%02x, received 0x%02x' % (req_pkt.cmd | AckCmd.MASK, ack_pkt.cmd))
        return None

    @staticmethod
    def _check_ack_packet_data(ack_pkt, req_pkt):
        ''' Check if packet payload is correct for ACK packet with specified properties. Return Exception if check failed. ''' 
        # parse data
        if ack_pkt.cmd in (AckCmd.RAM_WRITE, AckCmd.EEP_WRITE, AckCmd.STAT, AckCmd.ROLLBACK, AckCmd.REBOOT, AckCmd.RT_WRITE):
            if len(ack_pkt.data) != 2:
                return InvalidPacketError('Invalid ACK packet length, 2 expected.')
        elif ack_pkt.cmd in (AckCmd.RAM_READ, AckCmd.EEP_READ): 
            # check ACK READ packet payload
            if ack_pkt.data[0] != req_pkt.data[0]:
                return UnexpectedPacketError('Wrong ACK READ packet data addr %d expected %d' % (ack_pkt.data[0], req_pkt.data[0]))
            if ack_pkt.data[1] != req_pkt.data[1]:
                return UnexpectedPacketError('Wrong ACK READ packet data len %d expected %d' % (ack_pkt.data[1], req_pkt.data[1]))
            if len(ack_pkt.data) != ack_pkt.data[1] + 4:
                return InvalidPacketError('Invalid ACK READ packet: data len mismatch with packet len.')
        elif ack_pkt.cmd == AckCmd.RT_READ: 
            if len(ack_pkt.data) != RT_READ_ACK_DATA_LENGTH:
                return InvalidPacketError('Invalid ACK RT_READ packet length.')
        elif ack_pkt.cmd == AckCmd.RT_DEBUG:
            data_len = len(ack_pkt.data) 
            if data_len < RT_DEBUG_ACK_HEADER_LENGTH or (data_len - RT_DEBUG_ACK_HEADER_LENGTH % 2) != 0:
                return InvalidPacketError('Invalid ACK_RT_DEBUG payload length')
        else:
            return InvalidPacketError('Invalid ACK packet cmd %x' % cmd)
        return None

