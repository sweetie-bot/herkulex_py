# -*- coding: utf-8 -*-

import logging
import time
from tabulate import tabulate
from queue import SimpleQueue, Empty as QueueEmpty

import rospy
from sweetie_bot_herkulex_msgs.msg import HerkulexPacket as HerkulexPacketMsg
from sweetie_bot_herkulex_msgs.srv import GetCMServerInfo, GetCMServerInfoRequest, GetCMServerInfoResponse

from . import protocol

#
# Logging facilities
#

log = logging.getLogger('protocol.bus')

#
# Bus Server
#

class ROSBusServer:
    ''' ROS-based HerkulexPacket transport: server side.

    Usage example:
        
        period = 0.1 # control period, seconds

        bus = SerialBusHandle('/dev/ttyUSB0', 115200)
        protocol = ProtocolHandle(bus)
        server = ROSBusServer(bus = bus, poll_period = period)

        rate = rospy.Rate(1/period)
        while not rospy.is_shutdown():
            # Standart exchange
            # ...
            protocol.rt_write(bus, cmd)
            protocol.rt_read(servo_ids, state)
            # ...
            # Additional exchange via ROS transport
            server.spin()
    '''
    def __init__(self, bus, spin_period, spin_duration, description = None):
        ''' Create and start ROSBusServer. 

        Create server-related topics and info service.

        Parameters:
        -----------
        bus: BusHandleInterface
            Underlying bus handle object. It will be used in spin() call.
        spin_period: float
            Period of spin() call in seconds. This value is used by client to estimate timeout.
            It is expected that spin() is called at least once during period.
        spin_duration: float
            For long time spin() call will block underlying bus to forward requests to it.
        description: str or None
            Server description string. If None is passed it is deduced from bus __repr__.
        '''    
        # server parameters
        self._period = spin_period
        self._spin_duration = spin_duration
        self._bus = bus
        if description is None:
            self._description = f'ROSBusServer(bus={bus})'
        else:
            self._description = description
        # pacakage transport
        self._send_sub = rospy.Subscriber('send', HerkulexPacketMsg, self._send_callback)
        self._recv_pub = rospy.Publisher('recv', HerkulexPacketMsg, queue_size=10)
        # service info
        self._info_serv = rospy.Service('info', GetCMServerInfo, self._info_callback)
        # message buffers
        self._send_buffer = SimpleQueue()

    def spin(self):
        ''' Perform messgae forwarding. 

        This call blocks for 'spin_duration'. During this time period it forwards messages 
        from underlying 'bus' to ROSBusHandle using ROS transport.

        In carrent implementation it process only one request received on 'send' topic.
        '''

        # calculate deadline
        deadline = time.time() + self._spin_duration
        # get packet to send
        try:
            pkt = self._send_buffer.get_nowait()
        except QueueEmpty:
            return 
        # perform request
        self._bus.send_packet(pkt)
        # wait and forward servo response
        while True:
            pkt = self._bus.receive_packet(deadline)
            if pkt is not None:
                self._recv_pub.publish(HerkulexPacketMsg(servo_id=pkt.servo_id, command=pkt.cmd, data=pkt.data))
            else:
                break

    def _info_callback(self, req):
        # return service info
        return GetCMServerInfoResponse(description = self._description, max_delay = self._period, max_timeout = self._spin_duration)

    def _send_callback(self, msg):
        # enquue received packet
        self._send_buffer.put(protocol.HerkulexPacket(servo_id=msg.servo_id, cmd=protocol.ReqCmd(msg.command), data = msg.data))
        

#
# ROS Bus handle
#

class ROSBusHandle(protocol.BusHandleInterface):
    ''' ROS-based HerkulexPacket transport: client side.

        Provide abstraction to interract with BusHandle via ROSBusServer.

        Usage example:

            bus = SerialBusHandle('/herkulex_driver')

            req = HerkulexPacket(0xfd, ReqCmd.RAM_READ, [ 60, 2 ])
            deadline = time.time() + timeout
            bus.send_packet(req)
            ack = bus.receive_packet(deadline)
    '''

    def __init__(self, namespace, processing_delay = 0.005):
        ''' Construct ROSBusHandle.

        Parameters
        ----------
        namespace: str
            Namespace where Server is located.
        processing_delay: float, 0.005
            Estimated delay caused by ROS transport.
        '''
        # package transport
        self._send_pub = rospy.Publisher(namespace + '/send', HerkulexPacketMsg, queue_size=5)
        self._recv_sub = rospy.Subscriber(namespace + '/recv', HerkulexPacketMsg, self._recv_callback)
        # connect to info service to get info
        info_serv = rospy.ServiceProxy(namespace + '/info', GetCMServerInfo)
        info_serv.wait_for_service(3.0)
        info = info_serv()
        self._description = info.description
        self._max_delay = info.max_delay + processing_delay
        self._max_timeout = info.max_timeout
        # log
        log.info(f"Connected to ROSBusServer at '{namespace}': {self._description}")
        # receive buffer and lock
        self._recv_buffer = SimpleQueue()
        # self-description
        self._namespace = namespace

    @property
    def max_timeout(self):
        return self._max_timeout

    def __repr__(self):
        return f'ROSBus("{self._namespace}": {self._description}, delay = {self._max_delay}, timeout = {self._max_timeout})'

    def send_packet(self, pkt):
        ''' Send HerkulexPacket over ROS link.

        Parameters
        ----------
        pkt: HerkulexPacket
            Packet to send.
        '''
        # convert to ROS message and publish it
        msg = HerkulexPacketMsg(servo_id = pkt.servo_id, command = pkt.cmd, data = pkt.data)
        self._send_pub.publish(msg)
        log.debug(f' >> ROSBusServer({self._namespace}): {pkt}')

    def receive_packet(self, deadline = None):
        ''' Receive packet over ROS link.

        Parameters
        ----------
        deadline: float or None
            Time of receive deadline: deadline = time.time() + timeout. 

        Return
        ------
        HerkulexPacket or None
            Received packet or None if deadline is reached,
        '''
        try:
            return self._recv_buffer.get(deadline - time.time())
        except QueueEmpty:
            return None

    def _recv_callback(self, msg):
        # convert to HerkulexPacket
        pkt = protocol.HerkulexPacket(servo_id=msg.servo_id, cmd=protocol.AckCmd(msg.command), data=bytearray(msg.data))
        # add received packe to buffer and notify waiting threads
        self._recv_buffer.put(pkt)
        log.debug(f' << ROSBusServer({self._namespace}): {pkt}')

    def estimate_transfer_delay(self, recv_bytes, send_bytes):
        ''' Estimate exchange duration based on transport properties. 

            Parameters
            ----------
            recv_bytes, send_bytes: int
                Number of bytes in packet include header.

            Return
            ------
            float
                Transfer dealy in seconds.

        '''
        return self._max_delay
