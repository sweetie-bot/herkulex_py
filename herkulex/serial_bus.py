#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import serial, io
from functools import reduce
from operator import xor
from dataclasses import dataclass
from select import select

import time
import numpy as np
from tabulate import tabulate

from . import protocol
from .protocol import HEADER_LENGTH, HEADER_START, HEADER_ID_FIELD, HEADER_CMD_FIELD, HEADER_LEN_FIELD, HEADER_CHECKSUM1_FIELD, HEADER_CHECKSUM2_FIELD
from .protocol import HerkulexPacket, AckCmd

#
# Logging facilities
#

log = logging.getLogger('protocol.bus')


#
# Sataistics
#

@dataclass
class SerialBusStatistics:
    ''' Serial bus send/receive statistics.'''
    send_pkts : int = 0
    send_bytes : int  = 0
    recv_pkts : int = 0
    recv_pkt_checksum_errors : int  = 0
    recv_pkt_bytes : int = 0
    recv_bytes : int = 0

    def __str__(self):
        table = [
            [ 'Packets sent', self.send_pkts ],
            [ 'Bytes sent', self.send_bytes ],
            [ 'Packets receive', self.recv_pkts ],
            [ 'Packets checksum errors', self.recv_pkt_checksum_errors ],
            [ 'Packet bytes received', self.recv_pkt_bytes ],
            [ 'Total bytes received', self.recv_bytes ]
        ]
        output = 'SerialBus exchange statistics\n'
        output += tabulate(table)
        return output

    def reset(self):
        ''' Reset statistics. '''
        self.send_pkts = 0
        self.send_bytes = 0
        self.recv_pkts = 0
        self.recv_pkt_checksum_errors = 0
        self.recv_pkt_bytes = 0
        self.recv_bytes = 0




#
# Bus hanle
#

class SerialBusHandle(protocol.BusHandleInterface):
    ''' UART serial bus with ability send and receive HerkulexPacket. 

        Usage example:

            bus = SerialBusHandle('/dev/ttyUSB0', 115200)

            req = HerkulexPacket(0xfd, ReqCmd.RAM_READ, [ 60, 2 ])
            deadline = time.time() + timeout
            bus.send_packet(req)
            ack = bus.receive_packet(deadline)
    '''

    def __init__(self, port, baudrate):
        ''' Construct SerialBusHandle

        Parameters
        ----------
        port: str
            Serial port device.
        baudrate: int
            Baudrate. 
        '''
        # serial device
        self._dev = serial.Serial(port=port, baudrate=baudrate, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS)
        # receive buffer
        self._buf = bytearray()
        self._buf_length_expected = HEADER_LENGTH
        # transfer delay estimate
        self._byte_delay = (1 + 8 + 2)/baudrate
        # statistics
        self._stat = SerialBusStatistics()

    def __repr__(self):
        return f'SerialBus("{self._dev.port}", B{self._dev.baudrate})'

    def __srt_(self):
        return f'SerialBus("{self._dev.port}", B{self._dev.baudrate})'

    @property
    def statistics(self):
        ''' Get SerialBusStatistics object. '''
        return self._stat

    def reset_statistics(self):
        ''' Set all statistic counters to zero. '''
        self._stat.reset()

    def send_packet(self, pkt):
        ''' Send HerkulexPacket over serial link.

        Parameters
        ----------
        pkt: HerkulexPacket
            Packet to send.
        '''
        # Number of bytes following standard header: 0xff, 0xff, len, id, cmd, checksum1, checksum2
        length = HEADER_LENGTH + len(pkt.data)  # 7 + data_len
        # directly from Herkulex manual:
        # Check Sum1 = (PacketSize ^ pID ^ CMD ^ Data[0] ^ Data[1] ^ ... ^ Data[n]) & 0xFE
        checksum1 = ((length ^ pkt.servo_id ^ pkt.cmd ^ reduce(xor, pkt.data, 0)) & 0xfe)
        # Check Sum2 = ( ~CheckSum1) & 0xFE
        checksum2 = (( ~checksum1 ) & 0xFE)
        # packet: FF FF LENGTH ID CMD CHECKSUM1 CHECKSUM2 DATA1 DATA2 ... DATAn
        packet = bytearray(HEADER_START)
        packet.extend([length, pkt.servo_id, pkt.cmd, checksum1, checksum2])
        packet.extend(pkt.data)
        # write data to port
        self._dev.write(packet)
        # dubug log
        log.debug(f' >> {self._dev.port}: {pkt}')
        # sataistics collection
        self._stat.send_pkts += 1
        self._stat.send_bytes += len(packet)



    def receive_packet(self, deadline):
        ''' Wait until correct packet is received or deadline is reached.

        Parameters
        ----------
        deadline: float
            Time of receive deadline: deadline = time.time() + timeout. 

        Return
        ------
        HerkulexPacket or None
            Received packet or None if deadline is reached,
        '''
        while True:
            # parse availble data
            pkt = self._parse_available_data()
            if pkt is not None:
                log.debug(f'<< {self._dev.port}: {pkt}')
                return pkt 
            # check timeout
            timeout = deadline - time.time()
            if timeout < 0:
                return None
            # wait until data is avalab;e
            try:
                # wait until new daya is arrived
                rfds, _, _ = select([self._dev.fileno()], [], [], timeout)
                # check timeout is reached
                if (len(rfds) == 0):
                    return None
            except io.UnsupportedOperation:
                # fileno() is not supported: check data again
                time.sleep(0.005)

    def estimate_transfer_delay(self, recv_bytes, send_bytes):
        ''' Estimate exchange duration based on transport properties. 

            Parameters
            ----------
            recv_bytes, send_bytes: int
                Number of bytes in packet include header.

            Return
            ------
            float
                Transfer dealy in seconds.

        '''
        return self._byte_delay * (recv_bytes + send_bytes)


    def _parse_available_data(self):
        ''' Check input stream and return received HernulexPacket or None. Do not block.
        '''
        buf = self._buf
        # check if new data is availble
        n_bytes = self._dev.inWaiting()
        if n_bytes > 0:
            # read bytes
            buf.extend(self._dev.read(n_bytes))
            # print(f'add {n_bytes} to buffer: {buf}')
            # satatistics collection
            self._stat.recv_bytes += n_bytes 
        # check if enought data is received
        while True:
            # find header
            index = buf.find(HEADER_START)
            if index < 0:
                # no header, no packet
                del buf[:-len(HEADER_START)]
                return None
            # remove bytes before header
            if index > 0:
                del buf[:index]
            # check length 
            if len(buf) < self._buf_length_expected:
                return None
            # checksum2
            if buf[HEADER_CHECKSUM2_FIELD] != (( ~buf[HEADER_CHECKSUM1_FIELD] ) & 0xFE):
                # debug log
                # log.debug('Wrong packet checksum2.')
                # sataistics collection
                self._stat.recv_pkt_checksum_errors += 1
                # discard packet
                del buf[0]
                self._buf_length_expected = HEADER_LENGTH
                continue
            # length
            length = buf[HEADER_LEN_FIELD]
            if length > len(buf): 
                self._buf_length_expected = length
                return None
            # checksum1
            if buf[HEADER_CHECKSUM1_FIELD] != ((length ^ buf[HEADER_ID_FIELD] ^ buf[HEADER_CMD_FIELD] ^ reduce(xor, buf[HEADER_LENGTH:length])) & 0xFE): # checksum1
                # debug log
                # log.debug('Wrong packet checksum2.')
                # sataistics collection
                self._stat.recv_pkt_checksum_errors += 1
                # discard packet
                buf.pop(0)
                self._buf_length_expected = HEADER_LENGTH
                continue

            # now we have correct packet
            pkt = HerkulexPacket(servo_id = buf[HEADER_ID_FIELD], cmd = AckCmd(buf[HEADER_CMD_FIELD]), data = buf[HEADER_LENGTH:length])
            # remove packet from buffer
            del buf[:length]
            self._buf_length_expected = HEADER_LENGTH
            # sataistics collection
            self._stat.recv_pkts += 1
            self._stat.recv_pkt_bytes += length
            # return packet
            return pkt

