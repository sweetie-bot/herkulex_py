import logging
import numpy as np
from dataclasses import dataclass
from tabulate import tabulate

from .protocol import NoACKError, ProtocolError, ServoTimings, HEADER_LENGTH, RT_READ_ACK_DATA_LENGTH, RT_WRITE_SUBPACKET_LENGTH
from .servo import Memory, Servo

log = logging.getLogger('protocol')

def discover(protocol_handle, id_range = (0x00, 0xfe)):
    """ Discover servos on given bus by ID scanning. 

    Parameters:
    -----------
    protocol_handle: ProtocolHandle
        Protocol handle assotiated with given bus.
    id_range: tuple(start_id, end_id), default (0x00, 0xfe)
        ID range to scan. end_id is excluded'

    Returns:
    --------
    dict
        Dictionary {servo_id: ServoObject} with discovered servos. ServoObject is Servo subcalss or ServoDummy.

    """
    servos = {}
    for servo_id in range(*id_range):
        # request servo status
        try:
            # ping servo
            pkt = protocol_handle.status(servo_id, tryouts = 1)
        except NoACKError:
            continue
        # if answer is received add servo to list
        try:
            servos[servo_id] = Servo(protocol_handle, servo_id)
        except ProtocolError as e:
            log.error(f'Servo {servo_id} model and version query failed: {e}')
    return servos

def estimate_servo_timings(servo, baudrate = None, safety_margin = 0.1, printout = False):
    """ Calculate 'rt_start_delay', 'rt_delay' and 'ack_timeout' based on baudrate ans servo properties.

    Parameters:
    -----------
    servo: Servo
        Servo to be configured
    baudrate: int
        Use this baudrate instead of current bus baudrate.
    safety_margin: float
        All timings are multiplied in (1+safety_margin) after calculation.

    Returns:
    -------
    ServoTimings
        Timings configuration.
    """
    # constants
    max_ack_packet_length = servo.MAX_ACK_PACKET_LENGTH
    T1 = servo.T1
    multipler = safety_margin + 1.0
    # transfer dealy estimation method
    if baudrate is not None:
        estimate_transfer_delay = lambda nbytes: nbytes*10/baudrate
    else:
        estimate_transfer_delay = lambda nbytes: servo.bus.bus.estimate_transfer_delay(0, nbytes)
        baudrate = servo.bus.bus._dev.baudrate
    # get minimal rt_start_delay
    resp = servo.read_ram('min_rt_start_delay')
    min_rt_start_delay = resp.register_values[0].value
    # calculate timings
    timings = ServoTimings(0, 0, 0)
    timings.rt_start_delay = max(3*T1, min_rt_start_delay + T1)
    transfer_time = estimate_transfer_delay(HEADER_LENGTH + RT_READ_ACK_DATA_LENGTH)
    timings.rt_delay = (transfer_time + T1) * multipler
    # calculate normal function timings
    timings.ack_timeout = (estimate_transfer_delay(50) + 3*T1) * multipler
    # print explanation
    if printout:
        # print configuration results
        print('Servo timing estimation parameters')
        print(tabulate([
            ('T1', T1, 'Servo fast control cycle duration. It is maximal possible jitter value for protocol code execution.'),
            ('baudrate', float(baudrate), 'SerialBus baoudrate'),
            ('transfer_time', transfer_time, 'RT_READ ACK packet transfer time'),
            ('min_rt_start_delay', min_rt_start_delay, 'Maximal RT_READ processing time registered on servo. (Get from servo)'),
            ('safety_margin', safety_margin, 'Addtional time ratio.'),
        ]))
        print('Proposed servo timings configuration\n', timings)
    # return results
    return timings

def write_servo_timings(servos, timings, target_mem = Memory.RAM, **kwargs):
    """ Write timings configuration to servos.

    Parameters:
    -----------
    servos: list(Servo)
        Servos to be configured.
    timings: ServoTiming
        Configuration to be written.
    target_mem: herkulex.servo.Memory
        Where configuration should be written: RAM, EEP, BOTH
    receive_ack, tryouts, timeout:
        Additional args for write_ram() and write_eep methods.
    """
    if target_mem & Memory.RAM:
        for s in servos:
            s.write_ram(['rt_start_delay', 'rt_delay', 'ack_timeout'], [timings.rt_start_delay, timings.rt_delay, timings.ack_timeout], **kwargs) 
    if target_mem & Memory.EEP:
        for s in servos:
            s.write_eep(['rt_start_delay', 'rt_delay', 'ack_timeout'], [timings.rt_start_delay, timings.rt_delay, timings.ack_timeout], **kwargs) 

def read_servo_timings(servo, target_mem = Memory.RAM, **kwargs):
    """ Read timings configuration to servo register table.

    Parameters:
    -----------
    servo: Servo
        Servos to be configured.
    target_mem: herkulex.servo.Memory
        Where configuration should be written: RAM, EEP, BOTH
    receive_ack, tryouts, timeout:
        Additional args for write_ram() and write_eep methods.

    Returns:
    --------
    ServoTiming
        Servo timings structure.
    """
    if target_mem == Memory.RAM:
        resp  = servo.read_ram(['rt_start_delay', 'rt_delay', 'ack_timeout'], **kwargs)
    elif target_mem == Memory.EEP:
        resp  = servo.read_eep(['rt_start_delay', 'rt_delay', 'ack_timeout'], **kwargs)
    reg_values = [ reg_value.value for reg_value in resp.register_values ]
    return ServoTimings(*reg_values)

def set_servo_baudrate(servo, baudrate, safety_margin = 0.1):
    """ Set servo baudrate value and configure timings accordingly in EPP.

    Parameters:
    -----------
    servo: Servo
        Servo to configure.
    baudrate: Baudrate
        Desired baudrate as Enum type assotiated with corresponding servo register.
    safety_margin: float
        safety_margin parameter of estimate_servo_timings() call.

    Returns:
    --------
    ServoTimings
        New timings configuration.

    Raises:
    ------
    ProtocolError
        Error assotiated with servo communication.
    """
    servo.write_eep('baudrate', baudrate)
    baudrate = baudrate.numeric_value
    timings = estimate_servo_timings(servo, baudrate, safety_margin = safety_margin)
    write_servo_timings([servo], timings, target_mem=Memory.EEP)
    return timings

def estimate_protocol_timeouts(prot, timings, n_servos, safety_margin = 0.1, printout = False):
    """ Estimate RT exchange duration and typical protcol command timeot.

    It is assumed that one communication round includes RT exchange slot and configuration/monitoring slot.
    RT exchange time slot consists of RT_WRITE command and RT_READ command for selected group servos (servos controlled in real time).
    Config/monitoring slot includes one typical READ/WRITE/STATUS command for one servo.

    During calculations only transfer time add servos timeout are taken in account. Processing delay on
    host side may be added latter.

    Parameters:
    -----------
    prot: ProtocolHandle
        It provides access to bus with estimate_transfer_delay() method. Note that underlying BusHandle must
        be SerialBusHandle object or same to caclulations be meaningfull.
    timings: ServoTimings
        Servo timing configuration.
    n_servos: int
        Number of servos to control.
    safety_margin: float
        All timings are multiplied in (1+safety_margin) after calculation.
    printout: bool
        Print configuration summary.

    Return:
    ------
    rt_timeout: float
        Proposed duaration of RT exchange slot.
    normal_timeout: float
        Proposed duration of typical protocol command (WRITE_*, READ_*, STATUS).

    """
    # request transfer duration
    rt_write_bytes = HEADER_LENGTH + RT_WRITE_SUBPACKET_LENGTH * n_servos
    rt_read_bytes = HEADER_LENGTH + n_servos
    rt_req_transfer_time = prot.bus.estimate_transfer_delay(rt_write_bytes + rt_read_bytes, 0) 
    # rt_read ack duration
    rt_ack_duration = timings.rt_start_delay + n_servos * timings.rt_delay
    # debug/config exchange slot
    max_normal_packet_length = 20
    normal_req_transfer_time = prot.bus.estimate_transfer_delay(max_normal_packet_length, 0)
    # result
    rt_timeout = (rt_req_transfer_time + rt_ack_duration) * (1.0 + safety_margin)
    normal_timeout = normal_req_transfer_time + timings.ack_timeout * (1.0 + safety_margin)
    # print 
    if printout:
        print('Servo array timing estimation parameters')
        print(tabulate([
            ('n_servos', n_servos, 'Number of servos.'),
            ('rt_req_transfer_time', rt_req_transfer_time, 'Transfer duration of RT_WRITE and RT_READ REQ packets.'),
            ('rt_ack_duration', rt_ack_duration, 'Duration of RT_READ ACK round according to servo timings.'),
            ('normal_req_transfer_time', normal_req_transfer_time, f'Transfer duration of REQ packet ({max_normal_packet_length} bytes)'),
            ('normal_ack_timeout', timings.ack_timeout, 'ACK timeout according to servo timings.'),
            ('safety_margin', safety_margin, 'Addtional time ratio.'),
        ]))
        print('Proposed servo array timings configuration\n', timings)
        print(tabulate([
            ('rt_timeout', rt_timeout, 'Proposed RT slot duration'),
            ('normal_timeout', normal_timeout, 'Proposed config/debug slot duration'),
        ]))
    # return result
    return rt_timeout, normal_timeout


