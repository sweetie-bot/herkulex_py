import enum
from functools import reduce
import tabulate

class classproperty(property):
    def __get__(self, obj, objtype=None):
        return super(classproperty, self).__get__(objtype)
    def __set__(self, obj, value):
        super(classproperty, self).__set__(type(obj), value)
    def __delete__(self, obj):
        super(classproperty, self).__delete__(type(obj))

class IntEnumWithDescription(enum.IntEnum):
    ''' IntEnum agumented with description for each value. 

        Enum decalaration example^

            class Baudrate(IntEnumWithDescription):
                B9600 = 0, '9.6 kbit/s'
                B76000 = 1, '57.6 kbit/s'

    '''

    def __new__(cls, value, description):
        obj = int.__new__(cls, value)
        obj._value_ = value
        obj._description_ = description
        return obj

    @property
    def description(self):
        ''' Get enum item description.'''
        return self._description_

    @classmethod
    def has_value(cls, item):
        ''' Check if int value is in enum. '''
        return item in cls._value2member_map_
   
    @classproperty
    def members(cls):
        ''' Return dict which maps enum value names to enum item objects.'''
        return cls.__members__

    def to_string(self, verbose = False):
        ''' Short and detailed enum item representation. '''
        return self.description if verbose else self.name

    def __str__(self):
        return self.name

class IntFlagWithDescription(enum.IntFlag):
    ''' IntFlag agumented with descriptions. 

        Enum decalaration example:

            class (IntFlagWithDescription):
                FLAG1 = 0, 'Flag1 description'
                FLAG1 = 1, 'Flag2 description'

    '''
    def __new__(cls, value, description):
        obj = int.__new__(cls, value)
        obj._value_ = value
        obj._description_ = description
        return obj

    @property
    def description(self):
        ''' Get flag description.'''
        if hasattr(self, '_description_'):
            return self._description_
        else:
            return None

    @classmethod
    def has_value(cls, item):
        ''' Check if int value is correct set of flags (bitwise or of some set of flags). '''
        mask = reduce(lambda a, b: a | b.value, cls.__members__.values(), 0)
        return bool((item | mask) == mask)

    @classproperty
    def members(cls):
        ''' Return dict which maps flags names to flag objects. '''
        return cls.__members__
    
    def to_string(self, verbose = False):
        ''' Human-redable flag set representation. '''
        if verbose:
            table = []
            for v in list(type(self)):
                table.append([ 1 if v.value & self.value else 0, v.name, v.description ])
            return tabulate(table, headers = [ '0x%02x' % self.value, 'Name', 'Description' ])
        else:
            return str.join(', ', [ v.name for v in list(type(self)) if v.value & self.value ])

    def __str__(self):
        return str.join(', ', [ v.name for v in list(type(self)) if v.value & self.value ])


