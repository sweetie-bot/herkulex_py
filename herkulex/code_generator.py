import numpy as np
from enum import IntFlag, IntEnum
from types import NoneType

from .servo import Memory, Access, ConvScale, ConvEnum
from .conv_temperature import ConvTable, ConvTermistor

#
# Converters code generation.
#

class CodeGenerator:
    ''' Code generator for arbitrary objects.

    Reister object type and provide factory for producing 
    generators for given object. '''

    _code_generators = {}

    def __init_subclass__(cls, object_type, **kwargs):
        super(CodeGenerator, cls).__init_subclass__(**kwargs)
        cls._code_generators[object_type] = cls

    def __new__(cls, instance, *args, **kwargs):
        # get generator type: first registered type in mro
        code_generator_type = None
        for object_type in type(instance).mro():
            # check if type is known
            code_generator_type = cls._code_generators.get(object_type)
            if code_generator_type is not None:
                break
        if code_generator_type is None:
            raise NotImplementedError(f'Code generator for {type(instance)} is not implemeted')
        # construct code generator
        return super(CodeGenerator, cls).__new__(code_generator_type)

    def __init__(self, instance):
        self._instance = instance

class CxxConvNone(CodeGenerator, object_type = NoneType):
    def __repr__(self):
        return 'NoneConverter()'

class CxxConvScale(CodeGenerator, object_type = ConvScale):
    def __repr__(self):
        return f'ScaleConverter("{self._instance.unit}", {self._instance.scale})'

class CxxConvEnum(CodeGenerator, object_type = ConvEnum):
    def __repr__(self):
        decl = ""
        # get type: flag or enum
        enum_type = self._instance.enum_type
        if issubclass(enum_type, IntEnum):
            decl += 'EnumConverter'
        elif issubclass(enum_type, IntFlag):
            decl += 'FlagConverter'
        else:
            raise TypeError('ConvEnum unit type must be IntEnum or IntFlag')
        # type name
        decl += f'("{enum_type.__name__}", '
        # enum declaration
        decl += '{ '
        for flag in list(enum_type):
            decl += f'{{ "{flag.name}", {flag.value} }}, '
        decl += '})'
        # return result
        return decl

class CxxConvTermistor(CodeGenerator, object_type = ConvTermistor):
    def __repr__(self):
        return 'NoneConverter()'

class CxxConvTermistor(CodeGenerator, object_type = ConvTable):
    def __repr__(self):
        return 'NoneConverter()'

#
# Register description for C++ servo access library
#


def _find_index_if(lst, pred):
    ''' Linear searc in list for first element where pred is True. Return index or None. '''
    for idx, elem in enumerate(lst):
        if pred(elem):
            return idx
    return None

def generate_register_mapper_desc(servo_cls, with_converters = False):
    """ Generate C++ code to initialize herkulex::Servo::RegisterMapper. """
    reg_num = 0
    converters = []
    conv_decl = 'const std::array<const ConverterBase *> converters = {\n'
    reg_decl = 'const array<const Register> registers = {\n'
    for reg_desc in servo_cls.registers_desc:
        # skip reserved
        if reg_desc.name.startswith('reserved'):
            continue
        # get RAM address
        if reg_desc.memory & Memory.RAM:
            ram_addr = servo_cls.registers_ram[reg_desc.name].address
        else:
            ram_addr = -1
        # get EEP address
        if reg_desc.memory & Memory.EEP:
            eep_addr = servo_cls.registers_eep[reg_desc.name].address
        else:
            eep_addr = -1
        # register properties
        reg_name_padded = f'"{reg_desc.name}",'.ljust(30, ' ')
        access = 'true' if (reg_desc.access & Access.WO) else 'false'
        # register description
        if with_converters:
            # get converter
            conv = reg_desc.converter
            # check if we can reuse existing converter
            conv_idx = _find_index_if(converters, lambda c: c == conv)
            # and add converter if necessary
            if conv_idx is None:
                conv_idx = len(converters)
                converters.append(conv)
                conv_decl += f'\tnew %s,\n' % CodeGenerator(conv)
            # register declaration with converter
            reg_decl += '\t{ %3d, %s %3d, %3d, %3d,  %5s, converters[%d], "%s" },\n' % (reg_num, reg_name_padded, eep_addr, ram_addr, reg_desc.dtype().nbytes, access, conv_idx, reg_desc.description)
        else:
            # register declaration
            reg_decl += '\t{ %3d, %s %3d, %3d, %3d,  %5s, "%s" },\n' % (reg_num, reg_name_padded, eep_addr, ram_addr, reg_desc.dtype().nbytes, access, reg_desc.description)
        # increase reg number
        reg_num += 1
    # tail 
    conv_decl += '};\n\n'
    reg_decl += '};\n'
    # return result
    return conv_decl + reg_decl


#
# Generate firmware code for registers declaration.
#

def generate_register_table(register_decoder):
    """ Generate C code to define register tables in firmware. """
    reg_table = ''
    for reg_info in register_decoder:
        # create declatation
        reg_table += f'{np.dtype(reg_info.dtype).name}_t {reg_info.name};'.ljust(40, ' ')
        reg_table += f'// {reg_info.description}\n'
        # check aligment
        if reg_info.address % reg_info.width != 0:
            raise ValueError(f'Field "{reg_info.name}" is not aligned have bad aligned properly: addr = {reg_info.address}, width = {reg_info.width}.')
    # return result
    return reg_table
