import numpy as np
import matplotlib.pyplot as plt

def plot_rt_data(data, stat = set()):
    # determine number of subplots
    names = list(data.dtype.names)
    # extract time 
    if 'time' in names:
        names.remove('time')
        time = data['time']
        time_label = 't, s'
    else:
        time = np.arange(0, len(data))
        time_label = 't, cycle'
    # create figure
    fig, axs = plt.subplots(len(names), 1, sharex = 'all')
    if len(names) == 1:
        axs = [ axs ]
    # plotting
    for ax, name in zip(axs, names):
        signal = data[name]
        # calculate statistics
        if name in stat:
            mean = np.mean(signal)
            std = np.std(signal)
            label = f'{name}, mean {mean:.3f}, std {std:.3f}'
        else:
            label = name
        # actual plot
        ax.plot(time, signal, marker='.', label = label)
        ax.legend()
        ax.set_xlabel(time_label)
        ax.set_ylabel(name)
    # show figure
    plt.show()

def plot_rt_file(filename, stat = False):
    data = np.load(filename)
    stat_names = data.dtype.names if stat else set()
    plot_rt_data(data, stat_names)

