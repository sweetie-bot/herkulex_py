import numpy as np

from ..enum_description import IntFlagWithDescription, IntEnumWithDescription
from ..servo import *
from ..conv_temperature import ConvTermistorUp

# Regster flags and enums
class Baudrate(IntEnumWithDescription):
     B9600 = 0, '9600 baud'
     B57600 = 1, '57600 baud'
     B115200 = 2, '115200 baud'
     B230400 = 3, '230400 baud'
     B250000 = 4, '250000 baud'
     B460800 = 5, '460800 baud'
     B500000 = 6, '0.5 Mbit/s'
     B921600 = 7, '921600 baud'
     B1000000 = 8, '1 Mbit/s'
     B1228800 = 9, '1.22 Mbit/s'
     B2000000 = 10, '2 Mbit/s'
     B3000000 = 11, '3 Mbit/s'
     B4000000 = 12, '4 Mbit/s'
     B6000000 = 13, '7 Mbit/s'
  
     @property
     def numeric_value(self):
         return int(self.name[1:])

     @classmethod
     def from_numeric(cls, value):
         try:
             return cls.members[f'B{int(value)}']
         except (KeyError, TypeError, ValueError):
             raise ValueError(f'Incorrect or uncompatible numeric baudrate value: {value}')

class AckPolicy(IntFlagWithDescription):
    READ = 0x01, 'Send answer to READ command.'
    WRITE = 0x02, 'Send ACK packet to confirm WRITE command.'
    ERROR = 0x04, 'Answer with ACK ERROR package on failed command.'
    RT_READ = 0x10, 'Send answer to RT_READ command'
    ALL = 0x17, 'Send all ACK packets.'

class StatusError(IntFlagWithDescription):
    VOLTAGE = 0x01, 'Over- or undervoltage'
    POT_LIMIT = 0x02, 'Postion limit is exceeded'
    TEMPERATURE = 0x04, 'Overheat'
    OVERLOAD = 0x10, 'Oerload (current avg limit).'
    DRIVER_FAULT = 0x20, 'Hardware fault'
    EEP_ERROR = 0x40, 'EEP checksum error'
    CONTROL_ON = 0x80, 'Servo control is enable'

class StatusDetail(IntFlagWithDescription):
    MOVING = 0x01, 'Servo is moving'
    INPOS = 0x02, 'Target position is reached'
    RECV_OVERFLOW = 0x04, 'Receive buffer overflow'
    INVALID_REQ = 0x08, 'Invalid protocol command'
    BAD_REG_RANGE = 0x10, 'Invalid register address'
    PROT_OP_ERROR = 0x20, 'Operation failed'
    MOTOR_ON = 0x40, 'Motor is on'
    VERSION_BIT = 0x80, 'Status version bit (1)'

class TorqueMode(IntEnumWithDescription):
    FREE = 0x00, 'Force motor off.'
    BRAKE = 0x40, 'Force brake mode.'
    TORQUE_ON = 0x60, 'Normal function: mode controlled by RT_WRITE command.'

class OperationMode(IntFlagWithDescription):
    CURRENT_CONTROLLER_ON = 0x01, 'Current controller is on.'

class ControlMode(IntEnumWithDescription):
    FREE = 0x00, 'Motor off.'
    BRAKE = 0x01, 'Brake mode.'
    CURRENT = 0x02, 'Current control mode.'
    CURRENT_STEP_GENERATOR = 0x03, 'Rectagular impulse generator.'
    CURRENT_SAW_GENERATOR = 0x04, 'Saw impulse generator.'
    POSITION = 0x08, 'Position control mode.'
    SPEED = 0x09, 'Speed control mode.'
    PROFILE_ACCEL = 0x0a, 'Accel-speed based speed profile.'
    PROFILE_TIME = 0x0b, 'Time-based speed profile mode.'

def _create_register_mapper(encoder): 
    # converters
    c = {}

    # first order converters 
    if encoder:
        c['position'] = ConvScale(np.int16, 360/4096, 'deg')
    else:
        c['position'] = ConvScale(np.int16, 0.08057, 'deg')
    c['current'] = ConvScale(np.int16, 0.004884, 'A')
    c['voltage'] = ConvScale(np.int16, 0.008056, 'V')
    c['T1'] = ConvScale(np.uint8, 66.66e-6, 's')
    c['time'] = ConvScale(np.uint8, 1.0e-6, 's')
    c['temperature'] = ConvTermistorUp(np.uint8, T0=25, Rt0=1e4, B=3455, Rd=1e4, adc_bits=8, unit = '\'C')

    # status decoders
    c['status_error'] = ConvEnum(np.uint8, StatusError)
    c['status_detail'] = ConvEnum(np.uint8, StatusDetail)

    # second order converters
    c['T2'] = ConvDerived(np.uint8, mul=(c['T1'], 16))
    c['speed'] = ConvDerived(np.int16, mul=c['position'], div=c['T1'], fracbits=14, unit='deg/s')
    c['accel'] = ConvDerived(np.int16, mul=c['speed'], div=c['T1'], fracbits=10, unit='deg/s^2')

    # registers table
    registers = [
        [ "model_no_1",             np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Hardware model (major)." ],
        [ "model_no_2",             np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Hardware model (minor)." ],
        [ "version1",               np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Firmware version (major)." ],
        [ "version2",               np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Firmware version (minor)." ],
        [ "baudrate",               np.uint8,    Memory.EEP,    Access.RW,  ConvEnum(np.uint8, Baudrate),                                                 "Baudrate: 0x1 (57600), 0x2 (115200), 0x7 (921600), 0x8 (1000000). See manual for full mode list." ],
        [ "reserved1",              np.uint8,    Memory.EEP,    Access.RW,  None,                                                                         "Reserved" ],
        [ "id",                     np.uint8,    Memory.BOTH,   Access.RW,  None,                                                                         "Servo ID." ],
        [ "ack_policy",             np.uint8,    Memory.BOTH,   Access.RW,  ConvEnum(np.uint8, AckPolicy),                                                "ACK packet send policy. Flags: { READ_ACK (0x01), WRITE_ACK (0x02), ERROR ACK: (0x04) }" ],
        [ "torque_policy",          np.uint8,    Memory.BOTH,   Access.RW,  ConvEnum(np.uint8, StatusError),                                              "If (r{torque_policy} & r{status_error} & 0x7F) set r{torque_mode} to FREE or BRAKE if (r{torque_policy} & 0x80)." ],
		[ "operation_mode",         np.uint8,    Memory.BOTH,   Access.RW,  ConvEnum(np.uint8, OperationMode),                                            "Control loop configuration. Flags: { CURRENT_CONTROLER_ON (0x01) }" ],
        [ "max_temperature",        np.uint8,    Memory.BOTH,   Access.RW,  c['temperature'],                                                             "Overhead detection threshold." ],
        [ "min_voltage",            np.uint8,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint8, mul=c['voltage'], fracbits=-4),                         "Undervoltage detection threshold." ],
        [ "max_voltage",            np.uint8,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint8, mul=c['voltage'], fracbits=-4),                         "Overvoltage detection threshold." ],
        [ "avg_current_alpha",      np.uint8,    Memory.BOTH,   Access.RW,  ConvScale(np.uint8, target_unit=1.0, fracbits=16),                            "Current filter coefficient on T2 period (u.16)." ],
        [ "pwm_deadzone",           np.uint8,    Memory.BOTH,   Access.RW,  ConvScale(np.uint8, target_unit=1.0/800),                                     "If PWM filling factor is lower than this value apply zero PWM." ],
        [ "reserved2",              np.uint8,    Memory.BOTH,   Access.RW,  None,                                                                         "Reserved." ],
        [ "max_avg_current",       np.uint16,    Memory.BOTH,   Access.RW,  c["current"],                                                                 "Average overcurrent detection threshold." ],
        [ "max_current",           np.uint16,    Memory.BOTH,   Access.RW,  c["current"],                                                                 "Peak overcurrent detection threshold." ],
        [ "current_limit",         np.uint16,    Memory.BOTH,   Access.RW,  c["current"],                                                                 "Maximal allowed reference current (saturation limit)." ],
        [ "min_position",           np.int16,    Memory.BOTH,   Access.RW,  c["position"],                                                                "Movement limit absolute position. Check is performed after applying position offset." ],
        [ "max_position",           np.int16,    Memory.BOTH,   Access.RW,  c["position"],                                                                "Movement limit absolute position. Check is performed after applying position offset." ],
        [ "inpos_threshold",        np.uint8,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint8, mul=c["position"]),                                     "If position error less then threshold INPOS flag is set." ],
        [ "moving_threshold",       np.uint8,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint8, mul=c["speed"], fracbits=-2),                           "If speed is greater then threshold MOVING falg is set." ],
        [ "resistance",            np.uint16,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint16,mul=c['voltage'],div=c["current"],fracbits=12,unit="Ohm"), "Resistance. This is used to calculate voltage from current reference if current controller is disabled." ],
        [ "inv_back_emf_coeff",    np.uint16,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint16,mul=c['speed'],div=c["voltage"],fracbits=10,unit="deg/Vs"),"Inversed backEMF coefficient, deg/(sV)." ],
        [ "position_filter_alpha", np.uint16,    Memory.BOTH,   Access.RW,  ConvScale(np.uint16, target_unit=1.0, fracbits=18),                           "Position exponential filter coeffeicent on T1 (u.18)" ],
        [ "speed_filter_alpha",    np.uint16,    Memory.BOTH,   Access.RW,  ConvScale(np.uint16, target_unit=1.0, fracbits=18),                           "Speed exponential filter coeffeicent on T1 (u.18)" ],
        [ "current_offset",         np.int16,    Memory.BOTH,   Access.RW,  c["current"],                                                                 "Current measurements zero shift. Automatically updated in FREE mode." ],
        [ "current_kp",             np.int16,    Memory.BOTH,   Access.RW,  ConvDerived(np.int16, mul=c['voltage'], div=c["current"], fracbits=12),       "Current controller proportional gain, current to voltage (s3.12)." ],
        [ "current_ki",             np.int16,    Memory.BOTH,   Access.RW,  ConvDerived(np.int16, mul=c['voltage'], div=c["current"], fracbits=12),       "Current controller integral gain, current to voltage on T1 period (s3.12). T1 = T2/16." ],
        [ "current_alpha",          np.int16,    Memory.BOTH,   Access.RW,  ConvScale(np.int16, target_unit=1.0, fracbits=12),                            "Current controller integral drain (s3.12). If it is nonzero controller compensates backEMF only partially." ],
        [ "position_offset",        np.int16,    Memory.BOTH,   Access.RW,  c["position"],                                                                "Position offset, ADC units (s15)" ],
        [ "position_kp",           np.uint16,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint16, mul=c['current'], div=c["position"], fracbits=8),      "Position controller proportional gain, position to current (u8.8)." ],
        [ "position_ki",           np.uint16,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint16, mul=c['current'], div=c["position"], fracbits=12),     "Position controller np.integral gain, position to current on T2 period (u4.12)." ],
        [ "position_kd",           np.uint16,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint16, mul=c['current'], div=c["speed"], fracbits=-2),        "Position controller differential gain, speed to current (u.18)." ],
        [ "position_Iff0",          np.int16,    Memory.BOTH,   Access.RW,  c["current"],                                                                 "Speed sign feedforward to current, current ADC units (s15)." ],
        [ "position_kff1",          np.int16,    Memory.BOTH,   Access.RW,  ConvDerived(np.int16, mul=c['current'], div=c["speed"], fracbits=-2),         "Speed feedforward, speed to current (s.18)" ],
        [ "position_kff2",          np.int16,    Memory.BOTH,   Access.RW,  ConvDerived(np.int16, mul=c['current'], div=c["accel"], fracbits=-16),        "Accel feedforward, accel to current (s.32)" ],
        [ "profile_speed",          np.int16,    Memory.BOTH,   Access.RW,  c["speed"],                                                                   "Maximal speed in accel-speed based speed profile mode." ],
        [ "profile_accel",          np.int16,    Memory.BOTH,   Access.RW,  c["accel"],                                                                   "Maximal acceleration in accel-speed based speed profile mode." ],
        [ "profile_time_ratio",     np.uint8,    Memory.BOTH,   Access.RW,  ConvScale(np.uint8, target_unit=1.0, fracbits=8),                             "Raio beteween duration of acceleration (decelearion) stage and constant-speed stage in time-based profile mode (u.8)." ],
        [ "rt_start_delay",         np.uint8,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint8, mul=[c['time'], 10]),                                   "Delay between RT_READ request and first ACK packet, 10 mcs" ],
        [ "rt_delay",               np.uint8,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint8, mul=[c['time'], 10]),                                   "Delay between RT_READ ACK packets, 10 mcs" ],
        [ "ack_timeout",            np.uint8,    Memory.BOTH,   Access.RW,  ConvDerived(np.uint8, mul=[c['time'], 100]),                                  "Maximal allowed ACK packet delay for all requests except `RT_DEBUG` и `RT_EXCHANGE`, 100 mcs." ],
        [ "rt_playtime",            np.uint8,    Memory.BOTH,   Access.RW,  c['T2'],                                                                      "Position exptrapolation duration after receiving RT_WRITE command, T2 periods." ],
        [ "torque_mode",            np.uint8,    Memory.RAM,    Access.RW,  ConvEnum(np.uint8, TorqueMode),                                               "Control mode override: FREE (0x00), BRAKE (0x40), NORMAL (Ox60)" ],
        [ "status_error",           np.uint8,    Memory.RAM,    Access.RW,  c['status_error'],                                                            "Servo hardware status." ],
        [ "status_detail",          np.uint8,    Memory.RAM,    Access.RW,  c['status_detail'],                                                           "Servo status." ],
        [ "position_target",        np.int16,    Memory.RAM,    Access.RO,  c["position"],                                                                "Target postion for last PROFILE command (modified by RT_WRITE command)" ],
        [ "playtime",              np.uint16,    Memory.RAM,    Access.RO,  ConvDerived(np.uint16, mul=c['T2']),                                          "Motion duration for PROFILE_TIME or watchdog time for SPEED, POSITION and CURRENT mode, T2 periods." ],
        [ "position_ref",           np.int16,    Memory.RAM,    Access.RO,  c["position"],                                                                "Reference position set by RT_WRITE (POSITION)  or by trajectory generator (PROFILE, SPEED)." ],
        [ "speed_ref",              np.int16,    Memory.RAM,    Access.RO,  c["speed"],                                                                   "Reference speed set by RT_WRITE (POSITION, SPEED mode)  or by trajectory generator (PROFILE), position/T1 (s1.14)." ],
        [ "current_ff",             np.int16,    Memory.RAM,    Access.RO,  c["current"],                                                                 "Current feedforward modifer set by  RT_WRITE command (POSITION, SPEED, CURRENT)  or by trajectory generator (PROFILE)." ],
        [ "current_ref",            np.int16,    Memory.RAM,    Access.RO,  c["current"],                                                                 "Reference current for current regulator." ],
        [ "position_raw",           np.int16,    Memory.RAM,    Access.RO,  ConvDerived(np.uint16, mul=c['position'], fracbits=2),                        "Measured position (u14.2)." ],
        [ "position",               np.int16,    Memory.RAM,    Access.RO,  c["position"],                                                                "Filtered position (s15)." ],
        [ "speed",                  np.int16,    Memory.RAM,    Access.RO,  c["speed"],                                                                   "Estimated speed, position unit per T1 (s1.14)." ],
        [ "current",                np.int16,    Memory.RAM,    Access.RO,  c["current"],                                                                 "Actual current." ],
        [ "voltage",               np.uint16,    Memory.RAM,    Access.RO,  c["voltage"],                                                                 "DC source voltage." ],
        [ "voltage_pwm",            np.int16,    Memory.RAM,    Access.RO,  c["voltage"],                                                                 "Effective voltage applied to the motor." ],
        [ "temperature",            np.uint8,    Memory.RAM,    Access.RO,  c['temperature'],                                                             "Temperature readings." ],
        [ "control_mode",           np.uint8,    Memory.RAM,    Access.RO,  ConvEnum(np.uint8, ControlMode),                                              "Current control mode. This value is set by RT_WRITE cmd." ],
        [ "min_rt_start_delay",     np.uint8,    Memory.RAM,    Access.RO,  ConvDerived(np.uint8, mul=[c['time'], 10]),                                   "Minimal value of  r{rt_start_delay} value (RT_* request porcessing duration), 10 mcs)." ],
        [ "max_fast_control_delay", np.uint8,    Memory.RAM,    Access.RO,  c["time"],                                                                    "T1 level control code execution duration, 1 mcs." ],
        [ "max_slow_control_delay", np.uint8,    Memory.RAM,    Access.RO,  c["time"],                                                                    "T2 level control code execution duration, 1 mcs." ],
    ]  

    return registers

class ServoCMCBase():
    T1 = 66.66e-6
    T2 = T1*16
    MAX_ACK_PACKET_LENGTH = 50
    
    def __init_subclass__(cls, model, version, registers):
        # call Servo metaclass hook
        super(ServoCMCBase, cls).__init_subclass__(model, version, registers)
        # add additional members
        cls._converter_temperature = cls.registers_ram['temperature'].converter
        cls._position_scale = cls.registers_ram['position'].converter.scale
        cls._speed_scale = cls.registers_ram['speed'].converter.scale
        cls._accel_scale = cls.registers_ram['profile_accel'].converter.scale
        cls._current_scale = cls.registers_ram['current'].converter.scale

    def encode_rt_write_subpacket(self, mode, arg1, arg2, arg3, convert = True):
        try:
            if convert:
                # convert SI to target units 
                if mode == self.ControlMode.PROFILE_ACCEL:
                    position_target = int(arg1 / self._position_scale)
                    speed = int(arg2 / self._speed_scale)
                    accel = int(arg3 / self._accel_scale)
                    return struct.pack('BBhhh', self._servo_id, mode, position_target, speed, accel)
                elif mode == self.ControlMode.PROFILE_TIME:
                    position_target = int(arg1 / self._position_scale)
                    time1 = int(arg2 / self.T2)
                    time2 = int(arg3 / self.T2)
                    return struct.pack('BBhhh', self._servo_id, mode, position_target, time1, time2)
                else:
                    position_ref = int(arg1 / self._position_scale)
                    speed_ref = int(arg2 / self._speed_scale)
                    current_ff = int(arg3 / self._current_scale)
                    return struct.pack('BBhhh', self._servo_id, mode, position_ref, speed_ref, current_ff)
            else:
                # do not perform convertion
                return struct.pack('BBhhh', self._servo_id, mode, arg1, arg2, arg3)
        except struct.error as e:
            raise ValueError(str(e))

    def decode_rt_read(self, data, convert = True):
        # extract data
        try:
            position, speed, current, temp, status_error = struct.unpack('hhhBB', data)
        except struct.error as e:
            raise ValueError(str(e))
        # convertion
        if convert:
            position = self._position_scale * position
            speed = self._speed_scale * speed
            current = self._current_scale * current
            temp = self._converter_temperature.from_target(temp)
        # decode status
        status_error = self.StatusError(status_error)
        # return result
        return position, speed, current, temp, status_error

    def decode_rt_debug(self, channel, raw_data):
        if channel == 10:
            fields = [
                    ('current_ref', self.registers_ram['current'].converter),
                    ('current', self.registers_ram['current'].converter),
                    ('voltage_pwm', self.registers_ram['voltage'].converter),
                    ('position', self.registers_ram['position'].converter)
                ]
            return self._decode_rt_debug_data(raw_data, fields, self.T1)
        else:
            return raw_data, None
    
    def start_waveform_generator(self, mode, amplitude, period, offset, **kwargs):
        """ Start waveform generator.

        Parameters:
        -----------
        mode: ControlMode
            Generator mode
        amplitude: float, period, offset: float
            Signal amplitude (A), period (s) and offset (A). 
        receive_ack, timeout, tryouts
            Additional args for underlying ProtocolHandle method.

        """
        # calculate change rate
        change_rate = (2*abs(amplitude) / period) * self.T1
        # convert to target units
        amplitude = int(amplitude / self._current_scale)
        change_rate = int(change_rate / self._current_scale * 2**8) # add 8 fracbits
        offset = int(offset / self._current_scale)
        # assign saw generator configuration
        return self.rt_write_one(mode, amplitude, change_rate, offset, convert = False, **kwargs)


class ServoCMC01(ServoCMCBase, Servo, model = 0x10, version = 0x09, registers = _create_register_mapper(encoder = False)):
    pass

class ServoCMC01Enc(ServoCMCBase, Servo, model = 0x11, version = 0x09, registers = _create_register_mapper(encoder = True)):
    pass

#
# Sensors Test 
#

class ServoCMCTestSensorsBase():

    def __init_subclass__(cls, model, version, registers):
        # call Servo metaclass hook
        super(ServoCMCTestSensorsBase, cls).__init_subclass__(model, version, registers)

    def write_waveform_configuration(self, amplitude, period, offset, **kwargs):
        """ Set waveform for CURRENT_SAW and CURRENT_STEP mode.

        Assigns position_ref, speed_ref, current_ff registers used by waveform generator.

        Parameters:
        -----------
        amplitude, period, offset: float
            Signal amplitude, period and offset. 

        """
        # calculate change rate
        change_rate = (2*abs(amplitude) / period) * self.T1
        # convert to target units
        amplitude = int(amplitude / self._current_scale)
        change_rate = int(change_rate / self._current_scale * 2**8) # add 8 fracbits
        offset = int(offset / self._current_scale)
        # assign saw generator configuration
        return self.write_ram(['position_ref', 'speed_ref', 'current_ff'], [amplitude, change_rate, offset], convert = False, **kwargs)

    def encode_rt_write_subpacket(self, mode, arg1, arg2, arg3, convert = True):
        try:
            if convert:
                # convert SI to target units 
                position_ref = int(arg1 / self._position_scale)
                speed_ref = int(arg2 / self._speed_scale)
                current_ff = int(arg3 / self._current_scale)
                return struct.pack('BBhhh', self._servo_id, mode, position_ref, speed_ref, current_ff)
            else:
                # do not perform convertion
                return struct.pack('BBhhh', self._servo_id, mode, arg1, arg2, arg3)
        except struct.error as e:
            raise ValueError(str(e))

    def decode_rt_debug(self, channel, raw_data):
        if channel == 2:
            fields = [ ('position', self.registers_ram['position'].converter) ]
            return self._decode_rt_debug_data(raw_data, fields , self.T1/4)
        if channel == 3:
            fields = [ ('current', self.registers_ram['current'].converter) ]
            return self._decode_rt_debug_data(raw_data, fields , self.T1/4)
        elif channel == 4:
            fields = [ ('voltage', self.registers_ram['voltage'].converter) ]
            return self._decode_rt_debug_data(raw_data, fields, self.T1/4)
        elif channel == 5:
            fields = [ ('temperature', self.registers_ram['temperature'].converter) ]
            return self._decode_rt_debug_data(raw_data, fields, self.T1/4)
        elif channel == 6:
            fields = [
                    ('position', self.registers_ram['position'].converter),
                    ('current', self.registers_ram['current'].converter),
                    ('voltage', self.registers_ram['voltage'].converter),
                    ('temperature', self.registers_ram['temperature'].converter)
                ]
            return self._decode_rt_debug_data(raw_data, fields, self.T1)
        elif channel == 7:
            fields = [
                    ('position', self.registers_ram['position'].converter),
                    ('speed', self.registers_ram['speed'].converter),
                    ('current', self.registers_ram['current'].converter),
                    ('voltage', self.registers_ram['voltage'].converter),
                    ('voltage_pwm', self.registers_ram['voltage'].converter)
                ]
            return self._decode_rt_debug_data(raw_data, fields, self.T1)
        elif channel == 8:
            fields = [
                    ('position', self.registers_ram['position'].converter),
                    ('speed', self.registers_ram['speed'].converter),
                    ('current', self.registers_ram['current'].converter),
                    ('voltage_pwm', self.registers_ram['voltage'].converter)
                ]
            return self._decode_rt_debug_data(raw_data, fields, self.T1)
        elif channel == 9:
            fields = [
                    ('position', self.registers_ram['position'].converter),
                    ('speed', self.registers_ram['speed'].converter),
                    ('current', self.registers_ram['current'].converter),
                    ('voltage_pwm', self.registers_ram['voltage'].converter)
                ]
            return self._decode_rt_debug_data(raw_data, fields, self.T2)
        elif channel == 10:
            fields = [
                    ('current_ref', self.registers_ram['current'].converter),
                    ('current', self.registers_ram['current'].converter),
                    ('voltage_pwm', self.registers_ram['voltage'].converter),
                    ('position', self.registers_ram['position'].converter)
                ]
            return self._decode_rt_debug_data(raw_data, fields, self.T1)
        elif channel == 11:
            fields = [
                    ('current', self.registers_ram['current'].converter),
                    ('voltage_pwm', self.registers_ram['voltage'].converter),
                    ('voltage', self.registers_ram['voltage'].converter),
                    ('pwm', ConvScale(np.int16, 1/200))
                ]
            return self._decode_rt_debug_data(raw_data, fields, self.T1)
        else:
            return raw_data, None

class ServoCMC01TestSensor(ServoCMCTestSensorsBase, ServoCMCBase, Servo, model = 0x10, version = 0x89, registers = _create_register_mapper(encoder=False)):
    pass

class ServoCMC01TestSensorEnc(ServoCMCTestSensorsBase, ServoCMCBase, Servo, model = 0x11, version = 0x89, registers = _create_register_mapper(encoder=True)):

    def decode_rt_debug(self, channel, raw_data):
        if channel == 2:
            # override behavior: position data contains encoder return codes
            # try to separate actual meashurements (non-negative) and return codes (negative)
            # they are following with period of four so find first one
            raw_data = raw_data.view(np.int16)
            mind = np.argmax(raw_data >= 0)
            # use identity converter for codes 
            code_conv = ConvScale(np.int16, target_unit = 1.0)
            fields = [ ('position', self.registers_ram['position'].converter),
                       ('code1', code_conv),
                       ('code2', code_conv),
                       ('code3', code_conv),
                      ]
            return self._decode_rt_debug_data(raw_data[mind:], fields , self.T1*4)
        elif channel == 6:
            # override behavior: position data contains encoder return codes
            # replace encoder return codes with NaN
            shaped_data, conv_data = super(ServoCMC01TestSensorEnc, self).decode_rt_debug(channel, raw_data)
            conv_data['position'][shaped_data['position'] < 0] = np.nan
            return shaped_data, conv_data
        else:
            return super(ServoCMC01TestSensorEnc, self).decode_rt_debug(channel, raw_data)


