import numpy as np

from ..enum_description import IntFlagWithDescription, IntEnumWithDescription
from ..servo import *
from ..conv_temperature import ConvTable

# Regster flags and enums
class Baudrate(IntEnumWithDescription):
     B666666 = 0x02, '666 kbit/s'
     B500000 = 0x03, '500 kbit/s'
     B400000 = 0x04, '400 kbit/s'
     B250000 = 0x07, '250 kbit/s'
     B200000 = 0x09, '200 kbit/s'
     B115200 = 0x10, '115200 baud'
     B57600 =  0x22, '57600 baud'

     @property
     def numeric_value(self):
         return int(self.name[1:])

     @classmethod
     def from_numeric(cls, value):
         try:
             return cls.members[f'B{int(value)}']
         except (KeyError, TypeError, ValueError):
             raise ValueError(f'Incorrect or uncompatible numeric baudrate value: {value}')

class AckPolicy(IntEnumWithDescription):
    NOACK = 0x00, 'Reply only on STAT command.'
    READ = 0x01, 'Send answer to READ command.'
    WRITE = 0x02, 'Send answer to READ and WRITE commands.'

class StatusError(IntFlagWithDescription):
    VOLTAGE = 0x01, 'Over- or undervoltage'
    POT_LIMIT = 0x02, 'Postion limit is exceeded'
    INVALID_PACKET = 0x08, 'Received invalid packet.'
    TEMPERATURE = 0x04, 'Overheat'
    OVERLOAD = 0x10, 'Oerload (current avg limit).'
    DRIVER_FAULT = 0x20, 'Hardware fault'
    EEP_ERROR = 0x40, 'EEP checksum error'

class StatusDetail(IntFlagWithDescription):
    MOVING = 0x01, 'Servo is moving'
    INPOS = 0x02, 'Target position is reached'
    CHECKSUM_ERROR = 0x04, 'Checksum error'
    INVALID_CMD = 0x08, 'Invalid protocol command'
    BAD_REG_RANGE = 0x10, 'Invalid register address'
    GARBAGE = 0x20, 'Garbage on line is detecred.'
    MOTOR_ON = 0x40, 'Motor is on'

class TorqueMode(IntEnumWithDescription):
    FREE = 0x00, 'Motor off', 
    BRAKE = 0x40, 'Brake mode', 
    TORQUE_ON = 0x60, 'Motor is on.'

class LedMode(IntFlagWithDescription):
    ON = 0x01, 'Led is on'
    GREEN = 0x02, 'Green led is on.'
    BLUE = 0x04, 'Blue led is on.'
    RED = 0x08, 'Red led is on.'

class JogMode(IntFlagWithDescription):
    POSITION = 0x00, 'Position mode',
    STOP = 0x01, 'Motor off'
    SPEED = 0x02, 'Speed mode'
    LED_GREEN = 0x04, 'Green led is on'
    LED_BLUE = 0x08, 'Blue led is on'
    LED_RED = 0x10, 'Red led is on'
    JOG_MASK = 0x3, 'Select control-related bits'
    LED_MASK = 0x1c, 'Select led-related bits'

# converters
c = {}

# first order converters 
c['position16'] = ConvScale(np.int16, 0.325, 'deg')
c['position8'] = ConvScale(np.int8, 0.325, 'deg')
c['voltage'] = ConvScale(np.int16, 0.074, 'V')
c['pwm8'] = ConvScale(np.int8, 1/1024)
c['pwm16'] = ConvScale(np.int16, 1/1024)
c['time'] = ConvScale(np.uint8, 0.0112, 's')
temp_table = np.array([ -79.47,-71.78,-63.20,-57.81,-53.80,-50.58,-47.86,-45.49,-43.40,-41.51,-39.79,-38.20,-36.73,-35.35,
    -34.06,-32.83,-31.67,-30.57,-29.51,-28.50,-27.53,-26.59,-25.69,-24.82,-23.97,-23.15,-22.36,-21.59,-20.83,-20.10,-19.38,-18.68,-18.00,-17.33,-16.67,-16.03,
    -15.39,-14.77,-14.17,-13.57,-12.98,-12.40,-11.83,-11.26,-10.71,-10.16,-9.62,-9.09,-8.56,-8.04,-7.53,-7.02,-6.52,-6.02,-5.53,-5.04,-4.56,-4.08,-3.61,-3.14,
    -2.67,-2.21,-1.75,-1.29,-.84,-.39,5,.49,.93,1.37,1.81,2.24,2.67,3.10,3.52,3.94,4.37,4.78,5.20,5.62,6.03,6.44,6.86,7.27,7.67,8.08,8.49,8.89,9.29,9.70,10.10,10.50,
    10.90,11.30,11.70,12.09,12.49,12.89,13.28,13.68,14.07,14.47,14.86,15.26,15.65,16.05,16.44,16.84,17.23,17.62,18.02,18.41,18.81,19.20,19.60,19.99,20.39,20.79,
    21.19,21.58,21.98,22.38,22.78,23.18,23.59,23.99,24.39,24.80,25.20,25.61,26.02,26.43,26.84,27.25,27.66,28.08,28.50,28.91,29.33,29.76,30.18,30.60,31.03,31.46,
    31.89,32.32,32.76,33.20,33.64,34.08,34.53,34.97,35.42,35.88,36.33,36.79,37.25,37.72,38.18,38.66,39.13,39.61,40.09,40.57,41.06,41.56,42.05,42.56,43.06,43.57,
    44.09,44.61,45.13,45.66,46.19,46.73,47.28,47.83,48.39,48.95,49.52,50.09,50.68,51.27,51.86,52.47,53.08,53.70,54.33,54.96,55.61,56.26,56.93,57.60,58.28,58.98,
    59.68,60.40,61.13,61.87,62.63,63.39,64.17,64.97,65.78,68.61,67.46,68.32,69.20,70.10,71.02,71.96,72.92,73.91,74.92,75.96,77.03,78.12,79.25,80.41,81.60,82.84,
    84.11,85.42,86.79,88.20,89.66,91.18,92.76,94.41,96.13,97.93,99.82,101.81,103.90,106.11,108.45,110.93,113.59,116.43,119.49,122.80,126.41,130.36,134.72,
    139.59,145.09,151.39,158.73,167.50,178.29,192.18,211.32,241.01,300.91,np.nan ], dtype=np.float16)
c['temperature'] = ConvTable(np.uint8, temp_table, unit = '\'C')

# status decoders
c['status_error'] = ConvEnum(np.uint8, StatusError)
c['status_detail'] = ConvEnum(np.uint8, StatusDetail)

# second order converters
c['speed'] = ConvDerived(np.int16, mul=c['position16'], div=c['time'], fracbits=0, unit='deg/s')



# registers table
registers = [
    [ "model1",                       np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Hardware model (major)." ],
    [ "model2",                       np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Hardware model (minor)." ],
    [ "version1",                     np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Firmware version (major)." ],
    [ "version2",                     np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Firmware version (minor)." ],
	[ "baudrate",                     np.uint8,    Memory.EEP,    Access.RO,  ConvEnum(np.uint8, Baudrate),                                                 "Baudrate." ],
    [ "reserved1",                    np.uint8,    Memory.EEP,    Access.RO,  None,                                                                         "Reserved" ],
    [ "servo_id",                     np.uint8,    Memory.BOTH,   Access.RW,  None,                                                                         "Servo ID" ],
    [ "ack_policy",                   np.uint8,    Memory.BOTH,   Access.RW,  ConvEnum(np.uint8, AckPolicy),                                                "ACK Reply policy" ],
    [ "alarm_led_policy",             np.uint8,    Memory.BOTH,   Access.RW,  ConvEnum(np.uint8, StatusError),                                              "Blink LED policy" ],
    [ "torque_policy",                np.uint8,    Memory.BOTH,   Access.RW,  ConvEnum(np.uint8, StatusError),                                              "Torque policy" ],
    [ "reserved2",                    np.uint8,    Memory.BOTH,   Access.RO,  None,                                                                         "Reserved" ],
    [ "max_temperature",              np.uint8,    Memory.BOTH,   Access.RW,  c['temperature'],                                                             "Max. temperatue" ],
    [ "min_voltage",                  np.uint8,    Memory.BOTH,   Access.RW,  c['voltage'],                                                                 "Min. voltage" ],
    [ "max_voltage",                  np.uint8,    Memory.BOTH,   Access.RW,  c['voltage'],                                                                 "Max. voltage" ],
    [ "acceleration_ratio",           np.uint8,    Memory.BOTH,   Access.RW,  ConvScale(np.uint8, 0.01),                                                    "Acceleration ratio" ],
    [ "max_acceleration_time",        np.uint8,    Memory.BOTH,   Access.RW,  c['time'],                                                                    "Max. acceleration time" ],
    [ "dead_zone",                    np.uint8,    Memory.BOTH,   Access.RW,  c['position8'],                                                               "Dead zone" ],
    [ "saturator_offset",             np.uint8,    Memory.BOTH,   Access.RW,  c['pwm8'],                                                                    "Saturator offset" ],
    [ "saturator_slope",              np.uint16,   Memory.BOTH,   Access.RW,  ConvDerived(np.uint16, mul=c['pwm16'], div=c['position16']),                  "Saturator slope" ],
    [ "pwm_offset",                   np.int8,     Memory.BOTH,   Access.RW,  c['pwm8'],                                                                    "PWM Offset" ],
    [ "min_pwm",                      np.uint8,    Memory.BOTH,   Access.RW,  c['pwm8'],                                                                    "Min. PWM" ],
    [ "max_pwm",                      np.uint16,   Memory.BOTH,   Access.RW,  c['pwm16'],                                                                   "Max. PWM" ],
    [ "overload_pwm_threshold",       np.uint16,   Memory.BOTH,   Access.RW,  c['pwm16'],                                                                   "Overload PWM threshold" ],
    [ "min_position",                 np.int16,    Memory.BOTH,   Access.RW,  c['position16'],                                                              "Min. position" ],
    [ "max_position",                 np.int16,    Memory.BOTH,   Access.RW,  c['position16'],                                                              "Max. position" ],
    [ "position_kp",                  np.uint16,   Memory.BOTH,   Access.RW,  None,                                                                         "Proportional Gain" ],
    [ "position_kd",                  np.uint16,   Memory.BOTH,   Access.RW,  None,                                                                         "Derivative Gain" ],
    [ "position_ki",                  np.uint16,   Memory.BOTH,   Access.RW,  None,                                                                         "Integral Gain" ],
    [ "position_ff_1st_gain",         np.uint16,   Memory.BOTH,   Access.RW,  None,                                                                         "Position Feed forward 1st Gain" ],
    [ "position_ff_2st_gain",         np.uint16,   Memory.BOTH,   Access.RW,  None,                                                                         "Position Feed forward 2st Gain" ],
    [ "reserved3",                    np.uint16,   Memory.BOTH,   Access.RO,  None,                                                                         "Reserved" ],
    [ "reserved4",                    np.uint16,   Memory.BOTH,   Access.RO,  None,                                                                         "Reserved" ],
    [ "led_blink_period",             np.uint8,    Memory.BOTH,   Access.RW,  c['time'],                                                                    "Alarm LED blink period" ],
    [ "adc_fault_check_period",       np.uint8,    Memory.BOTH,   Access.RW,  c['time'],                                                                    "Temp/voltage error check period" ],
    [ "packet_garbage_check_period",  np.uint8,    Memory.BOTH,   Access.RW,  c['time'],                                                                    "Packet Erorr check period" ],
    [ "stop_detection_period",        np.uint8,    Memory.BOTH,   Access.RW,  c['time'],                                                                    "Stop detection check period" ],
    [ "overload_detection_period",    np.uint8,    Memory.BOTH,   Access.RW,  c['time'],                                                                    "Overload Check Interval" ],
    [ "stop_threshold",               np.uint8,    Memory.BOTH,   Access.RW,  c['speed'],                                                                   "Stop Threshold" ],
    [ "inposition_margin",            np.uint8,    Memory.RAM,    Access.RW,  c['position8'],                                                               "Offset Threshold" ],
    [ "reserved5",                    np.uint8,    Memory.RAM,    Access.RO,  None,                                                                         "Reserved" ],
    [ "reserved6",                    np.uint8,    Memory.RAM,    Access.RO,  None,                                                                         "Reserved" ],
    [ "calibration_difference",       np.int8,     Memory.RAM,    Access.RW,  c['position8'],                                                               "Servo Compensation" ],
    [ "status_error",                 np.uint8,    Memory.RAM,    Access.RW,  ConvEnum(np.uint8, StatusError),                                              "Status erorr mask" ],
    [ "status_detail",                np.uint8,    Memory.RAM,    Access.RW,  ConvEnum(np.uint8, StatusDetail),                                             "Status detail mask" ],
    [ "reserved7",                    np.uint8,    Memory.RAM,    Access.RO,  None,                                                                         "Reserved" ],
    [ "reserved8",                    np.uint8,    Memory.RAM,    Access.RO,  None,                                                                         "Reserved" ],
    [ "torque_mode",                  np.uint8,    Memory.RAM,    Access.RW,  ConvEnum(np.uint8, TorqueMode),                                               "Torque control" ],
    [ "led_control",                  np.uint8,    Memory.RAM,    Access.RW,  ConvEnum(np.uint8, LedMode),                                                  "Led contorl" ],
    [ "voltage",                      np.uint8,    Memory.RAM,    Access.RO,  c['voltage'],                                                                 "Voltage" ],
    [ "temperature",                  np.uint8,    Memory.RAM,    Access.RO,  c['temperature'],                                                             "Temperature" ],
    [ "jog_mode",                     np.uint8,    Memory.RAM,    Access.RO,  ConvEnum(np.uint8, JogMode),                                                  "Current JOG command control mode" ],
    [ "tick",                         np.uint8,    Memory.RAM,    Access.RO,  c['time'],                                                                    "Tick counter" ],
    [ "calibrated_position",          np.int16,    Memory.RAM,    Access.RO,  c['position16'],                                                              "Calibrated Position" ],
    [ "absolute_position",            np.int16,    Memory.RAM,    Access.RO,  c['position16'],                                                              "Uncalibrated absolute position" ],
    [ "differential_position",        np.uint16,   Memory.RAM,    Access.RO,  c['speed'],                                                                   "Differential Position" ],
    [ "pwm",                          np.int16,    Memory.RAM,    Access.RO,  c['pwm16'],                                                                   "Current torque in raw data" ],
    [ "reserved9",                    np.uint16,   Memory.RAM,    Access.RO,  None,                                                                         "Reserved" ],
    [ "absolute_goal_position",       np.int16,    Memory.RAM,    Access.RO,  c['position16'],                                                              "Uncalibrated goal position" ],
    [ "absolute_desired_position",    np.int16,    Memory.RAM,    Access.RO,  c['position16'],                                                              "Current intermediate goal position in trajectory" ],
    [ "desired_velocity",             np.uint16,   Memory.RAM,    Access.RO,  c['speed'],                                                                   "Desired speed" ]
]  


class ServoHerkulexBase:
    
    def __init_subclass__(cls, model, version, registers):
        super(ServoHerkulexBase, cls).__init_subclass__(model, version, registers)
        cls._position_scale = cls.registers_ram['absolute_position'].converter.scale
        cls._speed_scale = cls.registers_ram['differential_position'].converter.scale
        cls._time_scale = cls.registers_ram['tick'].converter.scale

    def encode_i_jog_subpacket(self, jog_mode, jog_cmd, playtime, convert = True):
        if convert:
            jog = jog_mode & JogMode.JOG_MASK
            if jog  == JogMode.POSITION:
                jog_cmd = int(jog_cmd / self._position_scale)
            else:
                jog_cmd = int(jog_cmd / self._speed_scale)
            playtime = int(playtime / self._time_scale)
        try:
            return struct.pack('hBBB', jog_cmd, jog_mode, self._servo_id, playtime)
        except struct.error as e:
            raise ValueError(str(e))

    def encode_s_jog_subpacket(self, jog_mode, jog_cmd, convert = True):
        if convert:
            jog = jog_mode & JogMode.JOG_MASK
            if jog == JogMode.POSITION:
                jog_cmd = int(jog_cmd / self._position_scale)
            else:
                jog_cmd = int(jog_cmd / self._speed_scale)
        try:
            return struct.pack('hBB', jog_cmd, jog_mode, self._servo_id)
        except struct.error as e:
            raise ValueError(str(e))

class ServoDRS101(ServoHerkulexBase, Servo, model = 0x01, version = 0x00, registers = registers):
    pass

class ServoDRS201(ServoHerkulexBase, Servo, model = 0x02, version = 0x00, registers = registers):
    pass
