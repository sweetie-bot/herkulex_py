# -*- coding: utf-8 -*-

import logging
import time
from queue import SimpleQueue, Empty as QueueEmpty
from threading import Thread

import struct
import zmq

from . import protocol

#
# Logging facilities
#

log = logging.getLogger('protocol.bus')

#
# Bus Server
#

class ZeroMQBusServer:
    ''' ZeroMQ-based HerkulexPacket transport: server side.

    Usage example:
        
        period = 0.1 # control period, seconds

        bus = SerialBusHandle('/dev/ttyUSB0', 115200)
        protocol = ProtocolHandle(bus)
        server = ZeroMQBusServer(bus = bus, spin_period = period, spin_duration = 0.005)

        rate = rospy.Rate(1/period)
        while not rospy.is_shutdown():
            # Standart exchange
            # ...
            protocol.rt_write(bus, cmd)
            protocol.rt_read(servo_ids, state)
            # ...
            # Additional exchange via ZeroMQ transport
            server.spin()
    '''
    def __init__(self, bus, spin_period, spin_duration, base_port = 5555, description = None):
        ''' Create and start ZeroMQBusServer. 

        Ports:
        * base_port is used to rovide REQ/RESP service with information about service.
        * (base_port+1) and (base_port+2) are used for packet exchange.

        Parameters:
        -----------
        bus: BusHandleInterface
            Underlying bus handle object. It will be used in spin() call.
        spin_period: float
            Period of spin() call in seconds. This value is used by client to estimate timeout.
            It is expected that spin() is called at least once during period.
        spin_duration: float
            For long time spin() call will block underlying bus to forward requests to it.
        base_port: int
            Parameter defines ports to be used fot ZeroMQ transport.
        description: str or None
            Server description string. If None is passed it is deduced from bus __repr__.
        '''    
        self._thread = None
        # server parameters
        self._period = spin_period
        self._spin_duration = spin_duration
        self._bus = bus
        if description is None:
            self._description = f'ZeroMQBusServer(bus={bus})'
        else:
            self._description = description
        # ZeroMQ transport
        context = zmq.Context()
        self._zmq_context = context
        self._zmq_server_info_serv = context.socket(zmq.REP)
        self._zmq_server_info_serv.bind(f"tcp://*:{base_port}")
        # pacakage transport
        self._zmq_send_sub = context.socket(zmq.SUB)
        self._zmq_send_sub.connect(f"tcp://localhost:{base_port+1}")
        self._zmq_send_sub.setsockopt(zmq.SUBSCRIBE, b"")
        self._zmq_recv_pub = context.socket(zmq.PUB)
        self._zmq_recv_pub.bind(f"tcp://*:{base_port+2}")
        # exit signal
        self._zmq_exit_pub = context.socket(zmq.PUB)
        self._zmq_exit_pub.bind(f"inproc://exit-{id(self)}")
        self._zmq_exit_sub = context.socket(zmq.SUB)
        self._zmq_exit_sub.connect(f"inproc://exit-{id(self)}")
        self._zmq_exit_sub.setsockopt(zmq.SUBSCRIBE, b"")
        # worker thread
        self._thread = Thread(target = self._zmq_worker)
        # message buffers
        self._send_buffer = SimpleQueue()
        # self description
        self._base_port = base_port
        # start server
        self._thread.start()

    def __del__(self):
        self.shutdown()

    def shutdown(self):
        if self._thread is not None:
            while self._thread.is_alive():
                # signal worker
                self._zmq_exit_pub.send(bytes())
                # attenpt to join worker thread
                self._thread.join(0.1)

    def _zmq_worker(self):
        # create poller 
        poll = zmq.Poller()
        poll.register(self._zmq_server_info_serv)
        poll.register(self._zmq_send_sub)
        poll.register(self._zmq_exit_sub)
        # motor events
        while True:
            # wait for message
            socks = dict(poll.poll())
            # process message: server info
            if self._zmq_server_info_serv in socks:
                # get request
                req = self._zmq_server_info_serv.recv()
                # send response
                resp = struct.pack('ff', self._period, self._spin_duration) + self._description.encode('utf-8')
                self._zmq_server_info_serv.send(resp)
            # process message: send packet
            if self._zmq_send_sub in socks:
                # get packet
                data = self._zmq_send_sub.recv()
                # deserialize it and add to queue
                pkt = protocol.HerkulexPacket(servo_id=int(data[0]), cmd=protocol.ReqCmd(data[1]), data = data[2:])
                self._send_buffer.put(pkt)
            # finish processing
            if self._zmq_exit_sub in socks:
                self._zmq_exit_sub.recv()
                return 

    def spin(self):
        ''' Perform messgae forwarding. 

        This call blocks for 'spin_duration'. During this time period it forwards messages 
        from underlying 'bus' to ROSBusHandle using ROS transport.

        In carrent implementation it process only one request received on 'send' topic.
        '''

        # calculate deadline
        deadline = time.time() + self._spin_duration
        # get packet to send
        try:
            pkt = self._send_buffer.get_nowait()
        except QueueEmpty:
            return
        # perform request
        self._bus.send_packet(pkt)
        # wait and forward servo response
        while True:
            pkt = self._bus.receive_packet(deadline)
            if pkt is not None:
                # serialize packet and send
                self._zmq_recv_pub.send(struct.pack('BB', pkt.servo_id, pkt.cmd) + pkt.data)
            else:
                break

#
# ZeroMQ Bus handle
#

class ZeroMQBusHandle(protocol.BusHandleInterface):
    ''' ZeroMQ-based HerkulexPacket transport: client side.

        Provide abstraction to interract with BusHandle via ZeroMQBusServer.

        Usage example:

            bus = ZeroMQHandle(base_port = 5555)

            req = HerkulexPacket(0xfd, ReqCmd.RAM_READ, [ 60, 2 ])
            deadline = time.time() + timeout
            bus.send_packet(req)
            ack = bus.receive_packet(deadline)
    '''

    def __init__(self, base_port = 5555, processing_delay = 0.005):
        ''' Construct ROSBusHandle.

        Parameters
        ----------
        namespace: str
            Namespace where Server is located.
        processing_delay: float, 0.005
            Estimated delay caused by ROS transport.
        '''
        self._thread = None
        # ZeroMQ transport
        context = zmq.Context()
        self._zmq_context = context
        # pacakage transport
        self._zmq_send_pub = context.socket(zmq.PUB)
        self._zmq_send_pub.bind(f"tcp://*:{base_port+1}")
        self._zmq_recv_sub = context.socket(zmq.SUB)
        self._zmq_recv_sub.connect(f"tcp://localhost:{base_port+2}")
        self._zmq_recv_sub.setsockopt(zmq.SUBSCRIBE, b"")
        # exit signal
        self._zmq_exit_pub = context.socket(zmq.PUB)
        self._zmq_exit_pub.bind(f"inproc://exit-{id(self)}")
        self._zmq_exit_sub = context.socket(zmq.SUB)
        self._zmq_exit_sub.connect(f"inproc://exit-{id(self)}")
        self._zmq_exit_sub.setsockopt(zmq.SUBSCRIBE, b"")
        # worker thread
        self._thread = Thread(target = self._zmq_worker)
        # message buffers
        self._send_buffer = SimpleQueue()
        # connect to info service to get info
        succees = False
        for tryouts in range(3):
            try:
                zmq_server_info_call = context.socket(zmq.REQ)
                zmq_server_info_call.connect(f"tcp://localhost:{base_port}")
                zmq_server_info_call.send(bytes())
                if zmq_server_info_call.poll(timeout = 1000) & zmq.POLLIN != 0:
                    # get info packet
                    data = zmq_server_info_call.recv()
                    if (len(data) < 8):
                        continue
                    # extract info
                    self._max_delay, self._max_timeout = struct.unpack('ff', data[:8])
                    self._description = data[8:].decode('utf-8')
                    # break loop
                    succees = True
                    break
            except zmq.ZMQError:
                pass
            finally:
                zmq_server_info_call.setsockopt(zmq.LINGER, 0)
                zmq_server_info_call.close()
        if not succees:
            raise RuntimeError(f'Unable connect to ZeroMQBusServer on port {base_port}.')
        else:
            log.info(f"Connected to ZMQBusServer port='{base_port}': {self._description}")
        # receive buffer and lock
        self._recv_buffer = SimpleQueue()
        # self-description
        self._base_port = base_port
        # start server
        self._thread.start()

    def __del__(self):
        self.shutdown()

    def shutdown(self):
        if self._thread is not None:
            while self._thread.is_alive():
                # signal worker
                self._zmq_exit_pub.send(bytes())
                # attenpt to join worker thread
                self._thread.join(0.1)

    @property
    def max_timeout(self):
        return self._max_timeout

    def __repr__(self):
        return f'ZeroMQBus(port {self._base_port}: {self._description}, delay = {self._max_delay}, timeout = {self._max_timeout})'

    def send_packet(self, pkt):
        ''' Send HerkulexPacket over ZeroMQ link.

        Parameters
        ----------
        pkt: HerkulexPacket
            Packet to send.
        '''
        # serialize message and publish it
        data = struct.pack('BB', pkt.servo_id, pkt.cmd) + pkt.data
        self._zmq_send_pub.send(data)
        log.debug(f' >> ZMQBusServer(port={self._base_port}): {pkt}')

    def receive_packet(self, deadline = None):
        ''' Receive packet over ZeroMQ link.

        Parameters
        ----------
        deadline: float or None
            Time of receive deadline: deadline = time.time() + timeout. 

        Return
        ------
        HerkulexPacket or None
            Received packet or None if deadline is reached,
        '''
        try:
            return self._recv_buffer.get(timeout = deadline - time.time())
        except QueueEmpty:
            return None

    def _zmq_worker(self):
        # create poller 
        poll = zmq.Poller()
        poll.register(self._zmq_recv_sub)
        poll.register(self._zmq_exit_sub)
        # motor events
        while True:
            # wait for message
            socks = dict(poll.poll())
            # process message: recv packet
            if self._zmq_recv_sub in socks:
                # get packet and deserialize
                data = self._zmq_recv_sub.recv()
                pkt = protocol.HerkulexPacket(servo_id=int(data[0]), cmd=protocol.AckCmd(data[1]), data = data[2:])
                # add to queue
                self._recv_buffer.put(pkt)
                log.debug(f' << ZMQBusServer(port={self._base_port}): {pkt}')
            # finish processing
            if self._zmq_exit_sub in socks:
                self._zmq_exit_sub.recv()
                return 

    def estimate_transfer_delay(self, recv_bytes, send_bytes):
        ''' Estimate exchange duration based on transport properties. 

            Parameters
            ----------
            recv_bytes, send_bytes: int
                Number of bytes in packet include header.

            Return
            ------
            float
                Transfer dealy in seconds.

        '''
        return self._max_delay
